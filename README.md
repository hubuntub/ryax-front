[![coverage report](https://gitlab.com/ryax-tech/dev/ryax-front/badges/master/coverage.svg)](https://gitlab.com/ryax-tech/dev/ryax-front/-/commits/master)
[![pipeline status](https://gitlab.com/ryax-tech/dev/ryax-front/badges/master/pipeline.svg)](https://gitlab.com/ryax-tech/dev/ryax-front/-/commits/master)

# Ryax Front

This project was generated using [Nx](https://nx.dev).

## The project goes like this

Use either `yarn start` to start your front locally.

Use `yarn start:testing` to start with your front plugged to Dev-1.

Use `yarn start:staging` to start with your front plugged to Staging-1.

Use `nx run ryax-design:storybook` or `yarn us` to start the storybook linked to the Ryax library.

Use `nx g pug-schematics:pc --project=project-name` to generate components using pug.

For ease `nx g pug-schematics:pc --project=ryax-design` is aliased as `yarn guc` and will automatically create a link to storybook.

## Quick Start & Documentation

[Nx Documentation](https://nx.dev/angular)

## Generate a library

Run `ng g @nrwl/angular:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@ryax/mylib`.

## Development server

Run `ng serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng g component my-component --project=ryax` to generate a new component.

Run `ng g component my-component --project=ryax-design` to generate a new component in the UI library
(this should automatically create a storybook component who implements it as well).

## Build

Run `ng build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `ng e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx dep-graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev/angular) to learn more.
