const pug = require('pug');

function transformPugToHtml(src) {
  const html = pug.render(src, { doctype: 'html' });
  const content = JSON.stringify(html);
  return `module.exports = ${content};`;
}

module.exports = {
  process(src) {
    return transformPugToHtml(src);
  },
};
