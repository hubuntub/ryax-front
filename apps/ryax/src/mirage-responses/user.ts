// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserDto } from '../app/modules/auth/entities/dtos/user.dto';
import { Roles } from '../app/modules/auth/entities/index';

const user: UserDto = {
  "comment": "Default ryax dev1 user",
  "email": "david@ryax.tech",
  "username": "user1",
  "role": Roles.ADMIN,
  "id": "d8ec15d7-aa55-4e1b-9ebb-730cee1626bb"
}

export default user;
