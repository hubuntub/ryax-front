// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DeploymentStatusDto } from '../app/modules/shared/entity/dtos/deployment-status.dto';
import { WorkflowDto } from '../app/modules/workflow/entities/dtos/workflow.dto';

const workflowList: WorkflowDto[] = [
  {
    id: '12',
    name: 'Test',
    description: 'This is what is does',
    has_form: true,
    deployment_status: DeploymentStatusDto.NONE,
    status: 'Valid',
    endpoint: '/test',
    modules: [
      {
        id: '32',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Form',
        technical_name: 'Form',
        version: '1.0.0',
        description: 'Survey form',
        kind: "SOURCE",
        inputs: [],
        outputs: [],
      },
      {
        id: '34',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Treatment',
        technical_name: 'Treatment',
        version: '2.1.0',
        description: 'Change it into whatever you want',
        kind: "PROCESSOR",
        inputs: [],
        outputs: [],
      }
    ]
  },
  {
    id: '13',
    name: 'Test 2',
    description: 'This is what is does 2',
    has_form: true,
    deployment_status: DeploymentStatusDto.DEPLOYED,
    status: 'Valid',
    endpoint: '/test',
    modules: [
      {
        id: '33',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Form2',
        technical_name: 'Form2',
        version: '1.0.1',
        description: 'Survey form 2',
        kind: "SOURCE",
        inputs: [],
        outputs: [],
      }
    ]
  }
];

export default workflowList;
