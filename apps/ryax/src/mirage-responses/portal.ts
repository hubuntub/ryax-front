// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowPortalDto } from '../app/modules/portal/entities/dtos/index';

const portal: WorkflowPortalDto = {
  name: 'Integrated Form',
  workflow_definition_id: '68197cdf-d4f6-40dd-99bb-2c867e4e09cd',
  workflow_deployment_id: '68197cdf-d4f6-40dd-99bb-2c867e4e09cd',
  outputs: [
    {
      "technical_name": "qjlsdjqlksdhqkjfhkqjd",
      "id": "sdjokfqjkdjqlduo",
      "type": "INTEGER",
      "enum_values": [],
      "display_name": "Test",
      "help": "New output help1"
    },
    {
      "technical_name": "7946258f-fff0-4c41-919f-1163ea97e904",
      "id": "156b2881-0b34-4ec3-9707-4eb552227b15",
      "type": "FILE",
      "enum_values": [],
      "display_name": "Sales history",
      "help": "New output help"
    },
    {
      "technical_name": "a1668bc6-5272-4cfe-ae23-ba6025bcceb5",
      "id": "b9cbd2bd-37d7-4e6b-9113-7e110ebf097c",
      "type": "STRING",
      "enum_values": [],
      "display_name": "Email",
      "help": "New output help"
    },
    {
      "technical_name": "qlsidjqsokdjql",
      "id": "qlsdjqlkjlqskdjqlskdjqlsd",
      "type": "LONGSTRING",
      "enum_values": [],
      "display_name": "Some text",
      "help": "New output help"
    },
    {
      "technical_name": "qloisdjlsdjqlosdjlqsdj",
      "id": "dfosjdqjdlqksfjhosihgoihg",
      "type": "ENUM",
      "enum_values": ["squirtle", "charmender", "bulbasaur"],
      "display_name": "Choose your starter",
      "help": "New output help"
    },
    {
      "technical_name": "glksflkqhflskjdl",
      "id": "dfgkjqfsjdlfkjqpdokjgf",
      "type": "PASSWORD",
      "enum_values": [],
      "display_name": "Account password",
      "help": "New output help"
    },
    {
      "technical_name": "flksfjlskgdhjlkdsfjlqksjdlk",
      "id": "dfgjflqjsmldjsldhqld",
      "type": "DIRECTORY",
      "enum_values": [],
      "display_name": "All the images",
      "help": "New output help"
    }
  ]
};

export default portal;
