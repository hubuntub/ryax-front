// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowExecutionDto } from '../app/modules/shared/entity/dtos/index';

const workflowExecutionList: WorkflowExecutionDto[] = [{
  id: 'wfr-1643623888-2n1p',
  submitted_at: '2022-01-31T10:11:25.370842+00:00',
  started_at: '2022-01-31T10:11:28.439268+00:00',
  last_result_at: '2022-01-31T10:11:28.553841+00:00',
  total_steps: 3,
  completed_steps: 0,
  state: 'Created',
  workflow_definition_id: '8dcb9e9c-81ee-40d6-b6b8-bac60523f64e',
  executions: [
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Waiting',
      submitted_at_date: '2022-01-31T10:11:28.468314+00:00',
      started_at_date: '2022-01-31T10:11:28.552233+00:00',
      ended_at_date: '2022-01-31T10:11:28.553841+00:00',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Form Input',
        technical_name: 'Form Input',
        version: '1.0',
        kind: 'SOURCE',
        description: 'Form you input data in',
        inputs: [
          {
            id: 'name',
            type: 'string',
            display_name: 'Your name is ...',
            enum_values: [],
            help: '',
          }
        ],
        outputs: []
      },
      inputs: [
        {
          name: 'Your name is ...',
          value: ''
        }
      ],
      outputs: []
    },
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Waiting',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Disrupt reality',
        technical_name: 'Disrupt reality',
        version: '18.1.5',
        kind: 'PROCESSOR',
        description: 'Elaborate on your destiny',
        inputs: [],
        outputs: []
      },
      inputs: [
        {
          name: 'Your name is ...',
          value: ''
        }
      ],
      outputs: []
    },
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Waiting',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'TimeSpace Paste',
        technical_name: 'TimeSpace Paste',
        version: '-$.5.6b',
        description: 'Apply para-dimensional space modification',
        kind: 'PROCESSOR',
        inputs: [],
        outputs: []
      },
      inputs: [
        {
          name: 'Your name is ...',
          value: ''
        }
      ],
      outputs: []
    }
  ],
}, {
  id: 'wfr-1643623888-2n1p',
  submitted_at: '2022-01-31T10:11:25.370842+00:00',
  started_at: '2022-01-31T10:11:28.439268+00:00',
  last_result_at: '2022-01-31T10:11:28.553841+00:00',
  total_steps: 3,
  completed_steps: 1,
  state: 'Running',
  workflow_definition_id: '8dcb9e9c-81ee-40d6-b6b8-bac60523f64e',
  executions: [
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Success',
      submitted_at_date: '2022-01-31T10:11:28.468314+00:00',
      started_at_date: '2022-01-31T10:11:28.552233+00:00',
      ended_at_date: '2022-01-31T10:11:28.553841+00:00',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Form Input',
        technical_name: 'form',
        version: '1.0',
        kind: 'SOURCE',
        description: 'Form you input data in',
        inputs: [{
          id: 'name',
          type: 'string',
          display_name: 'Your name is ...',
          help: '',
          value: '',
          enum_values: []
        }],
        outputs: [{
          id: 'name',
          type: 'string',
          display_name: 'Your name is ...',
          help: '',
          value: 'Samantha',
          enum_values: []
        }]
      },
      inputs: [{
        name: 'Your name is ...',
        value: '',
      }],
      outputs: [{
        name: 'Your name is ...',
        value: 'Samantha',
      }],
    },
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Running',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Disrupt reality',
        technical_name: 'disrupt',
        version: '18.1.5',
        kind: 'PROCESSOR',
        description: 'Elaborate on your destiny',
        inputs: [{
          id: 'name',
          type: 'string',
          display_name: 'Your name is ',
          help: '',
          value: 'Samantha',
          enum_values: []
        }],
        outputs: []
      },
      inputs: [{
        value: 'Samantha',
        name: 'Your name is '
      }],
      outputs: [],
    },
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Waiting',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'TimeSpace Paste',
        technical_name: 'time-space-paste',
        version: '-$.5.6b',
        description: 'Apply para-dimensional space modification',
        kind: 'PROCESSOR',
        inputs: [],
        outputs: []
      },
      inputs: [],
      outputs: []
    }
  ]
}, {
  id: 'wfr-1643623888-2n1p',
  submitted_at: '2022-01-31T10:11:25.370842+00:00',
  started_at: '2022-01-31T10:11:28.439268+00:00',
  last_result_at: '2022-01-31T10:11:28.553841+00:00',
  total_steps: 3,
  completed_steps: 3,
  state: 'Completed',
  workflow_definition_id: '8dcb9e9c-81ee-40d6-b6b8-bac60523f64e',
  executions: [
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Success',
      submitted_at_date: '2022-01-31T10:11:28.468314+00:00',
      started_at_date: '2022-01-31T10:11:28.552233+00:00',
      ended_at_date: '2022-01-31T10:11:28.553841+00:00',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Form Input',
        technical_name: 'form',
        version: '1.0',
        kind: 'SOURCE',
        description: 'Form you input data in',
        inputs: [{
          id: 'name',
          type: 'string',
          display_name: 'Your name is ...',
          help: '',
          value: '',
          enum_values: []
        }],
        outputs: [{
          id: 'name',
          type: 'string',
          display_name: 'Your name is ...',
          help: '',
          value: 'Samantha',
          enum_values: []
        }]
      },
      inputs: [{
        name: 'Your name is ...',
        value: '',
      }],
      outputs: [{
        name: 'Your name is ...',
        value: 'Samantha',
      }],
    },
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Success',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'Disrupt reality',
        technical_name: 'disrupt',
        version: '18.1.5',
        kind: 'PROCESSOR',
        description: 'Elaborate on your destiny',
        inputs: [{
          id: 'name',
          type: 'string',
          display_name: 'Your name is ',
          help: '',
          value: 'Samantha',
          enum_values: []
        }],
        outputs: [{
          id: 'role',
          value: 'Queen of England',
          type: 'string',
          display_name: 'You will become ',
          help: '',
          enum_values: []
        }]
      },
      inputs: [{
        value: 'Samantha',
        name: 'Your name is '
      }],
      outputs: [{
        name: 'You will become ',
        value: 'Queen of England',
      }],
    },
    {
      id: '76e2855c-037c-48ec-9482-69e385998c0e',
      state: 'Success',
      module: {
        id: '2520289e-5080-4d70-9377-0dd18a121962',
        module_id: '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
        name: 'TimeSpace Paste',
        technical_name: 'time-space-paste',
        version: '-$.5.6b',
        description: 'Apply para-dimensional space modification',
        kind: 'PROCESSOR',
        inputs: [
          {
            id: 'name',
            value: 'Samantha',
            type: 'string',
            display_name: 'Your name is',
            help: '',
            enum_values: []
          },
          {
            id: 'role',
            value: 'Queen of England',
            type: 'string',
            display_name: 'You will become ',
            help: '',
            enum_values: []
          }
        ],
        outputs: [{
          id: 'name',
          value: '0.1%',
          type: 'string',
          display_name: 'Chance of paste success: ',
          help: '',
          enum_values: []
        }]
      },
      inputs: [],
      outputs: []
    }
  ],
}];

export default workflowExecutionList;
