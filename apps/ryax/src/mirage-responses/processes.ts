// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowModule } from '../app/modules/studio/entities/workflow-module';

export const processes: WorkflowModule[] = [
  {
    id: 'process-id-14',
    name: 'HTTP response',
    kind: 'Process',
    description: 'Return an answer',
    mainValue: '',
    configurator: [
    ]
  },
  {
    id: 'process-id-15',
    name: 'PostgreSQL request',
    kind: 'Process',
    description: 'Act on your DB',
    mainValue: '',
    configurator: [
    ]
  },
  {
    id: 'process-id-16',
    name: 'Video filter',
    kind: 'Process',
    description: 'Apply video filter',
    mainValue: '',
    configurator: []
  },
  {
    id: 'process-id-17',
    name: 'Split a video in images',
    kind: 'Process',
    description: 'Useful for image detection',
    mainValue: '',
    configurator: [
    ]
  },
  {
    id: 'process-id-18',
    name: 'Send Discord message',
    kind: 'Process',
    description: 'Communicate with whomever you need',
    mainValue: '',
    configurator: [
    ]
  },
  {
    id: 'process-id-19',
    name: 'Send Email',
    kind: 'Process',
    description: 'Communicate with whomever you need',
    mainValue: 'email',
    configurator: [
      {
        id: 'email',
        inputType: 'text',
        name: 'Mail address',
        help: 'my-address@mail.com',
        required: true,
      }
    ]
  },
  {
    id: 'process-id-20',
    name: 'Detect Objects',
    kind: 'Process',
    description: 'From any image',
    mainValue: '',
    configurator: [
    ]
  }
];
