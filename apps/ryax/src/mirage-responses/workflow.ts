// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DeploymentStatusDto } from '../app/modules/shared/entity/dtos/deployment-status.dto';
import { WorkflowDto } from '../app/modules/workflow/entities/dtos/workflow.dto';

const workflow: WorkflowDto = {
  'id': '02cc8157-a65d-4874-9fdb-36d592095369',
  'description': 'You can print anything on reality.',
  'status': 'Valid',
  'has_form': false,
  'endpoint': '/test',
  'modules_links': [{
    'output_module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'input_module_id': '8685793e-8d6c-4bd6-907a-ea09126456b1',
    'id': 'c137fbe0-e4a6-48d3-ba52-f191e3f9bc02'
  }, {
    'output_module_id': '8685793e-8d6c-4bd6-907a-ea09126456b1',
    'input_module_id': '8eba3f34-0939-4ca9-834f-74ccf3e65f9c',
    'id': 'fe34bbbb-4de0-4017-9c35-bdcacc914828'
  }, {
    'output_module_id': '9b4908d3-2f7b-4457-90ba-9ba33de4fcc9',
    'input_module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'id': '4e29b1b2-3acd-4e9d-bab2-2d9fca476098'
  }, {
    'output_module_id': '412249d9-202d-4728-b20a-7cc4c2160195',
    'input_module_id': '9b4908d3-2f7b-4457-90ba-9ba33de4fcc9',
    'id': 'c9cebd25-c24b-49dd-b4ef-6f05de1b1681'
  }],
  'modules': [{
    'version': '1.1',
    'id': '412249d9-202d-4728-b20a-7cc4c2160195',
    'module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'description': 'Print the environment of the module: all the environments variables and all the files.',
    'kind': 'Processor',
    'name': 'Print Environment',
    'technical_name': 'print-env',
    'inputs': [],
    'outputs': []
  }, {
    'version': '1.1',
    'id': '8685793e-8d6c-4bd6-907a-ea09126456b1',
    'module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'description': 'Print the environment of the module: all the environments variables and all the files.',
    'kind': 'Processor',
    'name': 'Print Environment',
    'technical_name': 'print-env',
    'inputs': [],
    'outputs': []
  }, {
    'version': '1.1',
    'id': '8eba3f34-0939-4ca9-834f-74ccf3e65f9c',
    'module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'description': 'Launches a single execution of the workflow',
    'kind': 'Source',
    'name': 'Run one execution',
    'technical_name': 'one-run',
    'inputs': [],
    'outputs': []
  }, {
    'version': '1.2',
    'id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'description': 'Sleep for a given number of seconds',
    'kind': 'Processor',
    'name': 'Sleep',
    'technical_name': 'sleep',
    'inputs': [],
    'outputs': []
  }, {
    'version': '1.0',
    'id': '9b4908d3-2f7b-4457-90ba-9ba33de4fcc9',
    'module_id': '96144c70-5f43-4895-8acc-f1bf8ebe2b08',
    'description': 'Get the content of a file and put in the output of this module, as a string.',
    'kind': 'Publisher',
    'custom_name': 'Cat content of a file',
    'name': 'any',
    'technical_name': 'catfile',
    'inputs': [],
    'outputs': []
  }],
  'deployment_status': DeploymentStatusDto.DEPLOYING,
  'name': 'MegaLife printer'
}

export default workflow;
