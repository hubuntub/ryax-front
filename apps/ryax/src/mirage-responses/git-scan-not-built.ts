// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { GitScan } from '../app/modules/studio/entities/git-scan';

export const gitScanNotBuilt: GitScan = {
  'name': 'default-modules',
  'id': '57c14ccd-a9b4-4754-bbc5-be2c7cfe3bb7',
  'url': 'https://gitlab.com/ryax-tech/workflows/default-modules.git',
  'lastScan': {
      'builtModules': [],
      'notBuiltModules': [{
        'sha': 'e1e22246ce698531c0a9619d64ab9639b71db0e4',
        'dynamicOutputs': false,
        'description': 'Archive a directory into a zip file.',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.669212',
        'type': 'python3',
        'name': 'Archive a directory',
        'id': '8762f90f-4b73-4c63-8c36-2acb6a97984f',
        'kind': 'Publisher',
        'metadataPath': 'default_modules/publishers/dir_archive/ryax_metadata.yaml',
        'technicalName': 'dir-archive',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.1'
      }, {
        'sha': 'e1e22246ce698531c0a9619d64ab9639b71db0e4',
        'dynamicOutputs': false,
        'description': 'Create a new execution at a given rate',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.733963',
        'type': 'python3',
        'name': 'Emit Every',
        'id': 'bb844460-044d-4ca5-b13a-7985b56761dc',
        'kind': 'Source',
        'metadataPath': 'default_modules/sources/emitgw/ryax_metadata.yaml',
        'technicalName': 'emitevery',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.2'
      }, {
        'sha': 'e27af3c3d16ca73ca9571667f0f6a32e7469db15',
        'dynamicOutputs': true,
        'description': 'Use this source to trigger executions using a online form. You can choose the fields of the form in the workflow studio, by changing the outputs of this module.',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.788891',
        'type': 'python3',
        'name': 'Integrated Form',
        'id': 'ca5ae36a-02c1-4abd-aa41-774aea702cb8',
        'kind': 'Source',
        'metadataPath': 'default_modules/sources/postgw/ryax_metadata.yaml',
        'technicalName': 'postgw',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.4'
      }, {
        'sha': 'e1e22246ce698531c0a9619d64ab9639b71db0e4',
        'dynamicOutputs': false,
        'description': 'Print the environment of the module: all the environements variables and all the files.',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.621521',
        'type': 'python3',
        'name': 'Print Environment',
        'id': 'c530a2c8-ef23-4fe7-ad4b-4520e0de56b3',
        'kind': 'Processor',
        'metadataPath': 'default_modules/processors/print_env/ryax_metadata.yaml',
        'technicalName': 'print-env',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.1'
      }, {
        'sha': '0b8edb5524a8869b3a439ebe860df73bec80a971',
        'dynamicOutputs': false,
        'description': 'Sleep for a given number of seconds',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.631071',
        'type': 'python3',
        'name': 'Sleep',
        'id': '33d18c2c-3ee5-4cfe-9c98-70c848ba9e95',
        'kind': 'Processor',
        'metadataPath': 'default_modules/processors/sleep/ryax_metadata.yaml',
        'technicalName': 'sleep',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.2'
      }, {
        'sha': '57e78a0d2fbe8d1b1c229f94b58d33ac99c5c99a',
        'dynamicOutputs': false,
        'description': 'Write the input string to a text file and output it',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.647642',
        'type': 'python3',
        'name': 'String to Text File',
        'id': 'ce4ae1ac-07a1-4ca5-8248-120041c5fe1b',
        'kind': 'Processor',
        'metadataPath': 'default_modules/processors/string_to_file/ryax_metadata.yaml',
        'technicalName': 'str-to-txt',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.0'
      }, {
        'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
        'dynamicOutputs': false,
        'description': 'Unpack a zipfile.',
        'buildError': {},
        'creationDate': '2022-03-18T13:10:03.652333',
        'type': 'python3',
        'name': 'Unpack zipfile',
        'id': 'a90d853a-4876-416b-bd54-1c568194a726',
        'kind': 'Processor',
        'metadataPath': 'default_modules/processors/unpack_archive/ryax_metadata.yaml',
        'technicalName': 'unpack-archive',
        'scanErrors': '',
        'status': 'Ready',
        'version': '1.0'
      },
      {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Get the content of a file and put in the output of this module, as a string.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.656613',
      'type': 'python3',
      'name': 'Cat content of a file',
      'id': '403cddc3-3452-4f37-8708-63ff5b1d9a3e',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/catfile/ryax_metadata.yaml',
      'technicalName': 'catfile',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'e27af3c3d16ca73ca9571667f0f6a32e7469db15',
      'dynamicOutputs': false,
      'description': 'Trigger a new execution with regards to the cron expression provided.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.726434',
      'type': 'python3',
      'name': 'Cron source',
      'id': '366c515c-b5b4-4aa7-b2d8-a2727b842c84',
      'kind': 'Source',
      'metadataPath': 'default_modules/sources/crongw/ryax_metadata.yaml',
      'technicalName': 'crongw',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Download file using given object key from s3 bucket and matching strategy',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.593663',
      'type': 'python3',
      'name': 'Download an object from AWS S3 Bucket.',
      'id': '5ec7e010-4870-4b35-8a95-60283c03f528',
      'kind': 'Processor',
      'metadataPath': 'default_modules/processors/download_file_from_bucket/ryax_metadata.yaml',
      'technicalName': 'download-from-bucket',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.1'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Compare 2 integers. If the comparison fails, no further executions are started.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.613094',
      'type': 'python3',
      'name': 'Filter integer',
      'id': 'ecd9e67c-0908-4bbe-b9c4-98d0211ae222',
      'kind': 'Processor',
      'metadataPath': 'default_modules/processors/filter_integer/ryax_metadata.yaml',
      'technicalName': 'filter-integer',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.1'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Push file and directory to Google Cloud Storage',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.682824',
      'type': 'python3',
      'name': 'Google cloud storage',
      'id': 'cb1f5a7f-3ba3-46de-a6ea-1a11e898cf4f',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/gcp_storage_writer/ryax_metadata.yaml',
      'technicalName': 'gcp-storage-writer',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'e27af3c3d16ca73ca9571667f0f6a32e7469db15',
      'dynamicOutputs': false,
      'description': 'Ingest data from an MQTT server. Each message on the given topic will trigger an execution.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.772299',
      'type': 'python3',
      'name': 'MQTT Source',
      'id': '056bfb0a-e5ed-44c9-a2a0-d6c970170b01',
      'kind': 'Source',
      'metadataPath': 'default_modules/sources/mqttgw/ryax_metadata.yaml',
      'technicalName': 'mqttgw',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Publish a tweet message on twitter. Create a twitter app from apps.twiter.com and then generate access token from "generate the Access Tokens" menu',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.714548',
      'type': 'python3',
      'name': 'Publish a tweet on twitter',
      'id': '2f34e612-438a-488f-9ed2-14a09b5413c7',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/twitter_alerting/ryax_metadata.yaml',
      'technicalName': 'twitter-alerting',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Publish notifications on slack channel with Apprise. Generate Webhook URL from https://my.slack.com/services/new/incoming-webhook/',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.708752',
      'type': 'python3',
      'name': 'Publish alert on slack channel',
      'id': 'ea015f3f-f2ae-4e7f-a3f5-1bd90fe8c1f7',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/slack_alerting/ryax_metadata.yaml',
      'technicalName': 'slack-alerting',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Save a float to an influxdb instance.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.675418',
      'type': 'python3',
      'name': 'Push a float into influxdb',
      'id': 'fc5ee848-8d71-4639-82f3-0d73b8405f8c',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/floattoinfluxdb/ryax_metadata.yaml',
      'technicalName': 'floattoinfluxdb',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.1'
    }, {
      'sha': 'e1e22246ce698531c0a9619d64ab9639b71db0e4',
      'dynamicOutputs': false,
      'description': 'Launches a single execution of the workflow',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.781119',
      'type': 'python3',
      'name': 'Run one execution',
      'id': '24c58fd5-2529-42ea-bebf-a1cf2a0c6cb5',
      'kind': 'Source',
      'metadataPath': 'default_modules/sources/one_run/ryax_metadata.yaml',
      'technicalName': 'one-run',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.1'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Make an SQL query and return the result in a file.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.638855',
      'type': 'python3',
      'name': 'SQL Query',
      'id': '855afd76-9008-45c7-93ae-8238ea73d88e',
      'kind': 'Processor',
      'metadataPath': 'default_modules/processors/sql_query/ryax_metadata.yaml',
      'technicalName': 'sql-query',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Publish a message to cliq channel.',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.662021',
      'type': 'python3',
      'name': 'Send a message to cliq',
      'id': '9b07a0e0-1cd2-4848-b0ca-a6916709150a',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/cliq_message/ryax_metadata.yaml',
      'technicalName': 'cliq-message',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'This module sends email using an SMTP server (doesn\'t support Gmail host).',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.688795',
      'type': 'python3',
      'name': 'Send e-mail',
      'id': 'a2c40f5c-b78b-4083-9e21-701129f5464b',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/send_email/ryax_metadata.yaml',
      'technicalName': 'send-email',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.1'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'This module sends email with attachment file using an SMTP server (doesn\'t support Gmail host).',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.700295',
      'type': 'python3',
      'name': 'Send e-mail with attachment file',
      'id': 'c4510b06-ae35-4569-bd8b-3e7944fcc74b',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/send_email_with_attachment/ryax_metadata.yaml',
      'technicalName': 'send-email-attachment',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.1'
    }, {
      'sha': 'e27af3c3d16ca73ca9571667f0f6a32e7469db15',
      'dynamicOutputs': false,
      'description': 'This module is triggered when a new merge request is created, an existing merge request was updated/merged/closed or a commit is added in the source branch on Gitlab. Use this url to create an event : https://gitlab.com/file/project/-/hooks. Select Merge requests events and set the url of the module and the secret token from inputs. ',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.744499',
      'type': 'python3',
      'name': 'Trigger workflow From Gitlab merge request',
      'id': 'c4836df4-a127-4cab-8bc4-c8cf98e70ca1',
      'kind': 'Source',
      'metadataPath': 'default_modules/sources/gitlab_mr/ryax_metadata.yaml',
      'technicalName': 'gitlab-mr',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'e27af3c3d16ca73ca9571667f0f6a32e7469db15',
      'dynamicOutputs': false,
      'description': 'This module is triggered when A Gitlab pipeline status changes. Use this url to create an event : https://gitlab.com/file/project/-/hooks. Select Pipeline events and set the url of the module and the secret token from inputs. ',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.751487',
      'type': 'python3',
      'name': 'Trigger workflow From Gitlab pipeline',
      'id': '00b942aa-615f-4d38-872d-5cf5b2ddc9aa',
      'kind': 'Source',
      'metadataPath': 'default_modules/sources/gitlab_pipeline/ryax_metadata.yaml',
      'technicalName': 'gitlab-pipeline',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'e27af3c3d16ca73ca9571667f0f6a32e7469db15',
      'dynamicOutputs': false,
      'description': 'This module is triggered when a release is created or updated on Gitlab. Use this url to create an event : https://gitlab.com/file/project/-/hooks. Select Release events and set the url of the module and the secret token from inputs. ',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.758617',
      'type': 'python3',
      'name': 'Trigger workflow From Gitlab release',
      'id': '8692d3ea-946d-41ec-bc59-df055fe270e5',
      'kind': 'Source',
      'metadataPath': 'default_modules/sources/gitlab_release/ryax_metadata.yaml',
      'technicalName': 'gitlab-release',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }, {
      'sha': 'b30318dd818da23ddb7446a2c5c28dda28ad8ee0',
      'dynamicOutputs': false,
      'description': 'Upload input file to an AWS Bucket',
      'buildError': {},
      'creationDate': '2022-03-18T13:10:03.719637',
      'type': 'python3',
      'name': 'Upload file to aws',
      'id': 'a4eecccc-335f-4f3c-a5ab-d1cba5eb76b8',
      'kind': 'Publisher',
      'metadataPath': 'default_modules/publishers/upload_file_to_s3/ryax_metadata.yaml',
      'technicalName': 'upload-to-bucket',
      'scanErrors': '',
      'status': 'Ready',
      'version': '1.0'
    }],
    'sha': '57e78a0d2fbe8d1b1c229f94b58d33ac99c5c99a',
  }
}
