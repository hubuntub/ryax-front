// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ProjectDto } from '../app/modules/project/entities/dtos/index';

const projects: ProjectDto[] = [
  {
    id: '8769Ibdoqiy',
    name: 'First project',
    creation_date: new Date()
  },
  {
    id: '8769Ibdoqiy',
    name: 'Second project',
    creation_date: new Date(),
    current: true
  }]

export default projects;
