// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowModule } from '../app/modules/studio/entities/workflow-module';

export const sources: WorkflowModule[] = [
  {
    id: 'source-id-12',
    name: 'Serve static files',
    kind: 'Source',
    description: 'Plainly hosted',
    mainValue: 'domain_url',
    configurator: [
      {
        id: 'domain_url',
        name: 'Domain url',
        inputType: 'text',
        help: 'my-website.com',
        required: true,
      }
    ]
  },
  {
    id: 'source-id-13',
    name: 'Http API endpoint',
    kind: 'Source',
    description: 'Basic backend component',
    mainValue: 'endpoint_url',
    configurator: [
      {
        id: 'endpoint_url',
        name: 'Endpoint url',
        inputType: 'text',
        help: '/articles',
        required: true,
      }
    ]
  },
  {
    id: 'source-id-14',
    name: 'On a new Form request',
    kind: 'Source',
    description: 'Allow people to start your workflow from a form',
    mainValue: '',
    configurator: []
  },
  {
    id: 'source-id-15',
    name: 'Moon cycle',
    kind: 'Source',
    description: 'Triggers when the time comes',
    mainValue: 'cycle',
    configurator: [
      {
        id: 'cycle',
        name: 'Cycle event',
        inputType: 'select',
        help: 'Choose your favorite',
        required: true,
        options: [
          {
            name: 'New moon',
            value: 'new_moon',
          },
          {
            name: 'Full moon',
            value: 'full_moon',
          },
          {
            name: 'First quarter',
            value: 'quarter',
          }
        ]
      }
    ]
  },
  {
    id: 'source-id-16',
    name: 'On a new MQTT message',
    kind: 'Source',
    description: 'Do things when you receive one',
    mainValue: '',
    configurator: [
      {
        id: 'mqtt',
        inputType: 'Oauth',
        name: 'MQTT association',
      }
    ]
  },
  {
    id: 'source-id-17',
    name: 'On a new Hubspot client',
    kind: 'Source',
    description: 'Do things when someone subs',
    mainValue: '',
    configurator: [
      {
        id: 'hubspot',
        inputType: 'Oauth',
        name: 'Hubspot association',
      }
    ]
  },
  {
    id: 'source-id-18',
    name: 'On Twitter direct message',
    kind: 'Source',
    description: 'Do things when you receive one',
    mainValue: '',
    configurator: [
      {
        id: 'twitter',
        inputType: 'Oauth',
        name: 'Twitter association',
      }
    ]
  },
  {
    id: 'source-id-19',
    name: 'On a Github event',
    kind: 'Source',
    description: 'Triggers on some git actions',
    mainValue: '',
    configurator: [
      {
        id: 'github',
        inputType: 'Oauth',
        name: 'Github association',
      }
    ]
  },
  {
    id: 'source-id-20',
    name: 'On a Timed Schedule',
    kind: 'Source',
    description: 'Triggers when the time comes',
    mainValue: 'recurrence',
    configurator: [
      {
        id: 'recurrence',
        name: 'Recurrence',
        inputType: 'select',
        required: true,
        options: [
          { name: 'Every Monday', value: 'mondays' },
          { name: 'Every Tuesday', value: 'tuesdays' },
          { name: 'Every Wednesday', value: 'wednesdays' },
          { name: 'Every Thursday', value: 'thursdays' },
          { name: 'Every Friday', value: 'fridays' },
          { name: 'Every Saturday', value: 'saturdays' },
          { name: 'Every Sunday', value: 'sundays' },
        ]
      },
      {
        id: 'day_time',
        name: 'Day Time',
        inputType: 'text',
        help: 'HH:MM',
        required: true,
      }
    ]
  },
  {
    id: 'source-id-21',
    name: 'On a new message in Discord',
    kind: 'Source',
    description: 'Do things when you receive one',
    mainValue: '',
    configurator: [
      {
        id: 'discord',
        inputType: 'Oauth',
        name: 'Discord association',
      }
    ]
  },
  {
    id: 'source-id-22',
    name: 'On an event id Google Calendar',
    kind: 'Source',
    description: 'Triggers on your own events',
    mainValue: '',
    configurator: [
      {
        id: 'google',
        inputType: 'Oauth',
        name: 'Google association',
      }
    ]
  },
];
