// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createServer } from 'miragejs';
import { Response } from 'miragejs';
import portal from './mirage-responses/portal'
import workflowList from './mirage-responses/workflow-list';
import workflow from './mirage-responses/workflow';
import workflowExecutionList from './mirage-responses/workflow-execution-list';
import user from "./mirage-responses/user";
import projects from "./mirage-responses/projects";
import { gitScanNotBuilt } from './mirage-responses/git-scan-not-built';
import { sources } from './mirage-responses/sources';
import { processes } from './mirage-responses/processes';

function later(delay: number, value: unknown) {
  return new Promise(resolve => setTimeout(resolve, delay, value));
}

export function initMirage() {
  let stepTree = 0;

  createServer({
    routes() {
      this.passthrough();
      this.namespace = '/api';

      this.post("/authorization/login", (schema, req) => {
        const credentials = JSON.parse(req.requestBody);
        if (credentials.username === 'user1' && credentials.password === 'pass1') {
          return later(200, {"jwt": "MonJWT"});
        } else {
          return new Response(400, { some: 'header' }, { errors: [ 'name cannot be blank'] });
        }
      });

      this.post("/runner/portals/:portalId",() => {
        return {
          "name": "Ryax Post Gateway",
          "execution_id": "exec-1637660557-az0k",
          "data": {"50f5785b-02df-4d9e-b807-512dcfbbb46f": "2"}
        }
      });

      this.get("/runner/workflow_runs/:execId", () => {
        stepTree++;
        if (stepTree === 1) {
          return workflowExecutionList[stepTree - 1];
        } else if (stepTree === 2) {
          return workflowExecutionList[stepTree - 1];
        } else {
          return workflowExecutionList[2];
        }
      });

      this.get("/runner/portals/:workflowId", () => {
        return portal
      })

      this.get("/studio/workflows", () => {
        return later(400, workflowList);
      })

      this.get("/studio/workflows/:workflowId", () => {
        return later(400, workflow);
      })

      this.get("/runner/workflow_runs", () => {
        return later(200, new Response(200, {'content-range': '1-3/3'}, workflowExecutionList));
      })

      this.get("/authorization/me", () => {
        return later(200, user);
      })

      this.get("/authorization/v2/projects", () => {
        return later(200, projects);
      })

      this.get("/sources",() => {
        return sources;
      });

      this.get("/processes",() => {
        return processes;
      });

      this.get("/repos", () => {
        return [];
      });

      this.get("/repos/new", () => {
        return gitScanNotBuilt;
      });

      this.post("/build", (schema, request) => {
        const attrs = JSON.parse(request.requestBody)

        console.log(attrs)

        return new Response(200);
      });
    }
  });
}
