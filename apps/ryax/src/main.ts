// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { initMirage } from './mirage-config';

if (environment.production) {
  enableProdMode();
}

if (environment.mirage) {
  initMirage();
}

if (environment.demo) {
  const script1 = document.createElement('script');
  script1.async = true;
  script1.setAttribute('src', 'https://www.googletagmanager.com/gtag/js?id=G-MFQLE1NNHT');
  const script2 = document.createElement('script');
  script2.innerText = `window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date());`;
  document.getElementsByTagName('head')[0].append(script1, script2);
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));
