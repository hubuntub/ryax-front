// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { AuthFacade } from '../../state/facade';

@Component({
  selector: 'ryax-auth-login',
  templateUrl: './auth-login.component.pug',
  styleUrls: ['./auth-login.component.scss']
})
export class AuthLoginComponent {

  public passwordVisible = false;
  public error$ = this.authFacade.error$;
  public loading$ = this.authFacade.loading$;
  public displayDemo = environment.demo;

  public authForm = this.fb.group({
    username: this.fb.control(null, Validators.required),
    password: this.fb.control(null, Validators.required)
  });

  constructor(
    private fb: FormBuilder,
    private authFacade: AuthFacade
  ) {}

  public onSubmit() {
    if (this.authForm.valid) {
      this.authFacade.login(this.authForm.value);
    } else {
      // Afficher une erreur
    }
  }

}
