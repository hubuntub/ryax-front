// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { hot } from 'jest-marbles';
import { of } from 'rxjs';
import { AuthFacade } from '../../state/facade/index';

import { IsLoggedComponent } from './is-logged.component';

describe('IsLoggedComponent', () => {
  let component: IsLoggedComponent;
  let fixture: ComponentFixture<IsLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsLoggedComponent ],
      providers: [
        { provide: AuthFacade, useValue: { token$: of('token')} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show content when user roles are matching', () => {
    expect(component.isLogged()).toBeObservable(hot('(a|)',{a: true}));
  });

  it('should hide content when user roles are not matching', () => {
    expect(component.isLogged()).toBeObservable(hot('(a|)',{a: false}));
  });
});
