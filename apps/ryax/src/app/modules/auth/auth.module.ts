// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';
import { AuthLoginComponent } from './components/auth-login/auth-login.component';
import { HasRolesComponent } from './components/has-roles/has-roles.component';
import { IsLoggedComponent } from './components/is-logged/is-logged.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { AuthLoginApiService } from './services/auth-login-api/auth-login-api.service';
import { AutoLogoutInterceptor } from './services/auto-logout/auto-logout.interceptor';
import { UserApiService } from './services/user-api/user-api.service';
import { AuthLoginEffects } from './state/effects';
import { UserEffects } from './state/effects/user.effects';
import { AuthFeatureKey, AuthReducerProvider, AuthReducerToken } from './state/reducers';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(AuthFeatureKey, AuthReducerToken),
    EffectsModule.forFeature([AuthLoginEffects, UserEffects]),
    SharedModule,
    RouterModule
  ],
  declarations: [
    AuthLoginComponent,
    HasRolesComponent,
    IsLoggedComponent,
    UserInfoComponent,
  ],
  providers: [
    AuthReducerProvider,
    AuthLoginApiService,
    UserApiService,
    { provide: HTTP_INTERCEPTORS, useClass: AutoLogoutInterceptor, multi: true }
  ],
  exports: [
    HasRolesComponent,
    IsLoggedComponent,
    UserInfoComponent,
  ]
})
export class AuthModule { }
