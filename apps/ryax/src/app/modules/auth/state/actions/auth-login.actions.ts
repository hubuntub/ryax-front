// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { AccountToken, Credentials } from '../../entities';

export const login = createAction(
  '[Auth Login] Send credentials',
  (credentials: Credentials) => credentials
);

export const loginSuccess = createAction(
  '[Auth Login] Login success',
  (token: AccountToken) => ({ token })
);

export const loginError = createAction(
  '[Auth Login] Login error',
  (error: HttpErrorResponse) => ({ error })
);

export const getTokenFromStorage = createAction(
  '[Auth Login] Get Token from Storage',
  (token: AccountToken) => ({ token })
);

export const noTokenStorage = createAction(
  '[Auth Login] No Token in Storage'
);

export const logout = createAction(
  '[Auth Login] Logout'
);
