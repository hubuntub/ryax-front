// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { AccountToken, Credentials } from '../../entities';
import { AuthLoginApiService } from '../../services/auth-login-api/auth-login-api.service';
import { AuthStorageService } from '../../services/auth-storage/auth-storage.service';
import { LoginActions, UserActions } from '../actions';


@Injectable()
export class AuthLoginEffects implements OnInitEffects {

  login$ = createEffect(() => this.actions$.pipe(
    ofType(LoginActions.login),
    switchMap((credentials: Credentials) => this.authLoginApi.login(credentials).pipe(
      map((token: AccountToken) => LoginActions.loginSuccess(token)),
      catchError((err: HttpErrorResponse) => {
        return of(LoginActions.loginError(err))
      })
    )),
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(LoginActions.loginSuccess),
    tap(({ token }) => {
      this.authStorage.saveToken(token);
      this.router.navigateByUrl('/dashboard');
    })
  ), { dispatch: false });

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(LoginActions.logout, UserActions.loadError),
    tap(() => {
      this.authStorage.clearToken();
      this.router.navigateByUrl('/login');
    })
  ), { dispatch: false });

  constructor(
    private actions$: Actions,
    private authLoginApi: AuthLoginApiService,
    private authStorage: AuthStorageService,
    private router: Router
  ) {}

  ngrxOnInitEffects() {
    const token = this.authStorage.getToken();
    if (token) {
      return LoginActions.getTokenFromStorage(token);
    } else {
      return LoginActions.noTokenStorage();
    }
  }
}
