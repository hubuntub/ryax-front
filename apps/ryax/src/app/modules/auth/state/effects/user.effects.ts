// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { User } from '../../entities/user';
import { UserApiService } from '../../services/user-api/user-api.service';
import { UserActions } from '../actions'

@Injectable()
export class UserEffects {
  user$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.get),
    switchMap(() => this.userApiService.getUser().pipe(
      map((user: User) => UserActions.loadSuccess(user)),
      catchError((err: HttpErrorResponse) => {
        return of(UserActions.loadError(err))
      })
    )),
  ));

  displayErrors$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.loadError),
    tap((errAction) => this.notification.create(
      'error',
      'Something went wrong ! http error : ' + errAction.error.status,
      errAction.error.message
    ))
  ), { dispatch: false });

  constructor(
    private actions$: Actions,
    private userApiService: UserApiService,
    private notification: NzNotificationService
  ) {
  }
}
