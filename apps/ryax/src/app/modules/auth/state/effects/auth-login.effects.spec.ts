// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Credentials } from '../../entities';
import { AuthLoginApiService } from '../../services/auth-login-api/auth-login-api.service';
import { AuthStorageService } from '../../services/auth-storage/auth-storage.service';
import { LoginActions } from '../actions';
import { AuthLoginEffects } from './index';


describe('AuthLoginEffects', () => {
  let effects: AuthLoginEffects;
  let actions$: Observable<Actions>;
  const credentials: Credentials = { username: 'user', password: 'pass' };
  const authLoginApiSpy = {
    login: jest.fn()
  }
  const authStorageSpy = {
    getToken: jest.fn(),
    saveToken: jest.fn(),
    clearToken: jest.fn()
  }
  const routeSpy = {
    navigateByUrl: jest.fn()
  }

  beforeEach(() => {
    authLoginApiSpy.login.mockClear();

    TestBed.configureTestingModule({
      providers: [
        AuthLoginEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        { provide: AuthLoginApiService, useValue: authLoginApiSpy },
        { provide: AuthStorageService, useValue: authStorageSpy },
        { provide: Router, useValue: routeSpy }
      ]
    });
    effects = TestBed.inject(AuthLoginEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should retrieve token from storage onInit', () => {
    authStorageSpy.getToken.mockImplementation(() => 'MyToken');

    expect(effects.ngrxOnInitEffects()).toEqual({ type: '[Auth Login] Get Token from Storage', token: 'MyToken' });
  });

  it('should handle well not having a token stored', () => {
    authStorageSpy.getToken.mockImplementation(() => null);

    expect(effects.ngrxOnInitEffects()).toEqual({ type: '[Auth Login] No Token in Storage' });
  });

  it('should try to login and return success when a token is received', () => {
    authLoginApiSpy.login.mockImplementation(() => cold('-a|', { a: 'MyToken' }));

    actions$ = hot('-a', {
      a: LoginActions.login(credentials)
    });

    const expected$ = hot('--a', { a: LoginActions.loginSuccess('MyToken') });

    expect(effects.login$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(authLoginApiSpy.login).toHaveBeenCalledWith({ 'type': '[Auth Login] Send credentials', ...credentials });
    });
  });

  it('should try to login and return error when an error is received', () => {
    authLoginApiSpy.login.mockImplementation(() => cold('-#', {}, { error: '400' }));

    actions$ = hot('-a', {
      a: LoginActions.login(credentials)
    });

    const expected$ = hot('--a', { a: LoginActions.loginError({ error: '400' } as HttpErrorResponse) });

    expect(effects.login$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(authLoginApiSpy.login).toHaveBeenCalledWith({ 'type': '[Auth Login] Send credentials', ...credentials });
    });
  });

  it('should save the token on login success and redirect user afterward', () => {
    actions$ = hot('-a', {
      a: LoginActions.loginSuccess('MyToken')
    });

    const expected$ = hot('-a', { a: { type: '[Auth Login] Login success', token: 'MyToken' } });

    expect(effects.loginSuccess$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(routeSpy.navigateByUrl).toHaveBeenCalledWith('/dashboard');
      expect(authStorageSpy.saveToken).toHaveBeenCalledWith('MyToken');
    });
  });

  it('should clear the token on logout and redirect user afterward', () => {
    actions$ = hot('-a', {
      a: LoginActions.logout()
    });

    const expected$ = hot('-a', { a: { type: '[Auth Login] Logout' } });

    expect(effects.logout$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(routeSpy.navigateByUrl).toHaveBeenCalledWith('/login');
      expect(authStorageSpy.clearToken).toHaveBeenCalled();
    });
  });
});
