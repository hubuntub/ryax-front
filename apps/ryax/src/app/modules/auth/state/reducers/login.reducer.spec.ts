// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { AccountToken, Credentials } from '../../entities';
import { LoginActions } from '../actions';
import * as fromLogin from './login.reducer';

describe('loginReducer', () => {
  const state: fromLogin.LoginState = fromLogin.initialAuthState;

  const credentials: Credentials = {
    username: 'username',
    password: 'password',
  };

  const token: AccountToken = 'token';

  const error: HttpErrorResponse = {
    name: 'HttpErrorResponse',
    message: 'message',
    error: 401
  } as HttpErrorResponse;

  it('on LoginActions.login', () => {
    const action = LoginActions.login(credentials);
    expect(fromLogin.loginReducer(state, action)).toEqual({
      ...state,
      loading: true,
      error: null
    });
  });

  it.each([
    LoginActions.loginSuccess(token),
    LoginActions.getTokenFromStorage(token)
  ])('on LoginActions.loginSuccess or LoginActions.getTokenFromStorage', (action) => {
    expect(fromLogin.loginReducer(state, action)).toEqual({
      ...state,
      token,
      loading: false,
      error: null
    });
  });

  it('on LoginActions.loginError', () => {
    const action = LoginActions.loginError(error);
    expect(fromLogin.loginReducer(state, action)).toEqual({
      ...state,
      loading: false,
      error
    });
  });

  it('on LoginActions.logout', () => {
    const action = LoginActions.logout();
    expect(fromLogin.loginReducer(state, action)).toEqual({
      ...state,
      token: null
    });
  });

});
