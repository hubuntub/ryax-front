// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AccountToken, Credentials } from '../../entities';

import { AuthLoginApiService } from './auth-login-api.service';

describe('AuthLoginApiService', () => {
  let service: AuthLoginApiService;
  let httpTestingController: HttpTestingController;

  const credentials: Credentials = {
    username: 'username',
    password: 'password',
  };
  const token: AccountToken = 'token';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        AuthLoginApiService,
      ],
    });
    service = TestBed.inject(AuthLoginApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should login', () => {
    expect(service).toBeTruthy();

    service.login(credentials).subscribe(response => {
      expect(response).toEqual(token);
    });

    const request = httpTestingController.expectOne('/authorization/login');
    expect(request.request.method).toEqual('POST');
    request.flush(token);
  });
});
