// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { AccountToken } from '../../entities';

import { AuthStorageService } from './auth-storage.service';

describe('AuthStorageService', () => {
  let service: AuthStorageService;

  const localStorageSpy = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    removeItem: jest.fn(),
    clear: jest.fn(),
    key: jest.fn(),
    length: 1,
  } as typeof localStorage;

  const token: AccountToken = 'token';

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', { value: localStorageSpy });
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get token from storage', () => {
    service.getToken();
    expect(localStorage.getItem).toHaveBeenCalledWith('RYAX_TOKEN');
  });

  it('should save token into storage', () => {
    service.saveToken(token);
    expect(localStorage.setItem).toHaveBeenCalledWith('RYAX_TOKEN', token);
  });

  it('should clean token from storage', () => {
    service.clearToken();
    expect(localStorage.removeItem).toHaveBeenCalledWith('RYAX_TOKEN');
  });
});
