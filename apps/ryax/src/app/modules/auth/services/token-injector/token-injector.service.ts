// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { flatMap } from 'rxjs/internal/operators/index';
import { catchError, map, withLatestFrom } from 'rxjs/operators';
import { AuthFacade } from '../../state/facade/index';

@Injectable()
export class TokenInjectorService implements HttpInterceptor {

  private static handleProcess(request: HttpRequest<unknown>, token: string | null) {
    if (token) {
      return request.clone({
        headers: request.headers.set("Authorization", token)
      });
    } else {
      return request;
    }
  }

  private static handleError(err: HttpErrorResponse): Observable<never> {
    return throwError(err);
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  private token$ = this.facade.token$;

  constructor(
    private facade: AuthFacade
  ) { }

  public intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return of(req).pipe(
      withLatestFrom(this.token$),
      map(([request, token]) => TokenInjectorService.handleProcess(request, token)),
      flatMap(request => next.handle(request)),
      catchError((e) => TokenInjectorService.handleError(e))
    );
  }
}

export const TokenInjectorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInjectorService,
  multi: true
};
