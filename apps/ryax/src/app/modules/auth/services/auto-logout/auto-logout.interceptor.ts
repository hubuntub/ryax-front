// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthFacade } from '../../state/facade/index';

@Injectable()
export class AutoLogoutInterceptor implements HttpInterceptor {

  constructor(
    private authFacade: AuthFacade
  ) { }

  public intercept(httpRequest: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(httpRequest).pipe(catchError((err) => {
      if (err.status === 401) {
        this.authFacade.logout();
      }
      return throwError(err);
    }));
  }
}
