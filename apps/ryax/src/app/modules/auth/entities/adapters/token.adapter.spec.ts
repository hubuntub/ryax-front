// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { LoginResponseDto } from '../dtos/login-response.dto';
import { AccountToken } from '../index';
import { TokenAdapter } from './token.adapter';

describe('WorkflowAdapter', () => {
  const tokenAdapter: TokenAdapter = new TokenAdapter();

  const dto: LoginResponseDto = {
    jwt: 'token'
  };

  const token: AccountToken = 'token';

  it('should adapt LoginResponseDto into AccountToken', () => {
    expect(tokenAdapter.adaptLoginResponse(dto)).toEqual(token);
  });
});
