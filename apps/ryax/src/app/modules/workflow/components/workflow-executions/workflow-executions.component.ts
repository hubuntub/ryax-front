// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { WorkflowFacade } from '../../state/facade';

@Component({
  selector: 'ryax-workflow-executions',
  templateUrl: './workflow-executions.component.pug',
  styleUrls: ['./workflow-executions.component.scss']
})
export class WorkflowExecutionsComponent {

  public executionList$ = this.facade.workflowExecutionList$;
  public executionTotal$ = this.facade.workflowExecutionLength$;
  public executionLoading$ = this.facade.workflowExecutionLoading$;


  constructor(
    private facade: WorkflowFacade
  ) { }

  public getListPart(params: NzTableQueryParams) {
    this.facade.getWorkflowExecutions(params.pageIndex - 1, params.pageSize);
  }

}
