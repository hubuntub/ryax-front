// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkflowFacade } from '../../state/facade';

import { WorkflowExecutionsComponent } from './workflow-executions.component';

describe('WorkflowExecutionsComponent', () => {
  let component: WorkflowExecutionsComponent;
  let fixture: ComponentFixture<WorkflowExecutionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkflowExecutionsComponent],
      providers: [
        {
          provide: WorkflowFacade, useValue: {}
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowExecutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
