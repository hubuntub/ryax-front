// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Roles } from '../../../auth/entities/index';
import { WorkflowFacade } from '../../state/facade';

@Component({
  selector: 'ryax-workflow',
  templateUrl: './workflow.component.pug',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {

  public workflow$ = this.facade.workflow$;
  public workflowStatus$ = this.facade.workflowStatus$;
  public workflowLoading$ = this.facade.workflowLoading$;
  public workflowExecutionLength$ = this.facade.workflowExecutionLength$;
  public resultsConfig$ = this.facade.resultsConfig$;
  public studioRoles = [Roles.ADMIN, Roles.DEVELOPER];

  constructor(
    private facade: WorkflowFacade,
    private route: ActivatedRoute,
    private modal: NzModalService
  ) { }

  public ngOnInit(): void {
    this.facade.getWorkflow(this.route.snapshot.params['workflowId']);
  }

  public deploy(): void {
    this.facade.deploy();
  }

  public stop(): void {
    this.modal.confirm({
      nzTitle: 'Are you sure you want to undeploy this workflow?',
      nzContent: 'This will cancel all running and waiting runs. ',
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOnOk: () => this.facade.stop(),
      nzCancelText: 'No'
    });
  }

  public refreshRuns(): void {
    this.facade.refreshWorkflowExecutions();
  }

  public export(): void {
    this.facade.export()
  }

  public duplicate(): void {
    this.facade.copy()
  }

  public delete(): void {
    this.modal.confirm({
      nzTitle: 'Are you sure you want to delete?',
      nzContent: 'This will delete the workflow, all runs and their data. ',
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => this.facade.delete(),
      nzCancelText: 'No'
    });
  }
}
