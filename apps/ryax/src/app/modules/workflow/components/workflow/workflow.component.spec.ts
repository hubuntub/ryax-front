// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { WorkflowFacade } from '../../state/facade';

import { WorkflowComponent } from './workflow.component';

describe('WorkflowComponent', () => {
  let component: WorkflowComponent;
  let fixture: ComponentFixture<WorkflowComponent>;

  const workflowFacadeSpy = {
    getWorkflow: jest.fn()
  }

  const fakeRoute = {
    snapshot: {
      params: {
        workflowId: 12
      }
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkflowComponent],
      providers: [
        {
          provide: WorkflowFacade, useValue: workflowFacadeSpy
        },
        {
          provide: ActivatedRoute, useValue: fakeRoute
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get workflow on init depending on route id', () => {
    expect(workflowFacadeSpy.getWorkflow).toHaveBeenCalledWith(12);
  });
});
