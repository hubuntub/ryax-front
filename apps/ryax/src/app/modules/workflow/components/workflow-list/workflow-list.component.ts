// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { WorkflowFacade } from '../../state/facade';

@Component({
  selector: 'ryax-workflow-list',
  templateUrl: './workflow-list.component.pug',
  styleUrls: ['./workflow-list.component.scss']
})
export class WorkflowListComponent implements OnInit {

  public workflowList$ = this.facade.workflowList$;
  public workflowListLoading$ = this.facade.workflowListLoading$;

  constructor(
    private facade: WorkflowFacade,
  ) { }

  public ngOnInit(): void {
    this.facade.getWorkflows();
  }

  public refreshList(): void {
    this.facade.getWorkflows();
  }

}
