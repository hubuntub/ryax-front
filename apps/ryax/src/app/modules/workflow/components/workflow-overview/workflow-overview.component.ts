// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { TimelineColors } from '../../../shared/entity';
import { WorkflowFacade } from '../../state/facade';

@Component({
  selector: 'ryax-workflow-overview',
  templateUrl: './workflow-overview.component.pug',
  styleUrls: ['./workflow-overview.component.scss']
})
export class WorkflowOverviewComponent {

  public workflow$ = this.facade.workflow$;
  public endpoint$ = this.facade.endpoint$;
  public timelineColor = TimelineColors.Purple;

  constructor(
    private facade: WorkflowFacade
  ) { }

}
