// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DateTime } from 'luxon';
import { DeploymentStatus } from '../../shared/entity';
import { WorkflowStep } from '../../studio/entities/workflow-step';
import { Module } from './module';

export interface Workflow {
  id: string;
  name: string;
  description: string;
  hasForm: boolean;
  deploymentStatus: DeploymentStatus;
  modules: Module[]
  endpoint: string;
}

export interface BuilderStateFiller {
  workflow: WorkflowStep[];
  workflowId: string;
  workflowName: string;
  lastSave: DateTime;
}
