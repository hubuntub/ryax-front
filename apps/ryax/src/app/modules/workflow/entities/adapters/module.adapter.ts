// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleDto } from '../dtos/module.dto';
import { Module } from '../module';

export class ModuleAdapter {
  public adapt(dto: ModuleDto): Module {
    return {
      id: dto.id,
      definitionId: dto.module_id,
      name: dto.custom_name ? dto.custom_name : dto.name,
      version: dto.version,
      description: dto.description
    }
  }
}
