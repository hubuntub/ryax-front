// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleDto } from '../dtos/module.dto';
import { Module } from '../module';
import { ModuleAdapter } from './module.adapter';

describe('ModuleAdapter', () => {
  const adapter: ModuleAdapter = new ModuleAdapter();

  const dto: ModuleDto = {
    id: '',
    module_id: '',
    technical_name: 'test1',
    name: '',
    version: '',
    description: '',
    kind: '',
    inputs: [],
    outputs: []
  };

  const module: Module = {
    id: '',
    name: 'test1',
    version: '',
    description: '',
  };

  const customNameDto: ModuleDto = {
    id: '',
    module_id: '',
    technical_name: 'test1',
    name: '',
    custom_name: 'test2',
    version: '',
    description: '',
    kind: '',
    inputs: [],
    outputs: []
  };

  const customNameModule: Module = {
    id: '',
    name: 'test2',
    version: '',
    description: '',
  };

  it('should adapt ModuleDto into Module', () => {
    expect(adapter.adapt(dto)).toEqual(module);
  });

  it('should adapt ModuleDto into Module even with custom name', () => {
    expect(adapter.adapt(customNameDto)).toEqual(customNameModule);
  });
});
