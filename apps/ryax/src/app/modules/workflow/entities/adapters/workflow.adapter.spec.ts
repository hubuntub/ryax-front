// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DeploymentStatusDto } from '../../../shared/entity/dtos/deployment-status.dto';
import { DeploymentStatus } from '../../../shared/entity/index';
import { WorkflowDto } from '../dtos/workflow.dto';
import { Workflow } from '../workflow';
import { WorkflowAdapter } from './workflow.adapter';

describe('WorkflowAdapter', () => {
  const adapter: WorkflowAdapter = new WorkflowAdapter();

  const dto: WorkflowDto = {
    id: '',
    name: '',
    description: '',
    has_form: false,
    deployment_status: DeploymentStatusDto.DEPLOYED,
    status: 'Valid',
    modules: [
      {
        id: '',
        module_id: '',
        name: '',
        technical_name: '',
        version: '',
        description: '',
        kind: '',
        inputs: [],
        outputs: []
      },
      {
        id: '',
        module_id: '',
        name: '',
        technical_name: '',
        version: '',
        description: '',
        kind: '',
        inputs: [],
        outputs: []
      }
    ]
  };

  const workflow: Workflow = {
    id: '',
    name: '',
    description: '',
    hasForm: false,
    deploymentStatus: DeploymentStatus.DEPLOYED,
    modules: [
      {
        id: '',
        definitionId: '',
        name: '',
        version: '',
        description: '',
      },
      {
        id: '',
        definitionId: '',
        name: 'test',
        version: '',
        description: '',
      }
    ]
  };

  it('should adapt WorkflowDto into Workflow', () => {
    expect(adapter.adaptWorkflow(dto)).toEqual(workflow);
  });
});
