// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DeploymentStatusDto } from '../../../shared/entity/dtos/deployment-status.dto';
import { WorkflowModuleDetailedDto } from '../../../studio/entities/dtos/workflow-module.dto';
import { ModuleDto } from './module.dto';

export interface WorkflowDto {
  id: string;
  name: string;
  description: string;
  has_form: boolean;
  status: 'Valid' | 'Invalid';
  deployment_status: DeploymentStatusDto;
  modules?: ModuleDto[];
  modules_links?: WorkflowModuleLinkDto[];
  endpoint: string;
}

export interface WorkflowDetailedDto {
  id: string;
  name: string;
  description: string;
  has_form: boolean;
  status: 'Valid' | 'Invalid';
  deployment_status: DeploymentStatusDto;
  modules?: WorkflowModuleDetailedDto[];
  modules_links?: WorkflowModuleLinkDto[];
}

export interface WorkflowModuleLinkDto {
  id: string;
  input_module_id: string;
  output_module_id: string;
}
