// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { WorkflowExecution } from '../../../shared/entity';
import { WorkflowApiService } from '../../services/workflow-api.service';
import { WorkflowActions } from '../actions';
import { WorkflowState } from '../reducers/index';
import { selectLastParams, selectWorkflowData } from '../selectors/index';


@Injectable()
export class WorkflowEffects {

  getWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.getWorkflow, WorkflowActions.deploySuccess, WorkflowActions.stopSuccess),
    switchMap((action) => forkJoin([
      this.workflowApiService.getWorkflow(action.workflowId),
      this.workflowApiService.getWorkflowExecutions(action.workflowId),
      this.workflowApiService.getWorkflowResultsConfig(action.workflowId),
      this.workflowApiService.getEndpoint(action.workflowId),
    ]).pipe(
      switchMap(([workflow, executions, results, endpoint]) => {
        return [
          WorkflowActions.workflowSuccess(workflow),
          WorkflowActions.getWorkflowExecutionsSuccess(executions.list, executions.total),
          WorkflowActions.getResultsConfigSuccess(results),
          WorkflowActions.getEndpointSuccess(endpoint),
        ]
      }),
      catchError((err: HttpErrorResponse) => {
        return of(WorkflowActions.workflowError(err))
      })
    )),
  ));

  displayErrors$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.workflowError),
    tap((errAction) => this.notification.create(
      'error',
      'Something went wrong ! http error : ' + errAction.error.status,
      errAction.error.message
    ))
  ), { dispatch: false });

  getWorkflowExecutions$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.getWorkflowExecutions),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    switchMap(([action, workflow]) =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.workflowApiService.getWorkflowExecutions(workflow!.id, action.pageIndex, action.pageSize).pipe(
        map((data: { list: WorkflowExecution[], total : number }) => {
          return WorkflowActions.getWorkflowExecutionsSuccess(data.list, data.total)
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.getWorkflowExecutionsError(err))
        })
      )
    ),
  ));

  refreshWorkflowExecutions$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.refreshWorkflowExecutions),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    withLatestFrom(this.store$.select(selectLastParams)),
    switchMap(([[, workflow], params]) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      return this.workflowApiService.getWorkflowExecutions(workflow!.id, params[0], params[1]).pipe(
        map((data: { list: WorkflowExecution[], total : number }) => {
          return WorkflowActions.getWorkflowExecutionsSuccess(data.list, data.total)
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.getWorkflowExecutionsError(err))
        })
      )}
    ),
  ));

  deploy$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.deploy),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    switchMap(([, workflow]) =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.workflowApiService.deploy(workflow!.id).pipe(
        map(() => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          return WorkflowActions.deploySuccess(workflow!.id)
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.deployError(err))
        })
      )
    ),
  ));

  stop$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.stop),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    switchMap(([, workflow]) =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.workflowApiService.stop(workflow!.id).pipe(
        map(() => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          return WorkflowActions.stopSuccess(workflow!.id)
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.stopError(err))
        })
      )
    ),
  ));

  copy$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.copy),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    switchMap(([, workflow]) =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.workflowApiService.copy(workflow!.id, workflow!.name).pipe(
        map(() => {
          return WorkflowActions.copySuccess()
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.copyError(err))
        })
      )
    ),
  ));

  export$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.exportWf),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    switchMap(([, workflow]) =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.workflowApiService.export(workflow!.id).pipe(
        map(() => {
          return WorkflowActions.exportWfSuccess()
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.exportWfError(err))
        })
      )
    ),
  ));

  delete$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.deleteWf),
    withLatestFrom(this.store$.select(selectWorkflowData)),
    switchMap(([, workflow]) =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.workflowApiService.delete(workflow!.id).pipe(
        map(() => {
          this.router.navigateByUrl('/dashboard');
          return WorkflowActions.deleteWfSuccess()
        }),
        catchError((err: HttpErrorResponse) => {
          return of(WorkflowActions.deleteWfError(err))
        })
      )
    ),
  ));

  constructor(
    private actions$: Actions,
    private workflowApiService: WorkflowApiService,
    private notification: NzNotificationService,
    private store$: Store<WorkflowState>,
    private router: Router
  ) {}
}
