// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jest-marbles';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable } from 'rxjs';
import { WorkflowListEffects } from '.';
import { DeploymentStatus } from '../../../shared/entity/index';
import { Workflow } from '../../entities';
import { WorkflowApiService } from '../../services/workflow-api.service';
import { WorkflowListActions } from '../actions';

const workflowList: Workflow[] = [
  {
    id: '1',
    name: 'test1',
    description: 'Test description 1',
    hasForm: false,
    deploymentStatus: DeploymentStatus.DEPLOYED,
    runPrice: 18,
    timesRun: 8,
    modules: []
  },
  {
    id: '2',
    name: 'test2',
    description: 'Test description 2',
    hasForm: false,
    deploymentStatus: DeploymentStatus.DEPLOYED,
    runPrice: 9,
    timesRun: 6,
    modules: []
  }
];


describe('WorkflowListEffects', () => {
  let effects: WorkflowListEffects;
  let actions$: Observable<Actions>;

  const dashboardApiSpy = {
    getWorkflows: jest.fn()
  }

  const notifSpy = {
    create: jest.fn(),
  }

  beforeEach(() => {
    dashboardApiSpy.getWorkflows.mockClear();

    TestBed.configureTestingModule({
      providers: [
        WorkflowListEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        { provide: WorkflowApiService, useValue: dashboardApiSpy },
        { provide: NzNotificationService, useValue: notifSpy },
      ]
    });
    effects = TestBed.inject(WorkflowListEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should try to get workflowList when asked', () => {
    dashboardApiSpy.getWorkflows.mockImplementation(() => cold('-a|', { a: workflowList }));

    actions$ = hot('--a', {
      a: WorkflowListActions.getWorkflows()
    });

    const expected$ = hot('---a', { a: WorkflowListActions.workflowsSuccess(workflowList) });

    expect(effects.getWorkflows$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(dashboardApiSpy.getWorkflows).toHaveBeenCalled();
    });
  });

  it('should handle error when trying to get workflowList', () => {
    dashboardApiSpy.getWorkflows.mockImplementation(() => cold('-#', {}, { error: '400' }));

    actions$ = hot('--a', {
      a: WorkflowListActions.getWorkflows()
    });

    const expected$ = hot('---a', { a: WorkflowListActions.workflowsError({ error: '400' } as HttpErrorResponse) });

    expect(effects.getWorkflows$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(dashboardApiSpy.getWorkflows).toHaveBeenCalled();
    });
  });
});
