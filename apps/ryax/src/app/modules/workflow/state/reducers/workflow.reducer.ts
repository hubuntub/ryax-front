// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { WorkflowExecution } from '../../../shared/entity';
import { Workflow } from '../../entities';
import { WorkflowActions } from '../actions';

export interface WorkflowState {
  data: Workflow | null;
  executionList: WorkflowExecution[];
  lastParams: [number, number];
  totalExecutions: number;
  loading: boolean;
  executionsLoading: boolean;
  deployLoading: boolean;
  resultsConfig: { key: string, description: string }[];
  endpoint: string;
  error: HttpErrorResponse | null;
}

export const initialWorkflowState: WorkflowState = {
  data: null,
  executionList: [],
  lastParams: [0, 10],
  totalExecutions: 0,
  loading: false,
  executionsLoading: false,
  deployLoading: false,
  resultsConfig: [],
  endpoint: '',
  error: null
};

export const workflowReducer = createReducer<WorkflowState>(
  initialWorkflowState,
  on(WorkflowActions.getWorkflow, (state) => ({
    ...state,
    loading: true,
    error: null
  })),
  on(WorkflowActions.workflowSuccess, (state, { workflow }) => ({
    ...state,
    data: workflow,
    loading: false,
    error: null
  })),
  on(WorkflowActions.workflowError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
  on(WorkflowActions.getWorkflowExecutions, (state, { pageIndex, pageSize }) => ({
    ...state,
    lastParams: [pageIndex, pageSize],
    executionsLoading: true,
    error: null
  })),
  on(WorkflowActions.refreshWorkflowExecutions, (state) => ({
    ...state,
    executionsLoading: true,
    error: null
  })),
  on(WorkflowActions.getWorkflowExecutionsSuccess, (state, { workflowExecutions, total }) => ({
    ...state,
    executionList: workflowExecutions,
    totalExecutions: total,
    executionsLoading: false,
    error: null
  })),
  on(WorkflowActions.workflowError, (state, { error }) => ({
    ...state,
    executionsLoading: false,
    error
  })),
  on(WorkflowActions.deploy, (state) => ({
    ...state,
    deployLoading: true,
    error: null
  })),
  on(WorkflowActions.deploySuccess, (state) => ({
    ...state,
    deployLoading: false,
    error: null
  })),
  on(WorkflowActions.deployError, (state, { error }) => ({
    ...state,
    deployLoading: false,
    error
  })),
  on(WorkflowActions.stop, (state) => ({
    ...state,
    deployLoading: true,
    error: null
  })),
  on(WorkflowActions.stopSuccess, (state) => ({
    ...state,
    deployLoading: false,
    error: null
  })),
  on(WorkflowActions.stopError, (state, { error }) => ({
    ...state,
    deployLoading: false,
    error
  })),
  on(WorkflowActions.copy, (state) => ({
    ...state,
    loading: true,
    error: null
  })),
  on(WorkflowActions.copySuccess, (state) => ({
    ...state,
    loading: false,
    error: null
  })),
  on(WorkflowActions.copyError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
  on(WorkflowActions.exportWf, (state) => ({
    ...state,
    loading: true,
    error: null
  })),
  on(WorkflowActions.exportWfSuccess, (state) => ({
    ...state,
    loading: false,
    error: null
  })),
  on(WorkflowActions.exportWfError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
  on(WorkflowActions.deleteWf, (state) => ({
    ...state,
    loading: true,
    error: null
  })),
  on(WorkflowActions.deleteWfSuccess, (state) => ({
    ...state,
    loading: false,
    error: null
  })),
  on(WorkflowActions.deleteWfError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
  on(WorkflowActions.getResultsConfigSuccess, (state, { resultsConfig }) => ({
    ...state,
    resultsConfig,
    loading: false,
    error: null
  })),
  on(WorkflowActions.getResultsConfigError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
  on(WorkflowActions.getEndpointSuccess, (state, { endpoint }) => ({
    ...state,
    endpoint,
    loading: false,
    error: null
  })),
  on(WorkflowActions.getEndpointError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
);
