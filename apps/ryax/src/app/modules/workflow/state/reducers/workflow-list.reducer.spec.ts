// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { DeploymentStatus } from '../../../shared/entity/index';
import { WorkflowListActions } from '../actions';
import { initialWorkflowsState, workflowListReducer, WorkflowListState } from './workflow-list.reducer';

describe('workflowListReducer', () => {
  const state: WorkflowListState = initialWorkflowsState;

  const error: HttpErrorResponse = {
    name: 'HttpErrorResponse',
    message: 'message',
    error: 401
  } as HttpErrorResponse;

  it('on WorkflowListActions.getWorkflows', () => {
    const action = WorkflowListActions.getWorkflows();
    expect(workflowListReducer(state, action)).toEqual({
      ...state,
      loading: true,
      error: null
    });
  });

  it('on WorkflowListActions.workflowsSuccess', () => {
    const action = WorkflowListActions.workflowsSuccess([
      {
        id: '1',
        name: 'test1',
        description: 'Test 1',
        hasForm: false,
        deploymentStatus: DeploymentStatus.DEPLOYED,
        runPrice: 18,
        timesRun: 8,
        modules: []
      },
      {
        id: '2',
        name: 'test2',
        description: 'Test 2',
        hasForm: false,
        deploymentStatus: DeploymentStatus.DEPLOYED,
        runPrice: 9,
        timesRun: 6,
        modules: []
      }
    ]);
    expect(workflowListReducer(state, action)).toEqual({
      ...state,
      data: [
        {
          id: '1',
          name: 'test1',
          description: 'Test 1',
          hasForm: false,
          deploymentStatus: DeploymentStatus.DEPLOYED,
          runPrice: 18,
          timesRun: 8,
          modules: []
        },
        {
          id: '2',
          name: 'test2',
          description: 'Test 2',
          hasForm: false,
          deploymentStatus: DeploymentStatus.DEPLOYED,
          runPrice: 9,
          timesRun: 6,
          modules: []
        }
      ],
      loading: false,
      error: null
    });
  });

  it('on WorkflowListActions.workflowsError', () => {
    const action = WorkflowListActions.workflowsError(error);
    expect(workflowListReducer(state, action)).toEqual({
      ...state,
      loading: false,
      error: error
    });
  });

});
