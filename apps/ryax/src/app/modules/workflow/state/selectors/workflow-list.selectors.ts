// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { GlobalWorkflowState, WorkflowFeatureKey, WorkflowListState } from '../reducers';

const selectWorkflowListFn = (state: GlobalWorkflowState) => state.workflowList;
const selectWorkflowListErrorFn = (state: WorkflowListState) => state.error;
const selectWorkflowListLoadingFn = (state: WorkflowListState) => state.loading;
const selectWorkflowListDataFn = (state: WorkflowListState) => state.data;

export const selectGlobalWorkflowState = createFeatureSelector<GlobalWorkflowState>(WorkflowFeatureKey);
export const selectWorkflowListState = createSelector(selectGlobalWorkflowState, selectWorkflowListFn);
export const selectWorkflowListError = createSelector(selectWorkflowListState, selectWorkflowListErrorFn);
export const selectWorkflowListLoading = createSelector(selectWorkflowListState, selectWorkflowListLoadingFn);
export const selectWorkflowListData = createSelector(selectWorkflowListState, selectWorkflowListDataFn);


