// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createSelector } from '@ngrx/store';
import { GlobalWorkflowState, WorkflowState } from '../reducers';
import { selectGlobalWorkflowState } from './workflow-list.selectors';

const selectWorkflowFn = (state: GlobalWorkflowState) => state.workflow;
const selectWorkflowErrorFn = (state: WorkflowState) => state.error;
const selectWorkflowLoadingFn = (state: WorkflowState) => state.loading;
const selectWorkflowDataFn = (state: WorkflowState) => state.data;
const selectWorkflowStatusFn = (state: WorkflowState) => state.data?.deploymentStatus;
const selectWorkflowExecutionListFn = (state: WorkflowState) => state.executionList;
const selectWorkflowExecutionLengthFn = (state: WorkflowState) => state.totalExecutions;
const selectWorkflowExecutionLoadingFn = (state: WorkflowState) => state.executionsLoading;
const selectResultsConfigFn = (state: WorkflowState) => state.resultsConfig;
const selectLastParamsFn = (state: WorkflowState) => state.lastParams;
const selectEndpointFn = (state: WorkflowState) => state.endpoint;


export const selectWorkflowState = createSelector(selectGlobalWorkflowState, selectWorkflowFn);
export const selectWorkflowError = createSelector(selectWorkflowState, selectWorkflowErrorFn);
export const selectWorkflowLoading = createSelector(selectWorkflowState, selectWorkflowLoadingFn);
export const selectWorkflowData = createSelector(selectWorkflowState, selectWorkflowDataFn);
export const selectWorkflowStatus = createSelector(selectWorkflowState, selectWorkflowStatusFn);
export const selectWorkflowExecutionList = createSelector(selectWorkflowState, selectWorkflowExecutionListFn);
export const selectWorkflowExecutionLength = createSelector(selectWorkflowState, selectWorkflowExecutionLengthFn);
export const selectWorkflowExecutionLoading = createSelector(selectWorkflowState, selectWorkflowExecutionLoadingFn);
export const selectResultsConfig = createSelector(selectWorkflowState, selectResultsConfigFn);
export const selectLastParams = createSelector(selectWorkflowState, selectLastParamsFn);
export const selectEndpoint = createSelector(selectWorkflowState, selectEndpointFn);
