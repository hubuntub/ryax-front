// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { WorkflowActions, WorkflowListActions } from '../actions';
import { GlobalWorkflowState } from '../reducers';
import {
  selectEndpoint,
  selectResultsConfig,
  selectWorkflowData,
  selectWorkflowExecutionLength,
  selectWorkflowExecutionList, selectWorkflowExecutionLoading,
  selectWorkflowListData,
  selectWorkflowListLoading,
  selectWorkflowLoading, selectWorkflowStatus
} from '../selectors';

@Injectable({
  providedIn: 'root'
})
export class WorkflowFacade {

  public workflowList$ = this.store.select(selectWorkflowListData);
  public workflowListLoading$ = this.store.select(selectWorkflowListLoading);
  public workflow$ = this.store.select(selectWorkflowData);
  public workflowStatus$ = this.store.select(selectWorkflowStatus);
  public workflowLoading$ = this.store.select(selectWorkflowLoading);
  public workflowExecutionList$ = this.store.select(selectWorkflowExecutionList);
  public workflowExecutionLength$ = this.store.select(selectWorkflowExecutionLength);
  public workflowExecutionLoading$ = this.store.select(selectWorkflowExecutionLoading);
  public resultsConfig$ = this.store.select(selectResultsConfig);
  public endpoint$ = this.store.select(selectEndpoint);

  constructor(
    private readonly store: Store<GlobalWorkflowState>
  ) { }

  public getWorkflows() {
    this.store.dispatch(WorkflowListActions.getWorkflows());
  }

  public getWorkflow(id: string) {
    this.store.dispatch(WorkflowActions.getWorkflow(id));
  }

  public getWorkflowExecutions(pageIndex: number, pageSize: number) {
    this.store.dispatch(WorkflowActions.getWorkflowExecutions(pageIndex, pageSize));
  }

  public refreshWorkflowExecutions() {
    this.store.dispatch(WorkflowActions.refreshWorkflowExecutions());
  }

  public deploy() {
    this.store.dispatch(WorkflowActions.deploy());
  }

  public stop() {
    this.store.dispatch(WorkflowActions.stop());
  }

  public copy() {
    this.store.dispatch(WorkflowActions.copy());
  }

  public export() {
    this.store.dispatch(WorkflowActions.exportWf());
  }

  public delete() {
    this.store.dispatch(WorkflowActions.deleteWf());
  }
}
