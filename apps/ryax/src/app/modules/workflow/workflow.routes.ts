// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowAlertsComponent } from './components/workflow-alerts/workflow-alerts.component';
import { WorkflowExecutionsComponent } from './components/workflow-executions/workflow-executions.component';
import { WorkflowOverviewComponent } from './components/workflow-overview/workflow-overview.component';
import { WorkflowVersioningComponent } from './components/workflow-versioning/workflow-versioning.component';
import { WorkflowComponent } from './components/workflow/workflow.component';

export const workflowRoutes = [
  {
    path: ':workflowId',
    component: WorkflowComponent,
    children: [
      {
        path: 'overview',
        component: WorkflowOverviewComponent,
      },
      {
        path: 'executions',
        component: WorkflowExecutionsComponent
      },
      {
        path: 'alerts',
        component: WorkflowAlertsComponent
      },
      {
        path: 'versioning',
        component: WorkflowVersioningComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'overview',
      }
    ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/dashboard',
  }
];
