// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../shared/shared.module';
import { StudioModule } from '../studio/studio.module';
import { WorkflowAlertsComponent } from './components/workflow-alerts/workflow-alerts.component';
import { WorkflowCardComponent } from './components/workflow-card/workflow-card.component';
import { WorkflowExecutionsComponent } from './components/workflow-executions/workflow-executions.component';
import { WorkflowListComponent } from './components/workflow-list/workflow-list.component';
import { WorkflowOverviewComponent } from './components/workflow-overview/workflow-overview.component';
import { WorkflowVersioningComponent } from './components/workflow-versioning/workflow-versioning.component';
import { WorkflowComponent } from './components/workflow/workflow.component';
import { WorkflowApiService } from './services/workflow-api.service';
import { WorkflowEffects, WorkflowListEffects } from './state/effects';
import { WorkflowFeatureKey, WorkflowReducerProvider, WorkflowReducerToken } from './state/reducers';


@NgModule({
  declarations: [
    WorkflowComponent,
    WorkflowAlertsComponent,
    WorkflowCardComponent,
    WorkflowExecutionsComponent,
    WorkflowListComponent,
    WorkflowOverviewComponent,
    WorkflowVersioningComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(WorkflowFeatureKey, WorkflowReducerToken),
    EffectsModule.forFeature([WorkflowListEffects, WorkflowEffects]),
    RouterModule,
    SharedModule,
    AuthModule,
    StudioModule
  ],
  providers: [
    WorkflowReducerProvider,
    WorkflowApiService
  ],
  exports: [
    WorkflowListComponent
  ]
})
export class WorkflowModule { }
