// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserDto } from '../dtos';
import { User } from '../user';

export class UserAdapter {
  public adaptUser(dto: UserDto): User {
    return {
      name: dto.username,
      role: dto.role,
      email: dto.email,
      comment: dto.comment,
    }
  }
}
