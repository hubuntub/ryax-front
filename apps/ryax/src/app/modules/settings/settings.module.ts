// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import {RouterModule} from "@angular/router";
import {settingsRoutes} from "./settings.routes";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {SharedModule} from "../shared/shared.module";
import {EffectsModule} from "@ngrx/effects";
import {UserEffects} from "./state/effects";
import {SettingsFeatureKey, SettingsReducerProvider, SettingsReducerToken} from "./state/reducers";
import {UserApiService} from "./services/user-api/user-api.service";
import {StoreModule} from "@ngrx/store";
import { UserChangePasswordComponent } from './components/user-change-password/user-change-password.component';
import { UserChangeEmailComponent } from './components/user-change-email/user-change-email.component';
import { UserChangeUsernameComponent } from './components/user-change-username/user-change-username.component';


@NgModule({
  declarations: [
    UserSettingsComponent,
    UserChangePasswordComponent,
    UserChangeEmailComponent,
    UserChangeUsernameComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(settingsRoutes),
    EffectsModule.forFeature([UserEffects]),
    NzAvatarModule,
    SharedModule,
    StoreModule.forFeature(SettingsFeatureKey, SettingsReducerToken),
  ],
  providers: [
    SettingsReducerProvider,
    UserApiService
  ]
})
export class SettingsModule { }
