// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {Component, EventEmitter, Output} from '@angular/core';
import {UserSettingsFacade} from "../../state/facade";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthFacade} from "../../../auth/state/facade";

@Component({
  selector: 'ryax-user-change-username',
  templateUrl: './user-change-username.component.pug',
  styleUrls: ['./user-change-username.component.scss']
})
export class UserChangeUsernameComponent {
  @Output() changingNameDone = new EventEmitter<null>();
  validateForm: FormGroup;

  submitForm(): void {
    this.userSettingsFacade.updateName(this.validateForm.value.username)
    this.changingNameDone.emit()
  }

  cancel(): void {
    this.validateForm.reset();
    this.changingNameDone.emit()
  }

  constructor(
    private userSettingsFacade: UserSettingsFacade,
    private fb: FormBuilder,
  ) {
    this.validateForm = this.fb.group({
      username: ['', [Validators.required]],
    });
  }
}
