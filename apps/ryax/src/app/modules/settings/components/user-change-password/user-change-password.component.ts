// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserSettingsFacade} from "../../state/facade";

@Component({
  selector: 'ryax-user-change-password',
  templateUrl: './user-change-password.component.pug',
  styleUrls: ['./user-change-password.component.scss']
})
export class UserChangePasswordComponent {
  @Output() changingPasswordDone = new EventEmitter<null>();
  validateForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private userSettingsFacade: UserSettingsFacade
) {
    this.validateForm = this.fb.group({
      password: ['', [Validators.required]],
      confirm: ['', [this.confirmValidator]],
    });
  }

  submitForm(): void {
    this.userSettingsFacade.updatePassword(this.validateForm.value.password);
    this.changingPasswordDone.emit()
  }

  cancel(): void {
    this.validateForm.reset();
    this.changingPasswordDone.emit()
  }

  validateConfirmPassword(): void {
    setTimeout(() => this.validateForm.controls['confirm'].updateValueAndValidity());
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls['password'].value) {
      return { confirm: true, error: true };
    }
    return {};
  };
}
