// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { User } from '../../entities';


export const get = createAction(
  '[User] Get User full details'
);

export const getUserSuccess = createAction(
  '[User] Load User full details success',
  (user: User) => ({ user })
);

export const getUserError = createAction(
  '[User] Load User full details error',
  (error: HttpErrorResponse) => ({ error })
);

export const updatePassword = createAction(
  '[User] Update User password',
  (password: string) => ({ password })
);

export const updatePasswordSuccess = createAction(
  '[User] Update User password success',
  (user: User) => ({ user })
);

export const updatePasswordError = createAction(
  '[User] Update User password error',
  (error: HttpErrorResponse) => ({ error })
);

export const updateName = createAction(
  '[User] Update User name',
  (name: string) => ({ name })
);

export const updateNameSuccess = createAction(
  '[User] Update User name success',
  (user: User) => ({ user })
);

export const updateNameError = createAction(
  '[User] Update User name error',
  (error: HttpErrorResponse) => ({ error })
);

export const updateEmail = createAction(
  '[User] Update User email',
  (email: string) => ({ email })
);

export const updateEmailSuccess = createAction(
  '[User] Update User email success',
  (user: User) => ({ user })
);

export const updateEmailError = createAction(
  '[User] Update User email error',
  (error: HttpErrorResponse) => ({ error })
);
