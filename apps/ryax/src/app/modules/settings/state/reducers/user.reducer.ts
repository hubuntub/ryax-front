// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { Roles, User } from '../../entities';
import { UserActions } from '../actions';

export interface UserState {
  userDetails: User | null
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialUserState: UserState = {
  userDetails: { name: 'username', role: Roles.ANONYMOUS, comment: '', email: "Unknown" },
  loading: false,
  error: null
};

export const userReducer = createReducer<UserState>(
  initialUserState,
  on(UserActions.get, (state) => ({
    ...state,
    loading: true,
    error: null
  })),
  on(UserActions.getUserSuccess, UserActions.updatePasswordSuccess, UserActions.updateEmailSuccess, UserActions.updateNameSuccess, (state, { user }) => ({
    ...state,
    userDetails: user,
    loading: false,
    error: null
  })),
  on(UserActions.getUserError, UserActions.updatePasswordError, UserActions.updateEmailError, UserActions.updateNameError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  })),
  on(UserActions.updatePassword, (state, password) => ({
    ...state,
    password,
    loading: true,
    error: null
  })),
  on(UserActions.updateName, (state, username) => ({
    ...state,
    username,
    loading: true,
    error: null
  })),
  on(UserActions.updateEmail, (state, email) => ({
    ...state,
    email,
    loading: true,
    error: null
  })),
)
