// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {InjectionToken} from "@angular/core";
import {ActionReducerMap} from "@ngrx/store";
import { userReducer, UserState } from './user.reducer';

export * from './user.reducer';

export const SettingsFeatureKey = 'settingsDomain';

export interface UserSettingsState {
  user: UserState
}

export const SettingsReducerToken = new InjectionToken<ActionReducerMap<UserState>>(SettingsFeatureKey);

export const SettingsReducerProvider = {
  provide: SettingsReducerToken,
  useValue: {
    user: userReducer
  },
};
