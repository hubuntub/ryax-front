// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserActions } from '../actions';
import { UserSettingsState } from '../reducers';
import {selectUserDetails, selectUserError, selectUserLoading} from '../selectors/user.selectors';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsFacade {

  public user$ = this.store.select(selectUserDetails);
  public error$ = this.store.select(selectUserError);
  public loading$ = this.store.select(selectUserLoading);

  constructor(
    private readonly store: Store<UserSettingsState>
  ) { }

  public getUser() {
    this.store.dispatch(UserActions.get());
  }
  public updatePassword(password: string) {
    this.store.dispatch(UserActions.updatePassword(password))
  }
  public updateName(name: string) {
    this.store.dispatch(UserActions.updateName(name))
  }
  public updateEmail(email: string) {
    this.store.dispatch(UserActions.updateEmail(email))
  }
}
