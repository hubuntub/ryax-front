// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowExecutionStatus } from '../../entity';

@Pipe({
  name: 'statusName'
})
export class StatusNamePipe implements PipeTransform {

  public transform(value: WorkflowExecutionStatus): string {
    switch (value) {
      case WorkflowExecutionStatus.Created:
        return 'Created';
      case WorkflowExecutionStatus.Pending:
        return 'Pending';
      case WorkflowExecutionStatus.Deploying:
        return 'Deploying';
      case WorkflowExecutionStatus.Deployed:
        return 'Deployed';
      case WorkflowExecutionStatus.Running:
        return 'Execution is running';
      case WorkflowExecutionStatus.Error:
        return 'Error';
      case WorkflowExecutionStatus.Canceled:
        return 'Canceled';
      case WorkflowExecutionStatus.Completed:
        return 'Completed';
      default:
        return '';
    }
  }

}
