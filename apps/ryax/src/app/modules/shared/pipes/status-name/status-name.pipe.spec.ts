// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowExecutionStatus } from '../../entity';
import { StatusNamePipe } from '../index';

describe('StatusNamePipe', () => {
  it('create an instance', () => {
    const pipe = new StatusNamePipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform status into a name string', () => {
    const pipe = new StatusNamePipe();
    expect(pipe.transform(WorkflowExecutionStatus.Created)).toEqual('Created');
    expect(pipe.transform(WorkflowExecutionStatus.Pending)).toEqual('Pending');
    expect(pipe.transform(WorkflowExecutionStatus.Deploying)).toEqual('Deploying');
    expect(pipe.transform(WorkflowExecutionStatus.Deployed)).toEqual('Deployed');
    expect(pipe.transform(WorkflowExecutionStatus.Running)).toEqual('Execution is running');
    expect(pipe.transform(WorkflowExecutionStatus.Error)).toEqual('Error');
    expect(pipe.transform(WorkflowExecutionStatus.Completed)).toEqual('Completed');
    expect(pipe.transform('' as WorkflowExecutionStatus)).toEqual('');
  });
});
