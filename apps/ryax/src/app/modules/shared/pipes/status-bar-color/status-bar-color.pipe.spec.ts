// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowExecutionStatus, TimelineColors, ModuleExecutionStatus } from '../../entity';
import { StatusBarColorPipe } from '../index';

describe('StatusBarColorPipe', () => {
  it('create an instance', () => {
    const pipe = new StatusBarColorPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform status into a color string', () => {
    const pipe = new StatusBarColorPipe();
    expect(pipe.transform(WorkflowExecutionStatus.Created, 'item')).toEqual(TimelineColors.Purple);
    expect(pipe.transform(WorkflowExecutionStatus.Pending, 'start')).toEqual(TimelineColors.Red);
    expect(pipe.transform(WorkflowExecutionStatus.Error, 'end')).toEqual(TimelineColors.Purple);
    expect(pipe.transform(WorkflowExecutionStatus.Canceled, 'end')).toEqual(TimelineColors.Purple);
    expect(pipe.transform(WorkflowExecutionStatus.Completed, 'end')).toEqual(TimelineColors.Red);
    expect(pipe.transform('' as WorkflowExecutionStatus, 'start')).toEqual(TimelineColors.Purple);
    expect(pipe.transform(ModuleExecutionStatus.Waiting, 'item')).toEqual(TimelineColors.Purple);
    expect(pipe.transform(ModuleExecutionStatus.Running, 'item')).toEqual(TimelineColors.Gradient);
    expect(pipe.transform(ModuleExecutionStatus.Success, 'item')).toEqual(TimelineColors.Red);
    expect(pipe.transform('' as ModuleExecutionStatus, 'item')).toEqual(TimelineColors.Purple);
  });
});
