// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowExecutionStatus, ModuleExecutionStatus, TimelineColors } from '../../entity';

@Pipe({
  name: 'statusBarColor'
})
export class StatusBarColorPipe implements PipeTransform {

  public transform(status: WorkflowExecutionStatus | ModuleExecutionStatus, part: 'start' | 'item' | 'end'): TimelineColors {
    if (part !== 'item') {
      switch (status) {
        case WorkflowExecutionStatus.Created:
          return TimelineColors.Purple;
        case WorkflowExecutionStatus.Pending:
        case WorkflowExecutionStatus.Running:
        case WorkflowExecutionStatus.Deploying:
        case WorkflowExecutionStatus.Deployed:
        case WorkflowExecutionStatus.Error:
          return part === 'start' ? TimelineColors.Red : TimelineColors.Purple;
        case WorkflowExecutionStatus.Completed:
          return TimelineColors.Red;
        default:
          return TimelineColors.Purple;
      }
    } else {
      switch (status) {
        case ModuleExecutionStatus.Waiting:
          return TimelineColors.Purple;
        case ModuleExecutionStatus.Running:
        case ModuleExecutionStatus.Error:
          return TimelineColors.Gradient;
        case ModuleExecutionStatus.Success:
          return TimelineColors.Red;
        default:
          return TimelineColors.Purple;
      }
    }
  }
}
