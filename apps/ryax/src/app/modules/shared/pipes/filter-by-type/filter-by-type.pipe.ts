// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByType'
})
export class FilterByTypePipe implements PipeTransform {

  public transform(list: {groupName: string, values: {name: string, type: string, value: string}[]}[]  | null, type: string): {groupName: string, values: {name: string, type: string, value: string}[]}[]  {
    if (!list) {
      return [];
    }
    return list.map((group) => ({
      ...group,
      values: group.values.filter((value => value.type === type))
    })).filter((group) => group.values.length > 0);
  }

}
