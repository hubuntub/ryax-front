// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ColorsEnum, WorkflowExecutionStatus } from '../../entity';

@Pipe({
  name: 'statusTagColor'
})
export class StatusTagColorPipe implements PipeTransform {

  public transform(value: WorkflowExecutionStatus): string {
    switch (value) {
      case WorkflowExecutionStatus.Created:
      case WorkflowExecutionStatus.Pending:
        return ColorsEnum.Default;
      case WorkflowExecutionStatus.Deploying:
      case WorkflowExecutionStatus.Deployed:
      case WorkflowExecutionStatus.Running:
        return ColorsEnum.Primary;
      case WorkflowExecutionStatus.Canceled:
      case WorkflowExecutionStatus.Error:
        return ColorsEnum.Danger;
      case WorkflowExecutionStatus.Completed:
        return ColorsEnum.Success;
      default:
        return ColorsEnum.Default;
    }
  }
}
