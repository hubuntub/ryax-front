// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ColorsEnum, WorkflowExecutionStatus } from '../../entity';
import { StatusTagColorPipe } from '../index';

describe('StatusIconColorPipe', () => {
  it('create an instance', () => {
    const pipe = new StatusTagColorPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform status into a color string', () => {
    const pipe = new StatusTagColorPipe();
    expect(pipe.transform(WorkflowExecutionStatus.Created)).toEqual(ColorsEnum.Default);
    expect(pipe.transform(WorkflowExecutionStatus.Pending)).toEqual(ColorsEnum.Default);
    expect(pipe.transform(WorkflowExecutionStatus.Running)).toEqual(ColorsEnum.Primary);
    expect(pipe.transform(WorkflowExecutionStatus.Deploying)).toEqual(ColorsEnum.Primary);
    expect(pipe.transform(WorkflowExecutionStatus.Deployed)).toEqual(ColorsEnum.Primary);
    expect(pipe.transform(WorkflowExecutionStatus.Error)).toEqual(ColorsEnum.Danger);
    expect(pipe.transform(WorkflowExecutionStatus.Canceled)).toEqual(ColorsEnum.Danger);
    expect(pipe.transform(WorkflowExecutionStatus.Completed)).toEqual(ColorsEnum.Success);
    expect(pipe.transform('' as WorkflowExecutionStatus)).toEqual(ColorsEnum.Default);
  });
});
