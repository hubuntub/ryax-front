// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { WorkflowModuleLight } from '../../../studio/entities/workflow-module';

@Component({
  selector: 'ryax-module-mini-card',
  templateUrl: './module-mini-card.component.pug',
  styleUrls: ['./module-mini-card.component.scss']
})
export class ModuleMiniCardComponent {

  @Input() public module!: WorkflowModuleLight;
  @Input() public active = false;

}
