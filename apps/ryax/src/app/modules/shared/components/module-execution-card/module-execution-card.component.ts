// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DateTime, Duration, Interval } from 'luxon';
import { ModuleExecutionStatus, ModuleExecution } from '../../entity';

@Component({
  selector: 'ryax-module-execution-card',
  templateUrl: './module-execution-card.component.pug',
  styleUrls: ['./module-execution-card.component.scss']
})
export class ModuleExecutionCardComponent {
  @Input() public showDetails = false;
  @Output() public showDetailsChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public getLogs: EventEmitter<void> = new EventEmitter<void>();
  @Input() public sideBySide = true;
  @Input() public moduleExecution: ModuleExecution = {
    id: '',
    definitionId: '',
    name: '',
    version: '',
    description: '',
    status: ModuleExecutionStatus.Waiting,
    inputs: [],
    outputs: [],
    submittedDate: null,
    startedDate: null,
    endedDate: null
  }
  @Input() public logs = '';

  public dateFormat = 'H\'h\' mm\'m\' ss.S\'s\' dd/MM/y';

  public getDuration(): Duration {
    if (!this.moduleExecution.startedDate) {
      return Duration.fromMillis(0);
    } else if (!this.moduleExecution.endedDate) {
      return Interval.fromDateTimes(this.moduleExecution.startedDate, DateTime.now()).toDuration();
    } else {
      return Interval.fromDateTimes(this.moduleExecution.startedDate, this.moduleExecution.endedDate).toDuration();
    }
  }

  public toggleDetails() {
    this.showDetails = !this.showDetails;
    this.showDetailsChange.emit(this.showDetails);
  }

  public getLogsTrigger() {
    if (!this.logs) {
      this.getLogs.emit()
    }
  }
}
