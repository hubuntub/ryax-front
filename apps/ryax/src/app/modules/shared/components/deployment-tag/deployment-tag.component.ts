// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { DeploymentStatus } from '../../entity';

@Component({
  selector: 'ryax-deployment-tag',
  templateUrl: './deployment-tag.component.pug',
  styleUrls: ['./deployment-tag.component.scss']
})
export class DeploymentTagComponent {

  @Input() public status: DeploymentStatus = DeploymentStatus.DEPLOYED;

  public getColor(status: DeploymentStatus) {
    switch (status) {
      case DeploymentStatus.DEPLOYED:
        return 'green';
      case DeploymentStatus.DEPLOYING:
      case DeploymentStatus.UNDEPLOYING:
        return 'blue';
      case DeploymentStatus.ERROR:
        return 'red';
      case DeploymentStatus.VALID:
      case DeploymentStatus.INVALID:
      default:
        return 'default';
    }
  }

}
