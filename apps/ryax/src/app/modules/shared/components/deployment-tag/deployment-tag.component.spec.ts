// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DeploymentStatus } from '../../entity/index';

import { DeploymentTagComponent } from './deployment-tag.component';

describe('DeploymentTagComponent', () => {
  let component: DeploymentTagComponent;
  let fixture: ComponentFixture<DeploymentTagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeploymentTagComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeploymentTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the right color depending on provided deployment status', () => {
    expect(component.getColor(DeploymentStatus.DEPLOYED)).toEqual('green');
    expect(component.getColor(DeploymentStatus.DEPLOYING)).toEqual('gold');
    expect(component.getColor(DeploymentStatus.INVALID)).toEqual('red');
    expect(component.getColor(DeploymentStatus.UNDEPLOYING)).toEqual('gold');
  });
});
