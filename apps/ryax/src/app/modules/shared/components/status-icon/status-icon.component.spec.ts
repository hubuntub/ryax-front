// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ColorsEnum, ModuleExecutionStatus } from '../../entity';

import { StatusIconComponent } from './status-icon.component';

describe('StatusIconComponent', () => {
  let component: StatusIconComponent;
  let fixture: ComponentFixture<StatusIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzIconModule],
      declarations: [StatusIconComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should transform status into a theme string', () => {
    expect(component.getTheme(ModuleExecutionStatus.Waiting)).toEqual('outline');
    expect(component.getTheme(ModuleExecutionStatus.Running)).toEqual('outline');
    expect(component.getTheme(ModuleExecutionStatus.Error)).toEqual('fill');
    expect(component.getTheme(ModuleExecutionStatus.Success)).toEqual('fill');
    expect(component.getTheme('' as ModuleExecutionStatus)).toEqual('outline');
  });

  it('should transform status into a icon string', () => {
    expect(component.getType(ModuleExecutionStatus.Waiting)).toEqual('clock-circle');
    expect(component.getType(ModuleExecutionStatus.Running)).toEqual('loading');
    expect(component.getType(ModuleExecutionStatus.Error)).toEqual('close-circle');
    expect(component.getType(ModuleExecutionStatus.Success)).toEqual('check-circle');
    expect(component.getType('' as ModuleExecutionStatus)).toEqual('clock-circle');
  });

  it('should transform status into a color string', () => {
    expect(component.getColor(ModuleExecutionStatus.Waiting)).toEqual(ColorsEnum.Default);
    expect(component.getColor(ModuleExecutionStatus.Running)).toEqual(ColorsEnum.Primary);
    expect(component.getColor(ModuleExecutionStatus.Error)).toEqual(ColorsEnum.Danger);
    expect(component.getColor(ModuleExecutionStatus.Success)).toEqual(ColorsEnum.Success);
    expect(component.getColor('' as ModuleExecutionStatus)).toEqual(ColorsEnum.Default);
  });
});
