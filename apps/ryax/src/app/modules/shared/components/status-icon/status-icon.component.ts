// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { ColorsEnum, ModuleExecutionStatus } from '../../entity';

@Component({
  selector: 'ryax-status-icon',
  templateUrl: './status-icon.component.pug',
  styleUrls: ['./status-icon.component.scss']
})
export class StatusIconComponent {
  @Input() public status: ModuleExecutionStatus = ModuleExecutionStatus.Waiting;

  public getType(value: ModuleExecutionStatus): string {
    switch (value) {
      case ModuleExecutionStatus.Waiting:
        return 'clock-circle';
      case ModuleExecutionStatus.Running:
        return 'loading';
      case ModuleExecutionStatus.Error:
        return 'close-circle';
      case ModuleExecutionStatus.Success:
        return 'check-circle';
      default:
        return 'clock-circle';
    }
  }

  public getColor(value: ModuleExecutionStatus): string {
    switch (value) {
      case ModuleExecutionStatus.Waiting:
        return ColorsEnum.Default;
      case ModuleExecutionStatus.Running:
        return ColorsEnum.Primary;
      case ModuleExecutionStatus.Error:
        return ColorsEnum.Danger;
      case ModuleExecutionStatus.Success:
        return ColorsEnum.Success;
      default:
        return ColorsEnum.Default;
    }
  }

  public getTheme(value: ModuleExecutionStatus): 'fill' | 'outline' | 'twotone' {
    switch (value) {
      case ModuleExecutionStatus.Waiting:
      case ModuleExecutionStatus.Running:
        return 'outline';
      case ModuleExecutionStatus.Error:
      case ModuleExecutionStatus.Success:
        return 'fill';
      default:
        return 'outline';
    }
  }
}
