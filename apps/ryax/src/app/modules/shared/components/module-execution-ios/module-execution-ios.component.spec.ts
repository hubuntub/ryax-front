// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleExecutionIosComponent } from './module-execution-ios.component';

describe('ModuleExecutionIosComponent', () => {
  let component: ModuleExecutionIosComponent;
  let fixture: ComponentFixture<ModuleExecutionIosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ModuleExecutionIosComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleExecutionIosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a boolean to display passwords independently', () => {
    component.ngOnChanges({
      data: {
        currentValue: [
          { id: 'pass1', value: 'P@$$1', type: 'password' },
          { id: 'pass2', value: 'P@$$2', type: 'password' },
          { id: 'notPass', value: 'file.ext', type: 'file' }
        ],
        firstChange: true,
        isFirstChange(): boolean {
          return true
        },
        previousValue: undefined
      }
    })

    expect(component.passwordVisibility).toEqual({
      pass1: false,
      pass2: false
    });
  });

  it('should emit a show modal event', () => {
    component.showModal.emit = jest.fn();

    component.onShowModal();

    expect(component.showModal.emit).toHaveBeenCalled();
  });

  it('should return a boolean when the string is matching', () => {
    expect(component.isFile('file')).toBeTruthy();
    expect(component.isFile('directory')).toBeFalsy();
    expect(component.isFile('password')).toBeFalsy();

    expect(component.isDirectory('file')).toBeFalsy();
    expect(component.isDirectory('directory')).toBeTruthy();
    expect(component.isDirectory('password')).toBeFalsy();

    expect(component.isPassword('file')).toBeFalsy();
    expect(component.isPassword('directory')).toBeFalsy();
    expect(component.isPassword('password')).toBeTruthy();
  });
});
