// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DateTime } from 'luxon';

export enum WorkflowExecutionStatus {
  None = 'None',
  Created = 'Created',
  Pending = 'Pending',
  Deploying = 'Deploying',
  Deployed = 'Deployed',
  Running = 'Running',
  Completed = 'Completed',
  Canceled = 'Canceled',
  Error = 'Error'
}

export enum ModuleExecutionStatus {
  Success = 'Success',
  Error = 'Error',
  Running = 'Running',
  Waiting = 'Waiting',
}

export interface WorkflowExecution {
  id: string;
  workflowId: string;
  status: WorkflowExecutionStatus;
  startedAt: DateTime;
  moduleExecutions: ModuleExecution[];
  step: number;
  totalSteps: number;
  duration: string;
}

export interface ModuleExecution {
  id: string;
  definitionId: string;
  name: string;
  version: string;
  description: string;
  status: ModuleExecutionStatus;
  inputs: ExecutionData[];
  outputs: ExecutionData[];
  submittedDate: DateTime | null;
  startedDate: DateTime | null;
  endedDate: DateTime | null;
}

export interface ExecutionData {
  id: string;
  name: string;
  value?: string | number;
  enumValues: string[];
  help: string;
  type: string;
}
