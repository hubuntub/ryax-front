// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowExecutionDto } from '../../dtos';
import { ModuleExecutionStatus, WorkflowExecutionStatus } from '../../execution';
import { ExecutionAdapter } from './execution.adapter';

describe('ExecutionAdapter', () => {
  const executionAdapter: ExecutionAdapter = new ExecutionAdapter();

  it('should adapt WorkflowExecutionDto into WorkflowExecution', () => {
    const workflowExecutionDto: WorkflowExecutionDto = {
      id: 'exec-id',
      workflow_definition_id: '',
      started_at: '2022-01-14T18:01:06.801174',
      last_result_at: '2022-01-14T18:01:06.801174',
      submitted_at: '2022-01-14T18:01:06.801174',
      total_steps: 34,
      completed_steps: 13,
      state: 'Created',
      executions: [
        {
          id: 'child-exec-id',
          module: {
            id: '',
            kind: 'SOURCE',
            description: 'child-description',
            technical_name: 'child-name',
            version: '1.0.0',
            inputs: [
              {
                id: 'input-id',
                value: 'Test value',
                display_name: 'input-name',
                help: '',
                enum_values: [],
                type: 'input-type',
              }
            ],
            outputs: [
              {
                id: 'input-id',
                value: 'Test value',
                display_name: 'output-name',
                help: '',
                enum_values: [],
                type: 'output-type',
              }
            ]
          },
          state: 'Waiting',
          inputs: [
            {
              value: 'Test value',
              name: 'input-name',
            }
          ],
          outputs: [
            {
              value: 'Test value',
              name: 'output-name',
            }
          ]
        },
        {
          id: 'child-exec-id2',
          state: 'Waiting',
          module: {
            id: '',
            kind: 'SOURCE',
            description: 'child-description2',
            human_name: 'Child Name 2',
            technical_name: 'child-name2',
            version: '2.0.0',
            inputs: [],
            outputs: []
          },
          inputs: [],
          outputs: []
        }
      ],
    };
    const result = executionAdapter.adapt(workflowExecutionDto);
    expect(result).toEqual({
      id: 'exec-id',
      status: WorkflowExecutionStatus.Created,
      startedAt: new Date('2022-01-14T18:01:06.801174'),
      duration: 0,
      step: 13,
      totalSteps: 34,
      moduleExecutions: [
        {
          id: 'child-exec-id',
          name: 'child-name',
          version: '1.0.0',
          description: 'child-description',
          status: ModuleExecutionStatus.Waiting,
          inputs: [
            {
              id: 'input-id',
              value: 'Test value',
              name: 'input-name',
              type: 'input-type',
              enumValues: [],
              help: ''
            }
          ],
          outputs: [
            {
              id: 'input-id',
              value: 'Test value',
              name: 'output-name',
              type: 'output-type',
              enumValues: [],
              help: ''
            }
          ],
        },
        {
          id: 'child-exec-id2',
          name: 'Child Name 2',
          version: '2.0.0',
          description: 'child-description2',
          status: ModuleExecutionStatus.Waiting,
          inputs: [],
          outputs: [],
        }
      ]
    })
  });
});
