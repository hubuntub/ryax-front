// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InputDto } from '../../../../workflow/entities/dtos/module.dto';
import { ModuleExecutionDto, WorkflowExecutionDto } from '../../dtos';
import {
  ExecutionData,
  ModuleExecution,
  ModuleExecutionStatus,
  WorkflowExecution,
  WorkflowExecutionStatus
} from '../../execution';
import { DateTime, Interval } from 'luxon';

export class ExecutionAdapter {

  public adapt(dto: WorkflowExecutionDto): WorkflowExecution {
    return {
      id: dto.id,
      workflowId: dto.workflow_definition_id,
      status: dto.state as WorkflowExecutionStatus,
      startedAt: dto.started_at ? DateTime.fromISO(dto.started_at) : DateTime.invalid('Not started yet'),
      duration: this.getDuration(dto),
      step: dto.completed_steps,
      totalSteps: dto.total_steps,
      moduleExecutions: dto.executions ? this.adaptModuleExecution(dto.executions) : [],
    }
  }

  public getDuration(dto: WorkflowExecutionDto): string {
    if (!dto.started_at || !dto.last_result_at) {
      return `Can't calculate`;
    } else {
      return Interval.fromDateTimes(
        DateTime.fromISO(dto.started_at),
        DateTime.fromISO(dto.last_result_at)
      ).toDuration().toFormat('mm\'m\' ss.SSS\'s\'');
    }
  }

  public adaptModuleExecution(dtos: ModuleExecutionDto[]): ModuleExecution[] {
    return dtos.map((dto) => {
      return {
        id: dto.id,
        definitionId: dto.module.id,
        description: dto.module.description,
        name: dto.module.custom_name ? dto.module.custom_name : dto.module.name,
        version: dto.module.version,
        status: dto.state as ModuleExecutionStatus,
        inputs: this.adaptExecutionData(dto.module.inputs),
        outputs: this.adaptExecutionData(dto.module.outputs),
        submittedDate: dto.submitted_at_date ? DateTime.fromISO(dto.submitted_at_date) : null,
        startedDate: dto.started_at_date ? DateTime.fromISO(dto.started_at_date) : null,
        endedDate: dto.ended_at_date ? DateTime.fromISO(dto.ended_at_date) : null
      }
    });
  }

  public adaptExecutionData(dtos: InputDto[]): ExecutionData[] {
    return dtos.map((dto) => {
      return {
        id: dto.id,
        name: dto.display_name,
        value: dto.value,
        enumValues: dto.enum_values,
        help: dto.help,
        type: dto.type
      }
    });
  }
}
