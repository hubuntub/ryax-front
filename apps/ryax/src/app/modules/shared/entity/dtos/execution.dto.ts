// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleDto } from '../../../workflow/entities/dtos/module.dto';

export interface WorkflowExecutionDto {
  id: string;
  workflow_definition_id: string
  submitted_at: string;
  started_at: string;
  last_result_at: string;
  state: WorkflowExecutionStateDto;
  completed_steps: number;
  total_steps: number;
  executions?: ModuleExecutionDto[];
}

export interface ModuleExecutionDto {
  id: string;
  state: ModuleExecutionStateDto;
  module: ModuleDto;
  inputs: ExecutionInputDto[];
  outputs: ExecutionInputDto[];
  submitted_at_date?: string;
  started_at_date?: string;
  ended_at_date?: string;
}

export interface ExecutionInputDto {
  name: string;
  value: unknown;
}

export type WorkflowExecutionStateDto = 'None' | 'Created' | 'Pending' | 'Deploying' | 'Deployed' | 'Running' | 'Completed' | 'Canceled' | 'Error';
export type ModuleExecutionStateDto = 'Success' | 'Error' | 'Running' | 'Waiting';
