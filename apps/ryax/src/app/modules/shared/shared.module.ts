// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IconDefinition } from '@ant-design/icons-angular';
import {
  ArrowLeftOutline,
  DeleteOutline,
  DownloadOutline,
  EditOutline,
  EyeInvisibleOutline,
  FileTwoTone,
  InboxOutline,
  MoreOutline,
  PlusOutline,
  RedoOutline,
  UserOutline,
  WarningTwoTone
} from '@ant-design/icons-angular/icons';
import { DesignModule } from '@ryax-front/design';
import { LuxonModule } from 'luxon-angular';
import { NzAlertModule } from 'ng-zorro-antd/alert';

import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NZ_CONFIG, NzConfig } from 'ng-zorro-antd/core/config';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { TokenInjectorProvider } from '../auth/services/token-injector/token-injector.service';
import {
  DeploymentTagComponent,
  LayoutComponent,
  ModuleExecutionCardComponent,
  ModuleExecutionIosComponent,
  ModuleMiniCardComponent,
  StatusIconComponent,
  StatusTagComponent,
  TimelineComponent,
  TimelineItemComponent
} from './components';
import { StatusBarColorPipe, StatusNamePipe, StatusTagColorPipe } from './pipes';
import { FilterByTypePipe } from './pipes/filter-by-type/filter-by-type.pipe';
import { FilterNamePipe } from './pipes/filter-name/filter-name.pipe';
import { ConsoleDisplayComponent } from './components/console-display/console-display.component';

const ngZorroConfig: NzConfig = {
  message: { nzTop: 120 },
  notification: { nzDuration: 7000, nzPlacement: 'bottomRight' }
};

const icons: IconDefinition[] = [
  ArrowLeftOutline,
  DeleteOutline,
  DownloadOutline,
  DeleteOutline,
  EditOutline,
  EyeInvisibleOutline,
  FileTwoTone,
  InboxOutline,
  MoreOutline,
  PlusOutline,
  RedoOutline,
  UserOutline,
  WarningTwoTone
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NzIconModule.forChild(icons),
    ...SharedModule.NgZorroModules,
    DesignModule,
    LuxonModule
  ],
  declarations: [
    LayoutComponent,
    ModuleExecutionCardComponent,
    ModuleExecutionIosComponent,
    StatusIconComponent,
    TimelineComponent,
    TimelineItemComponent,
    StatusBarColorPipe,
    StatusNamePipe,
    StatusTagColorPipe,
    FilterNamePipe,
    FilterByTypePipe,
    StatusTagComponent,
    DeploymentTagComponent,
    ModuleMiniCardComponent,
    ConsoleDisplayComponent,
  ],
  providers: [
    TokenInjectorProvider,
    { provide: NZ_CONFIG, useValue: ngZorroConfig }
  ],
  exports: [
    // ==== Modules ====
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NzIconModule,
    ...SharedModule.NgZorroModules,
    DesignModule,
    LuxonModule,
    // ==== Components ====
    DeploymentTagComponent,
    ModuleExecutionCardComponent,
    ModuleExecutionIosComponent,
    ModuleMiniCardComponent,
    StatusIconComponent,
    StatusTagComponent,
    TimelineComponent,
    TimelineItemComponent,
    ConsoleDisplayComponent,
    // ==== Pipes ====
    StatusBarColorPipe,
    StatusNamePipe,
    StatusTagColorPipe,
    FilterNamePipe,
    FilterByTypePipe
  ]
})
export class SharedModule {

  // eslint-disable-next-line
  static NgZorroModules: any[] = [
    NzButtonModule,
    NzInputModule,
    NzSelectModule,
    NzUploadModule,
    NzCardModule,
    NzSpinModule,
    NzInputNumberModule,
    NzFormModule,
    NzToolTipModule,
    NzTimelineModule,
    NzTagModule,
    NzGridModule,
    NzCollapseModule,
    NzTableModule,
    NzProgressModule,
    NzNotificationModule,
    NzEmptyModule,
    NzStepsModule,
    NzResultModule,
    NzTabsModule,
    NzMessageModule,
    NzModalModule,
    NzAlertModule,
    NzDropDownModule
  ];
}
