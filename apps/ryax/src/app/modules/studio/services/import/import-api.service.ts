// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Metadata } from '../../entities/metadata';

@Injectable({
  providedIn: 'root'
})
export class ImportApiService {

  private baseUrl = '/api';

  constructor(
    private http: HttpClient
  ) { }

  public loadExistingRepos(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + '/repos');
  }

  public loadNewRepo(url: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/repos/new?url=' + url);
  }

  public buildModule(metadata: Metadata) {
    return this.http.post<any>(this.baseUrl + '/build', metadata);
  }
}
