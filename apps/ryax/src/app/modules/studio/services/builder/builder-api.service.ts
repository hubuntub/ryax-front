// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { WorkflowAdapter } from '../../../workflow/entities/adapters/workflow.adapter';
import { WorkflowDetailedDto } from '../../../workflow/entities/dtos/workflow.dto';
import { BuilderStateFiller } from '../../../workflow/entities/index';
import { ModuleConfigAdapter } from '../../entities/adapters/module-config/module-config.adapter';
import { ModuleLinkAdapter } from '../../entities/adapters/module-link/module-link.adapter';
import { StudioErrorAdapter } from '../../entities/adapters/studio-error/studio-error.adapter';
import { WorkflowModuleAdapter } from '../../entities/adapters/workflow-module/workflow-module.adapter';
import { StudioErrorDto } from '../../entities/dtos/studio-error.dto';
import { WorkflowModuleDetailedDto } from '../../entities/dtos/workflow-module.dto';
import { StudioError } from '../../entities/studio-error';
import { Config, ModuleOutput, WorkflowModuleDetailed, WorkflowModuleLight } from '../../entities/workflow-module';
import { FormValues, OutputFormValues } from '../../entities/workflow-step';

@Injectable({
  providedIn: 'root'
})
export class BuilderApiService {

  constructor(
    private http: HttpClient
  ) { }

  public create() {
    return this.http.post<{ workflow_id: string }>('/api/studio/workflows', { name: 'My new workflow'});
  }

  public loadFullWorkflowForStudio(workflowId: string): Observable<BuilderStateFiller> {
    return this.http.get<WorkflowDetailedDto>('/api/studio/v2/workflows/' + workflowId).pipe(
      map((workflowDto: WorkflowDetailedDto) => {
        return new WorkflowAdapter().adaptWorkflowToStudio(workflowDto);
      })
    );
  }

  public getModuleList(): Observable<[WorkflowModuleLight[], WorkflowModuleLight[]]> {
    return this.http.get<WorkflowModuleLight[]>('/api/studio/modules').pipe(
      map((modules) => {
        const sources = modules.filter((module) => module.kind === "Source")
        const processes = modules.filter((module) => module.kind !== "Source")
        return [sources, processes];
      })
    );
  }

  public getModule(id: string): Observable<WorkflowModuleDetailed> {
    return this.http.get<WorkflowModuleDetailedDto>('/api/studio/modules/' + id).pipe(
      map((module) => new WorkflowModuleAdapter().adaptDetailed(module))
    );
  }

  public addModuleToWorkflow(moduleDefId: string, workflowId: string, prevModuleId: string | undefined): Observable<{ module: WorkflowModuleDetailed, defaultValues: FormValues }> {
    return this.http.post<WorkflowModuleDetailedDto>(
      '/api/studio/v2/workflows/' + workflowId + '/modules',
      { module_definition_id: moduleDefId, parent_workflow_module_id: prevModuleId }
    ).pipe(map((module) => ({
      module: new WorkflowModuleAdapter().adaptDetailed(module),
      defaultValues: new WorkflowModuleAdapter().adaptInputs(module.inputs)
    })));
  }

  public updateModuleInWorkflow(moduleDefId: string, workflowId: string, currentModuleId: string | undefined): Observable<{ module: WorkflowModuleDetailed, defaultValues: FormValues }> {
    return this.http.post<WorkflowModuleDetailedDto>(
      '/api/studio/v2/workflows/' + workflowId + '/modules',
      { module_definition_id: moduleDefId, replace_workflow_module_id: currentModuleId }
    ).pipe(map((module) => ({
      module: new WorkflowModuleAdapter().adaptDetailed(module),
      defaultValues: new WorkflowModuleAdapter().adaptInputs(module.inputs)
    })));
  }

  public removeModuleFromWorkflow(workflowId: string, moduleId: string | undefined): Observable<void> {
    return this.http.delete<void>('/api/studio/workflows/' + workflowId + '/modules/' + moduleId);
  }

  public setModulesLinks(
    workflowId: string, moduleIds: string[], indexes: { currentIndex: number, previousIndex: number}
  ): Observable<{workflow: BuilderStateFiller, indexes: { currentIndex: number, previousIndex: number}}> {
    moveItemInArray(moduleIds, indexes.previousIndex, indexes.currentIndex);
    return this.http
      .put<void>('/api/studio/v2/workflows/' + workflowId + '/links', new ModuleLinkAdapter().adapt(moduleIds))
      .pipe(
        switchMap(() => this.loadFullWorkflowForStudio(workflowId)),
        map((fullWorkflow) => ({workflow: fullWorkflow, indexes}))
      );
  }

  public updateModuleConfiguration(
    form: FormValues,
    valid: boolean,
    workflowId: string,
    moduleId: string,
    moduleConf: { customName: string, inputs: Config[] | undefined, outputs: ModuleOutput[] | undefined }
  ): Observable<void> {
    return this.http.put<void>(
      '/api/studio/v2/workflows/' + workflowId + '/modules/' + moduleId,
      new ModuleConfigAdapter().adapt(form, moduleConf)
    );
  }

  public updateDynamicModuleConfiguration(
    form: OutputFormValues,
    valid: boolean,
    workflowId: string,
    moduleId: string,
    moduleConf: { customName: string, inputs: Config[] | undefined, outputs: ModuleOutput[] | undefined }
  ): Observable<void> {
    return this.http.put<void>(
      '/api/studio/v2/workflows/' + workflowId + '/modules/' + moduleId,
      new ModuleConfigAdapter().adaptOutput(form, moduleConf)
    );
  }

  public updateModuleName(workflowId: string, moduleId: string, name: string) {
    return this.http.put<void>('/api/studio/workflows/' + workflowId + '/modules/' + moduleId, { custom_name: name });
  }

  public editName(name: string, moduleId: string): Observable<string> {
    return this.http.put<void>('/api/studio/workflows/' + moduleId, { name }).pipe(map(() => name));
  }

  public deploy(workflowId: string) {
    return this.http.post(`/api/studio/workflows/${workflowId}/deploy`, null).pipe(map(() => workflowId));
  }

  public addOutput(ids: string[]) {
    const body = {
      display_name: "New input",
      enum_values: [],
      help: "New input help",
      technical_name: "new-input",
      type: "string"
    }
    return this.http.post(`/api/studio/workflows/${ids[0]}/modules/${ids[1]}/outputs`, body).pipe(map(() => ids[0]));
  }

  public removeOutput(inputId: string, workflowId: string, moduleId: string) {
    return this.http.delete(`/api/studio/workflows/${workflowId}/modules/${moduleId}/outputs/${inputId}`).pipe(map(() => workflowId));
  }

  public saveFile(file: NzUploadFile, inputId: string, workflowId: string, moduleId: string) {
    return this.http.post(`/api/studio/workflows/${workflowId}/modules/${moduleId}/inputs/${inputId}/file`, file);
  }

  public removeFile(inputId: string, workflowId: string, moduleId: string) {
    return this.http.delete(`/api/studio/workflows/${workflowId}/modules/${moduleId}/inputs/${inputId}/file`)
      .pipe(map(() => ({ inputId })));
  }

  public getResults(workflowId: string) {
    return this.http.get<{key:string, workflow_module_io_id: string}[]>(`/api/studio/v2/workflows/${workflowId}/results`)
    .pipe(map((results) => results.map((res) => ({ key: res.key, value: res.workflow_module_io_id}))));
  }

  public saveResults(workflowId: string, results: { key: string, value: string }[]) {
    return this.http.put(`/api/studio/v2/workflows/${workflowId}/results`, results.map(({ key, value }) => ({ key, workflow_module_io_id: value })));
  }

  public getEndpoint(workflowId: string) {
    return this.http.get<{ path: string }>(`/api/studio/v2/workflows/${workflowId}/endpoint`).pipe(map(({path}) => path));
  }

  public saveEndpoint(workflowId: string, endpoint: string) {
    return this.http.put<void>(`/api/studio/v2/workflows/${workflowId}/endpoint`, { path: endpoint });
  }

  public getStudioErrors(workflowId: string): Observable<StudioError[]> {
    return this.http.get<StudioErrorDto[]>(`/api/studio/workflows/${workflowId}/errors`).pipe(
      map((errors) => new StudioErrorAdapter().adapt(errors))
    );
  }

  public changeVersion(workflowId: string, workflowModuleId: string, newVersionModuleId: string) {
    return this.http.post<{ workflow_module_id: string }>(
      `/api/studio/workflows/${workflowId}/modules/${workflowModuleId}/change_version`,
      { module_id: newVersionModuleId }
    );
  }
}
