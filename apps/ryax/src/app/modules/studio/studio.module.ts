// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgDatePipesModule } from 'ngx-pipes';
import { SharedModule } from '../shared/shared.module';
import { BuilderComponent } from './components/builder/builder.component';
import { BuildingComponent } from './components/building/building.component';
import { ConfigEndpointComponent } from './components/config-endpoint/config-endpoint.component';
import { ConfigFormComponent } from './components/config-form/config-form.component';
import { ConfigPanelComponent } from './components/config-panel/config-panel.component';
import { ImportBuildComponent } from './components/import-build/import-build.component';
import { ImportCodeSelectComponent } from './components/import-code-select/import-code-select.component';
import { ImportDefineMetadataComponent } from './components/import-define-metadata/import-define-metadata.component';
import { ImportGitComponent } from './components/import-git/import-git.component';
import { ImportComponent } from './components/import/import.component';
import { ResultModalComponent } from './components/result-modal/result-modal.component';
import { BuilderEffects } from './state/effects/builder.effects';
import { ImportEffects } from './state/effects/import.effects';
import { StudioFeatureKey, StudioReducerProvider, StudioReducerToken } from './state/reducers/index';
import { ModuleDescriptionComponent } from './components/module-description/module-description.component';
import { ConfigDynamicOutputsComponent } from './components/config-dynamic-outputs/config-dynamic-outputs.component';
import { CreateWorkflowComponent } from './components/create-workflow/create-workflow.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DragDropModule,
    NgDatePipesModule,
    StoreModule.forFeature(StudioFeatureKey, StudioReducerToken),
    EffectsModule.forFeature([BuilderEffects, ImportEffects]),
    SharedModule
  ],
  declarations: [
    BuilderComponent,
    BuildingComponent,
    ConfigFormComponent,
    ConfigPanelComponent,
    ConfigEndpointComponent,
    ImportComponent,
    ImportGitComponent,
    ImportCodeSelectComponent,
    ImportDefineMetadataComponent,
    ImportBuildComponent,
    ModuleDescriptionComponent,
    ConfigDynamicOutputsComponent,
    CreateWorkflowComponent,
    ResultModalComponent
  ],
  providers: [
    StudioReducerProvider
  ],
  exports: [
    CreateWorkflowComponent
  ]
})
export class StudioModule { }
