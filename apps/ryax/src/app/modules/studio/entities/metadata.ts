// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface Metadata {
  moduleName: string
  language: string
  dependencies: string
  inputs: {name: string, type: string}[]
  outputs: {name: string, type: string}[]
}
