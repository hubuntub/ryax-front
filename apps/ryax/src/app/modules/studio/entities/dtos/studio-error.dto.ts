export interface StudioErrorDto {
  code: number;
  id: string;
  workflow_module_id: string | null;
}
