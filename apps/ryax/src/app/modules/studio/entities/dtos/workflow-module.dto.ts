// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface WorkflowModuleLightDto {
  description: string;
  id: string;
  module_id: string;
  kind: string;
  name: string;
  version:string;
}

export interface WorkflowModuleDetailedDto {
  description: string;
  id: string;
  module_id: string;
  kind: string;
  custom_name?: string;
  name: string;
  technical_name: string;
  has_dynamic_outputs: boolean;
  version: string;
  versions: { version: string, id: string }[];
  categories: CategoryDto[];
  inputs: IosDto[]
  outputs: IosDto[]
  dynamic_outputs: IosDto[]
}

interface CategoryDto {
  name: string;
  id: string;
}

export interface IosDto {
  display_name: string;
  enum_values: string[];
  help: string;
  id: string;
  technical_name: string;
  type: string;
  reference_value?: string;
  static_value?: string | null;
  default_value?: string | null;
}
