// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Config, ModuleOutput } from '../../workflow-module';
import { FormValues, OutputFormValues } from '../../workflow-step';

export class ModuleConfigAdapter {
  public adapt(formValues: FormValues, moduleConf: { customName: string, inputs: Config[] | undefined, outputs: ModuleOutput[] | undefined}) {

    const inputs = moduleConf.inputs?.filter((input) => {
      return input.inputType !== 'file' && input.inputType !== 'directory' || formValues[input.id].linked;
    }).map((input) => {
      const isLinked = formValues[input.id].linked;
      let formValue;
      if (formValues[input.id].value) {
        formValue = formValues[input.id].value;
      } else {
        formValue = formValues[input.id].value === 0 ? formValues[input.id].value : null;
      }

      return { id: input.id, [isLinked ? 'reference_value' : 'static_value']: formValue }
    });

    return { custom_name: moduleConf.customName, inputs , dynamic_outputs: moduleConf.outputs }
  }

  public adaptOutput(formValues: OutputFormValues, moduleConf: { customName: string, inputs: Config[] | undefined, outputs: ModuleOutput[] | undefined}) {
    const dynamic_outputs = moduleConf.outputs?.map((output) => {
      return {
        help: formValues[output.id].help,
        id: output.id,
        technical_name: output.id,
        display_name: formValues[output.id].name,
        type: formValues[output.id].type
      }
    });

    return { custom_name: moduleConf.customName, inputs: moduleConf.inputs , dynamic_outputs }
  }
}
