// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { IosDto, WorkflowModuleDetailedDto, WorkflowModuleLightDto } from '../../dtos/workflow-module.dto';
import {
  WorkflowModuleDetailed,
  WorkflowModuleLight
} from '../../workflow-module';
import { FormValues, OutputFormValues, WorkflowStep } from '../../workflow-step';

export class WorkflowModuleAdapter {
  public adaptLight(dto: WorkflowModuleLightDto): WorkflowModuleLight {
    return {
      id: dto.id,
      definitionId: dto.module_id,
      name: dto.name,
      description: dto.description,
      kind: dto.kind,
      version: dto.version,
    };
  }

  public adaptDetailed(dto: WorkflowModuleDetailedDto): WorkflowModuleDetailed {
    return {
      id: dto.id,
      definitionId: dto.module_id,
      name: dto.custom_name ? dto.custom_name : dto.name,
      description: dto.description,
      kind: dto.kind,
      hasDynamic: dto.has_dynamic_outputs,
      version: dto.version,
      technicalName: dto.technical_name,
      mainValue: dto.inputs[0] ? dto.inputs[0].id : '',
      categories: dto.categories,
      versions: dto.versions?.map((versionDto) => ({ name: versionDto.version, id: versionDto.id})),
      inputs: dto.inputs?.map((input) => ({
        id: input.id,
        name: input.display_name,
        required: true,
        help: input.help,
        inputType: input.type,
        options: input.enum_values?.map((value) => ({ value, name: value })),
      })) ?? [],
      outputs: dto.outputs?.map((output) => ({
        id: output.id,
        help: output.help,
        name: output.display_name,
        type: output.type,
        possibleValues: output.enum_values
      })) ?? [],
      dynamicOutputs: dto.dynamic_outputs?.map((dyna_output) => ({
        id: dyna_output.id,
        help: dyna_output.help,
        name: dyna_output.display_name,
        type: dyna_output.type,
        possibleValues: dyna_output.enum_values
      })) ?? [],
    } as WorkflowModuleDetailed;
  }

  public adaptModulesToStudio(modules: WorkflowModuleDetailedDto[] | undefined): WorkflowStep[] {
    if (modules && modules.length > 0) {
      return modules.map((moduleDto) => {
        return {
          name: moduleDto.custom_name ? moduleDto.custom_name : moduleDto.name,
          status: 'READY', // 'UNDEFINED' | 'UNCONFIGURED' | 'READY'
          type: moduleDto.kind === 'source' ? 'SOURCE' : 'PROCESS',
          panels: [
            {
              name: 'Select',
              status: 'finish',
              originalValue: new WorkflowModuleAdapter().adaptDetailed(moduleDto)
            },
            {
              name: 'Configure',
              inputFormValues: this.adaptInputs(moduleDto.inputs),
              outputFormValues: this.adaptDynamicOutput(moduleDto.dynamic_outputs),
              status: 'finish'
            }
          ],
          currentPanelIndex: 2
        }
      })
    } else {
      return [
        {
          name: 'Source',
          status: 'UNDEFINED',
          type: 'SOURCE',
          panels: [
            {
              name: 'Select',
              status: 'process',
              originalValue: undefined
            },
            {
              name: 'Configure',
              inputFormValues: {},
              outputFormValues: {},
              status: 'wait'
            }
          ],
          currentPanelIndex: 0
        }
      ];
    }
  }

  public adaptInputs(inputs: IosDto[]): FormValues {
    return inputs.reduce((prev, input) => {
      let inputValue;
      if (input.reference_value) {
        inputValue = { linked: true, value: input.reference_value }
      } else if (input.static_value) {
        inputValue = { linked: false, value: input.static_value }
      } else {
        inputValue = { linked: false, value: input.default_value ? input.default_value : null }
      }
      return { ...prev, [input.id]: inputValue }
    }, {})
  }

  public adaptDynamicOutput(dyna_outputs: IosDto[]): OutputFormValues {
    return dyna_outputs?.reduce((sum: OutputFormValues, { id, help, type, display_name}) => {
      return { ...sum, [id]: { type, help, name: display_name }}
    }, {});
  }

  public adaptType(type: string): string {
    if (type === 'string') {
      return 'text'
    } else if (type === 'password') {
      return 'password'
    } else if (type === 'longstring') {
      return 'textarea'
    } else if (type === 'integer' || type === 'float') {
      return 'number'
    } else if (type === 'enum') {
      return 'select'
    } else if (type === 'file' || type === 'directory') {
      return 'file'
    } else {
      return 'text'
    }
  }
}
