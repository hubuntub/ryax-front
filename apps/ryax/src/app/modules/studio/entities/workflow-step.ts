// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowModuleDetailed } from './workflow-module';

export interface WorkflowStep {
  name: string;
  status: StepStatus;
  type: 'SOURCE' | 'PROCESS';
  panels: ModulePanels;
  currentPanelIndex: number;
}

export type ModulePanels = [SelectPanel, ...ConfigurationPanel[]];

interface SelectPanel {
  name: string;
  status: PanelStatus;
  originalValue?: WorkflowModuleDetailed;
}

interface ConfigurationPanel {
  name: string;
  status: PanelStatus;
  inputFormValues: FormValues,
  outputFormValues: OutputFormValues
}

export interface FormValues {
  [key: string]: {
    linked: boolean,
    value: string | number | null
  }
}

export interface OutputFormValues {
  [key: string]: {
    type: string,
    name: string,
    help: string
  }
}

export type StepStatus = 'UNDEFINED' | 'UNCONFIGURED' | 'READY';
export type PanelStatus = 'wait' | 'process' | 'finish' | 'error';
