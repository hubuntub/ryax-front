export interface StudioError {
  id: string;
  error: string;
  workflow_module_id: string | null;
}
