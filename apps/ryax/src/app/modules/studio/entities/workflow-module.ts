// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface WorkflowModuleLight {
  id: string;
  definitionId: string;
  name: string;
  kind: string;
  description: string;
  version: string;
}

export interface WorkflowModuleDetailed {
  id: string;
  definitionId: string;
  name: string;
  technicalName: string;
  kind: string;
  description: string;
  version: string;
  mainValue: string;
  hasDynamic: boolean;
  categories: { name: string, id: string }[];
  versions: { name: string, id: string }[];
  inputs: Config[];
  outputs: ModuleOutput[];
  dynamicOutputs: ModuleOutput[];
}

export interface WorkflowModule {
  id: string;
  name: string;
  kind: string;
  description: string;
  mainValue: string;
  configurator: Config[];
}

export interface Config {
  id: string;
  inputType: string;
  name: string;
  help?: string;
  required?: boolean;
  options?: Option[];
}

interface Option {
  value: unknown;
  name: string;
  default?: boolean;
}

export interface ModuleOutput {
  id: string;
  name: string;
  help: string;
  type: string;
  possibleValues?: string[];
}
