// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StudioFeatureKey, StudioState, BuilderState } from '../reducers';

const selectBuilderFn = (state: StudioState) => state.builder;
const selectErrorFn = (state: BuilderState) => state.error;
const selectLoadingFn = (state: BuilderState) => state.loading;
const selectConfigLoadingFn = (state: BuilderState) => state.configLoading;
const selectLastSaveFn = (state: BuilderState) => state.lastSave;
const selectWorkflowNameFn = (state: BuilderState) => state.workflowName.value;
const selectWorkflowNameLoadingFn = (state: BuilderState) => state.workflowName.loading;
const selectWorkflowIdFn = (state: BuilderState) => state.workflowId;
const selectWorkflowAndPreviousIdFn = (state: BuilderState) => [
  state.workflowId,
  state.currentStepIndex >= 1 ? state.workflow[state.currentStepIndex - 1].panels[0].originalValue?.id : undefined
] as [string, string | undefined];
const selectWorkflowFn = (state: BuilderState) => state.workflow;
const selectCurrentStepFn = (state: BuilderState) => state.workflow[state.currentStepIndex];
const selectCurrentIndexFn = (state: BuilderState) => state.currentStepIndex;
const selectCurrentPanelIndexFn = (state: BuilderState) => state.workflow[state.currentStepIndex].currentPanelIndex;
const selectSourceListFn = (state: BuilderState) => state.sourceList;
const selectProcessListFn = (state: BuilderState) => state.processList;
const selectStatusListFn = (state: BuilderState) => state.workflow[state.currentStepIndex].panels.map((panel) => panel.status);
const selectDeployableFn = (state: BuilderState) => state.workflow.reduce((prev, step) => step.status === 'READY' && prev, true);
const selectPreviousOutputsFn = (state: BuilderState) => {
  return state.workflow
    .filter((step, i) => i < state.currentStepIndex)
    .map((step,  index) => ({
      groupName: (step.panels[0].originalValue?.name ?? '') + ` ${index}`,
      values: step.panels[0].originalValue?.outputs.concat(step.panels[0].originalValue?.dynamicOutputs).map(
        (input) => ({name: input.name, value: input.id, type: input.type})
      ) ?? [],
    }))
    .filter((optionGroup) => optionGroup.values.length > 0);
}
const selectIsCurrentStepSourceFn = (state: BuilderState) => state.currentStepIndex === 0;
const selectToBuildFn = (state: BuilderState) => ({name: state.workflowName.value, workflow: state.workflow});
const selectModuleDynamicOutputFn = (state: BuilderState) => state.workflow[state.currentStepIndex].panels[0].originalValue?.dynamicOutputs;
const selectIdsFn = (state: BuilderState) => [state.workflowId, state.workflow[state.currentStepIndex].panels[0].originalValue?.id ?? ''] as [string, string];
const selectWorkflowIdWithModuleIdsFn = (state: BuilderState) => [state.workflowId, state.workflow.map((step) => step.panels[0].originalValue?.id ?? '')] as [string, string[]];
const selectModulesDescriptionsFn = (state: BuilderState) => {
  const allModulesOutputs = state.workflow.map((step, index) => {
    const modulesOutputs = step.panels[0].originalValue?.outputs.concat(step.panels[0].originalValue?.dynamicOutputs)
    return {
      groupName: (step.panels[0].originalValue?.name ?? '') + ` ${index}`,
      outputs: modulesOutputs ?? []
    }
  });

  return state.workflow.map((step, index) => {
    let description = '';

    if (step.panels[0].originalValue) {
      const mainValue = step.panels[1].inputFormValues[step.panels[0].originalValue.mainValue];

      if (index === 0 && state.endpoint.value) {
        description = 'Endpoint: ' + state.endpoint.value;
      } else if (mainValue) {
        description += mainValue.linked ? 'Data from ' : '';

        description += allModulesOutputs.reduce((final, outputGroup) => {
          const index = outputGroup.outputs.findIndex((output) => output.id === mainValue.value)
          if (index >= 0) {
            return `${outputGroup.outputs[index].name} in ${outputGroup.groupName}`
          } else {
            return final;
          }
        }, '');
      }
      if (!description) {
        description = step.panels[0].originalValue.description;
      }
    }

    return description;
  });
}
const selectIsAllSetFn = (state: BuilderState) => state.workflow.reduce(
  (isAllSet, step) => isAllSet && !!step.panels[0].originalValue, true
);
const selectModuleConfigFn = (state: BuilderState) => {
  const currentModule = state.workflow[state.currentStepIndex];

  return {
    customName: currentModule.name,
    inputs: currentModule.panels[0].originalValue?.inputs,
    outputs: currentModule.panels[0].originalValue?.dynamicOutputs
  }
};

const selectModuleConfigurationFn = (state: BuilderState) => {
  const currentModule = state.workflow[state.currentStepIndex];

  const inputs = currentModule.panels[0].originalValue?.inputs.filter((input) => {
    return input.inputType !== 'file' && input.inputType !== 'directory';
  }).map((input) => {
    const isLinked = currentModule.panels[1].inputFormValues[input.id].linked;
    return { id: input.id, [isLinked ? 'reference' : 'value']: currentModule.panels[1].inputFormValues[input.id].value }
  });

  // select inputs + name instead and do this in api service

  const outputs: string[] = [];

  return { customName: currentModule.name, inputs , outputs }
}
const selectResultsFn = (state: BuilderState) => state.results;
const selectEveryInputFn = (state: BuilderState) => {
  return state.workflow.reduce((result: { value: string, label: string }[], step) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return [ ...result, ...step.panels[0].originalValue!.inputs.map((input) => ({
      value: input.id,
      label: 'Input value "' + input.name + '" of "' + step.panels[0].originalValue?.name + '"' }))];
  }, []);
}
const selectEveryOutputFn = (state: BuilderState) => {
  return state.workflow.reduce((result: { value: string, label: string }[], step) => {
    if (step.panels[0].originalValue) {
      const allOutputs = step.panels[0].originalValue.outputs.concat(step.panels[0].originalValue.dynamicOutputs);
      return [ ...result, ...allOutputs.map((output) => ({
        value: output.id,
        label: 'Output value "' + output.name + '" of "' + step.panels[0].originalValue?.name + '"'
      }))]
    }
    return result
  }, []);
}
const selectEndpointValueFn = (state: BuilderState) => (state.endpoint.value);
const selectStudioErrorsFn = (state: BuilderState) => (state.studioError);


export const selectStudioState = createFeatureSelector<StudioState>(StudioFeatureKey);
export const selectBuilderState = createSelector(selectStudioState, selectBuilderFn);
export const selectError = createSelector(selectBuilderState, selectErrorFn);
export const selectLoading = createSelector(selectBuilderState, selectLoadingFn);
export const selectConfigLoading = createSelector(selectBuilderState, selectConfigLoadingFn);
export const selectLastSave = createSelector(selectBuilderState, selectLastSaveFn);
export const selectWorkflowName = createSelector(selectBuilderState, selectWorkflowNameFn);
export const selectWorkflowNameLoading = createSelector(selectBuilderState, selectWorkflowNameLoadingFn);
export const selectWorkflowId = createSelector(selectBuilderState, selectWorkflowIdFn);
export const selectWorkflowAndPreviousId = createSelector(selectBuilderState, selectWorkflowAndPreviousIdFn);
export const selectWorkflow = createSelector(selectBuilderState, selectWorkflowFn);
export const selectCurrentIndex = createSelector(selectBuilderState, selectCurrentIndexFn);
export const selectCurrentStep = createSelector(selectBuilderState, selectCurrentStepFn);
export const selectCurrentPanelIndex = createSelector(selectBuilderState, selectCurrentPanelIndexFn);
export const selectSourceList = createSelector(selectBuilderState, selectSourceListFn);
export const selectProcessList = createSelector(selectBuilderState, selectProcessListFn);
export const selectStatusList = createSelector(selectBuilderState, selectStatusListFn);
export const selectDeployable = createSelector(selectBuilderState, selectDeployableFn);
export const selectPreviousOutputs = createSelector(selectBuilderState, selectPreviousOutputsFn);
export const selectIsCurrentStepSource = createSelector(selectBuilderState, selectIsCurrentStepSourceFn);
export const selectToBuild = createSelector(selectBuilderState, selectToBuildFn);
export const selectModuleDynamicOutput = createSelector(selectBuilderState, selectModuleDynamicOutputFn);
export const selectIds = createSelector(selectBuilderState, selectIdsFn);
export const selectWorkflowIdWithModuleIds = createSelector(selectBuilderState, selectWorkflowIdWithModuleIdsFn);
export const selectModulesDescriptions = createSelector(selectBuilderState, selectModulesDescriptionsFn);
export const selectIsAllSet = createSelector(selectBuilderState, selectIsAllSetFn);
export const selectModuleConfiguration = createSelector(selectBuilderState, selectModuleConfigurationFn);
export const selectModuleConfig = createSelector(selectBuilderState, selectModuleConfigFn);
export const selectResults = createSelector(selectBuilderState, selectResultsFn);
export const selectEveryInput = createSelector(selectBuilderState, selectEveryInputFn);
export const selectEveryOutput = createSelector(selectBuilderState, selectEveryOutputFn);
export const selectEndpointValue = createSelector(selectBuilderState, selectEndpointValueFn);
export const selectStudioErrors = createSelector(selectBuilderState, selectStudioErrorsFn);
