// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StudioFeatureKey, StudioState } from '../reducers';
import { ImportState } from '../reducers/import.reducer';

const selectImportFn = (state: StudioState) => state.import;
const selectPanelIndexFn = (state: ImportState) => state.panelIndex;
const selectPanelsFn = (state: ImportState) => state.panels;
const selectExistingGitsListFn = (state: ImportState) => state.existingGits.list;
const selectNewGitContentFn = (state: ImportState) => state.newGit?.content;
const selectNotBuiltCodeFn = (state: ImportState) => state.newGit?.content?.lastScan.notBuiltModules;
const selectAlreadyBuiltCodeFn = (state: ImportState) => state.newGit?.content?.lastScan.builtModules;
const selectBuildCodeFn = (state: ImportState) => state.build.code;
const selectBuildMetadataFn = (state: ImportState) => state.build.metadata;


export const selectStudioState = createFeatureSelector<StudioState>(StudioFeatureKey);
export const selectImportState = createSelector(selectStudioState, selectImportFn);
export const selectPanelIndex = createSelector(selectImportState, selectPanelIndexFn);
export const selectPanels = createSelector(selectImportState, selectPanelsFn);
export const selectExistingGitsList = createSelector(selectImportState, selectExistingGitsListFn);
export const selectNewGitContent = createSelector(selectImportState, selectNewGitContentFn);
export const selectNotBuiltCode = createSelector(selectImportState, selectNotBuiltCodeFn);
export const selectAlreadyBuiltCode = createSelector(selectImportState, selectAlreadyBuiltCodeFn);
export const selectBuildCode = createSelector(selectImportState, selectBuildCodeFn);
export const selectBuildMetadata = createSelector(selectImportState, selectBuildMetadataFn);


