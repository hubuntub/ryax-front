// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { FormValues, OutputFormValues } from '../../entities/workflow-step';
import { StudioState } from '../reducers/index';
import { BuilderActions } from '../actions';
import {
  selectConfigLoading,
  selectCurrentIndex, selectCurrentPanelIndex,
  selectCurrentStep,
  selectDeployable, selectEndpointValue, selectEveryInput, selectEveryOutput, selectIds, selectIsAllSet,
  selectIsCurrentStepSource,
  selectLastSave, selectModuleDynamicOutput, selectModulesDescriptions,
  selectPreviousOutputs, selectProcessList, selectResults, selectSourceList,
  selectStatusList, selectStudioErrors,
  selectWorkflow,
  selectWorkflowName, selectWorkflowNameLoading
} from '../selectors/builder.selectors';

@Injectable({
  providedIn: 'root'
})
export class BuilderFacade {

  public workflowName$ = this.store.select(selectWorkflowName);
  public workflowNameLoading$ = this.store.select(selectWorkflowNameLoading);
  public workflow$ = this.store.select(selectWorkflow);
  public currentIndex$ = this.store.select(selectCurrentIndex);
  public currentStep$ = this.store.select(selectCurrentStep);
  public currentPanelIndex$ = this.store.select(selectCurrentPanelIndex);
  public statusList$ = this.store.select(selectStatusList);
  public deployable$ = this.store.select(selectDeployable);
  public lastSave$ = this.store.select(selectLastSave);
  public previousOutputs$ = this.store.select(selectPreviousOutputs);
  public isCurrentStepSource$ = this.store.select(selectIsCurrentStepSource);
  public sourceList$ = this.store.select(selectSourceList);
  public processList$ = this.store.select(selectProcessList);
  public dynamicOutputs$ = this.store.select(selectModuleDynamicOutput);
  public modulesDescriptions$ = this.store.select(selectModulesDescriptions);
  public isAllSet$ = this.store.select(selectIsAllSet);
  public ids$ = this.store.select(selectIds);
  public results$ = this.store.select(selectResults);
  public everyInput$ = this.store.select(selectEveryInput);
  public everyOutput$ = this.store.select(selectEveryOutput);
  public configLoading$ = this.store.select(selectConfigLoading);
  public endpointValue$ = this.store.select(selectEndpointValue);
  public studioErrors$ = this.store.select(selectStudioErrors);


  constructor(
    private readonly store: Store<StudioState>
  ) { }

  public create() {
    this.store.dispatch(BuilderActions.create());
  }

  public loadWorkflow(workflowId: string) {
    this.store.dispatch(BuilderActions.loadWorkflow(workflowId));
  }

  public getModules() {
    this.store.dispatch(BuilderActions.getModules())
  }

  public toStep(stepIndex: number) {
    this.store.dispatch(BuilderActions.toStep(stepIndex));
  }

  public updatePanelIndex(index: number) {
    this.store.dispatch(BuilderActions.toPanel(index));
  }

  public async addStep() {
    if (await this.isAllSet$.pipe(take(1)).toPromise()) {
      this.store.dispatch(BuilderActions.addStep());
    }
  }

  public removeCurrentStep() {
    this.store.dispatch(BuilderActions.removeCurrentStep());
  }

  public editName(name: string) {
    this.store.dispatch(BuilderActions.editName(name));
  }

  public reorganize(previousIndex: number, currentIndex: number) {
    this.store.dispatch(BuilderActions.reorganize(previousIndex, currentIndex));
  }

  public addModule(selectedModuleId: string) {
    this.store.dispatch(BuilderActions.choseStepModule(selectedModuleId))
  }

  public updateModule(selectedModuleId: string) {
    this.store.dispatch(BuilderActions.updateStepSelection(selectedModuleId))
  }

  public saveForm(form: FormValues, valid: boolean) {
    this.store.dispatch(BuilderActions.updateStepConfiguration(form, valid));
  }

  public saveDynamicForm(form: OutputFormValues, valid: boolean) {
    this.store.dispatch(BuilderActions.updateDynamicStepConfiguration(form, valid));
  }

  public deploy() {
    this.store.dispatch(BuilderActions.deploy());
  }

  public done() {
    this.store.dispatch(BuilderActions.done());
  }

  public addOutput() {
    this.store.dispatch(BuilderActions.addOutput());
  }

  public removeOutput(id: string) {
    this.store.dispatch(BuilderActions.removeOutput(id));
  }

  public saveFile() {
    this.store.dispatch(BuilderActions.saveFile());
  }

  public saveFileSuccess(inputId: string, fileName: string) {
    this.store.dispatch(BuilderActions.saveFileSuccess(inputId, fileName));
  }

  public saveFileError(err: HttpErrorResponse) {
    this.store.dispatch(BuilderActions.saveFileError(err));
  }

  public removeFile(inputId: string) {
    this.store.dispatch(BuilderActions.removeFile(inputId));
  }

  public getResults() {
    this.store.dispatch(BuilderActions.getResults());
  }

  public addResult() {
    this.store.dispatch(BuilderActions.addResults());
  }

  public removeResult(index: number) {
    this.store.dispatch(BuilderActions.removeResult(index));
  }

  public saveResults(results: { key: string, value: string }[]) {
    this.store.dispatch(BuilderActions.saveResults(results));
  }

  public saveEndpoint(value: string) {
    this.store.dispatch(BuilderActions.saveEndpoint(value));
  }

  public saveActionName(name: string) {
    this.store.dispatch(BuilderActions.saveActionName(name));
  }

  public changeVersion(id: string) {
    this.store.dispatch(BuilderActions.changeVersion(id));
  }
}
