// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ScannedModule } from '../../entities/git-scan';
import { Metadata } from '../../entities/metadata';
import { StudioState } from '../reducers/index';
import {
  selectAlreadyBuiltCode,
  selectBuildMetadata,
  selectExistingGitsList,
  selectNewGitContent, selectNotBuiltCode,
  selectPanelIndex,
  selectPanels
} from '../selectors/import.selectors';
import { ImportActions } from '../actions';

@Injectable({
  providedIn: 'root'
})
export class ImportFacade {

  public panelIndex$ = this.store.select(selectPanelIndex);
  public panels$ = this.store.select(selectPanels);
  public existingGits$ = this.store.select(selectExistingGitsList);
  public gitContent$ = this.store.select(selectNewGitContent);
  public metadata$ = this.store.select(selectBuildMetadata);
  public notBuilt$ = this.store.select(selectNotBuiltCode);
  public alreadyBuilt$ = this.store.select(selectAlreadyBuiltCode);

  constructor(
    private readonly store: Store<StudioState>
  ) { }

  public loadExistingGit() {
    this.store.dispatch(ImportActions.loadExistingRepos());
  }

  public goToPanel(index: number) {
    this.store.dispatch(ImportActions.toPanel(index));
  }

  public codeDropped(code: any) {
    this.store.dispatch(ImportActions.codeDropped(code));
  }

  public loadNewGit(url: string) {
    this.store.dispatch(ImportActions.loadNewRepo(url));
  }

  public selectContent(scanned: ScannedModule) {
    this.store.dispatch(ImportActions.selectContent(scanned));
  }

  public saveMetadata(value: Metadata) {
    this.store.dispatch(ImportActions.saveMetadata(value));
  }

  public build() {
    this.store.dispatch(ImportActions.build());
  }

  public done() {
    this.store.dispatch(ImportActions.done());
  }

}
