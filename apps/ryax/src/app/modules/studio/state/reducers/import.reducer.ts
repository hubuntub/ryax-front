// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { mutableOn } from 'ngrx-etc';
import { GitScan, ScannedModule } from '../../entities/git-scan';
import { ImportPanel } from '../../entities/import-panel';
import { Metadata } from '../../entities/metadata';
import { ImportActions } from '../actions';

export interface ImportState {
  panels: ImportPanel[];
  panelIndex: number;
  existingGits: {
    list: any[]
    selectedIndex: number | null
    loading: boolean
    error: HttpErrorResponse | null
  }
  newGit: {
    url: string
    content: GitScan | null
    loading: boolean
    error: HttpErrorResponse | null
  }
  build: {
    code: ScannedModule | any | null
    selectedFileId: string | null
    metadata: Metadata
    loading: boolean
    error: HttpErrorResponse | null
  }
}

const initialPanelsState: ImportPanel[] = [
  { title: 'Load your code', status: 'process', disabled: false },
  { title: 'Select module', status: 'wait', disabled: true },
  { title: 'Define metadata', status: 'wait', disabled: true },
  { title: 'Build', status: 'wait', disabled: true }
]

export const initialImportState: ImportState = {
  panels: initialPanelsState,
  panelIndex: 0,
  existingGits: {
    list: [],
    selectedIndex: null,
    loading: false,
    error: null
  },
  newGit: {
    url: '',
    content: null,
    loading: false,
    error: null
  },
  build: {
    code: null,
    selectedFileId: null,
    metadata: {
      moduleName: '',
      dependencies: '',
      language: '',
      inputs: [],
      outputs: []
    },
    loading: false,
    error: null
  }
};

export const importReducer = createReducer<ImportState>(
  initialImportState,
  on(ImportActions.start, ImportActions.done, () => ({
    ...initialImportState
  })),
  on(ImportActions.loadExistingRepos, (state) => ({
    ...state,
    existingGits: {
      list: state.existingGits.list,
      selectedIndex: null,
      loading: true,
      error: null
    }
  })),
  on(ImportActions.loadExistingReposSuccess, (state, { gitContents }) => ({
    ...state,
    existingGits: {
      list: gitContents,
      selectedIndex: null,
      loading: false,
      error: null
    }
  })),
  on(ImportActions.loadExistingReposError, (state, { error }) => ({
    ...state,
    existingGits: {
      list: state.existingGits.list,
      selectedIndex: null,
      loading: false,
      error: error
    }
  })),
  mutableOn(ImportActions.selectRepo, (state, { index }) => {
    state.panels[0].status = 'finish';
    state.panels[1].status = 'process';
    state.panels[1].disabled = false;
    state.panelIndex = 1;
    state.existingGits.selectedIndex = index;
    return state
  }),
  mutableOn(ImportActions.loadNewRepo, (state, { gitUrl }) => {
    state.newGit = {
      url: gitUrl,
      content: null,
      loading: true,
      error: null
    }
    return state
  }),
  mutableOn(ImportActions.loadNewRepoSuccess, (state, { gitScan }) => {
    state.panels[0].status = 'finish';
    state.panels[1].status = 'process';
    state.panels[1].disabled = false;
    state.panelIndex = 1;
    console.log('gitContent', gitScan);
    state.newGit.content = gitScan;
    state.newGit.loading = false;
    return state
  }),
  mutableOn(ImportActions.loadNewRepoError, (state, { error }) => {
    state.newGit.loading = false;
    state.newGit.error = error;
    return state;
  }),
  mutableOn(ImportActions.selectContent, (state, { scannedModule }) => {
    state.panels[1].status = 'finish';
    state.panels[2].disabled = true;
    state.panels[3].status = 'process';
    state.panels[3].disabled = false;
    state.panelIndex = 3;
    state.build.code = scannedModule;
    // state.build.metadata = {}; TODO make it so scanned modules have inputs and other things in the metadata
    return state
  }),
  mutableOn(ImportActions.codeDropped, (state, { code }) => {
    state.panels[0].status = 'finish';
    state.panels[1].status = 'finish';
    state.panels[1].disabled = true;
    state.panels[2].status = 'process';
    state.panels[2].disabled = false;
    state.build.code = code;
    state.panelIndex = 2;
    return state;
  }),
  mutableOn(ImportActions.saveMetadata, (state, { metadata }) => {
    state.panels[3].status = 'process';
    state.panels[3].disabled = false;
    state.build.metadata = metadata;
    state.panelIndex = 3;
    return state;
  }),
  on(ImportActions.toPanel, (state, { index }) => ({
    ...state,
    panelIndex: index
  })),
);
