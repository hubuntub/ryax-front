// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { builderReducer, BuilderState } from './builder.reducer';
import { importReducer, ImportState } from './import.reducer';

export * from './builder.reducer';

export const StudioFeatureKey = 'studioDomain';

export interface StudioState {
  builder: BuilderState,
  import: ImportState
}

export const StudioReducerToken = new InjectionToken<ActionReducerMap<StudioState>>(StudioFeatureKey);

export const StudioReducerProvider = {
  provide: StudioReducerToken,
  useValue: {
    builder: builderReducer,
    import: importReducer
  },
};
