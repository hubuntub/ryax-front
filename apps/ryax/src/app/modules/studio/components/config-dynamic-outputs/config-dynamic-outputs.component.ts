// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, take, withLatestFrom } from 'rxjs/operators';
import { ModuleOutput } from '../../entities/workflow-module';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-config-dynamic-outputs',
  templateUrl: './config-dynamic-outputs.component.pug',
  styleUrls: ['./config-dynamic-outputs.component.scss']
})
export class ConfigDynamicOutputsComponent implements OnChanges, OnDestroy {

  @Input() public config!: ModuleOutput[];

  public form = this.fb.group({});
  private sub: Subscription | undefined;
  public outputTypes: string[] = ['string', 'integer', 'password', 'longstring', 'float', 'enum', 'file', 'directory'];

  constructor(
    private fb: FormBuilder,
    private facade: BuilderFacade
  ) { }

  public ngOnChanges() {
    this.sub?.unsubscribe();
    this.createNewForm();
  }

  private createNewForm() {
    this.form = this.fb.group({});

    this.config.forEach(async (conf) => {
      // Create Form Control
      this.form.addControl(conf.id, this.fb.group({
        type: this.fb.control(conf.type, Validators.required),
        help: this.fb.control(conf.help, Validators.required),
        name: this.fb.control(conf.name, Validators.required),
      }));

      // if (conf.type === 'enum') {
      //   (this.form.get(conf.id) as FormGroup).addControl('possibleValues', this.fb.array(conf.possibleValues ?? []))
      // }

      // Set value from state in control
      const currentStep = await this.facade.currentStep$.pipe(take(1)).toPromise();
      if (currentStep.panels[1].outputFormValues[conf.id]) {
        this.form.get(conf.id)?.setValue(currentStep.panels[1].outputFormValues[conf.id]);
      }
    });

    // Auto-save
    this.sub = this.form.valueChanges.pipe(
      distinctUntilChanged((prev, curr) => JSON.stringify(prev) === JSON.stringify(curr)),
      withLatestFrom(this.facade.currentStep$),
      filter(([values, currentStep]) => JSON.stringify(values) !== JSON.stringify(currentStep.panels[1].outputFormValues)),
      debounceTime(500)
    ).subscribe((value) => {
      this.facade.saveDynamicForm(value[0], this.form.valid);
    });
  }

  public addOutput() {
    this.facade.addOutput()
  }

  public removeOutput(id: string) {
    this.facade.removeOutput(id);
  }

  // public addValue(id: string) {
  //   (this.form.get(id + '.possibleValues') as FormArray).push(this.fb.control(''))
  // }
  //
  // public removeValue(id: string, index: number) {
  //   (this.form.get(id + '.possibleValues') as FormArray).removeAt(index);
  // }

  public ngOnDestroy() {
    this.sub?.unsubscribe();
  }

}
