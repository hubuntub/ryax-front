// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkflowModuleDetailed } from '../../entities/workflow-module';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-module-description',
  templateUrl: './module-description.component.pug',
  styleUrls: ['./module-description.component.scss']
})
export class ModuleDescriptionComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public module: WorkflowModuleDetailed | undefined;

  public currentVersion = this.fb.control(null);
  public sub = new Subscription();

  constructor(
    private fb: FormBuilder,
    private facade: BuilderFacade
  ) {}

  public ngOnInit() {
    this.sub.add(this.currentVersion.valueChanges.pipe(
      map((name) => this.module?.versions.find((version) => version.name === name)?.id)
    ).subscribe((id) => {
      if (id) {
        this.facade.changeVersion(id)
      }
    }));
  }

  public ngOnChanges(changes: SimpleChanges) {
    this.currentVersion.patchValue(changes['module'].currentValue.version, { emitEvent: false });
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
