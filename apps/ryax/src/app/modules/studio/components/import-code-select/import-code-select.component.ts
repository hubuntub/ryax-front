// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { ScannedModule } from '../../entities/git-scan';
import { ImportFacade } from '../../state/facade/import-facade.service';

@Component({
  selector: 'ryax-import-code-select',
  templateUrl: './import-code-select.component.pug',
  styleUrls: ['./import-code-select.component.scss']
})
export class ImportCodeSelectComponent {

  public gitContent$ = this.importFacade.gitContent$;
  public notBuilt$ = this.importFacade.notBuilt$;
  public alreadyBuilt$ = this.importFacade.alreadyBuilt$;

  constructor(
    private importFacade: ImportFacade
  ) { }

  public selectScannedModule(scan: ScannedModule) {
    this.importFacade.selectContent(scan);
  }

  // public takeAll() {
  //   this.importFacade.takeAllContent();
  // }

}
