// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ImportFacade } from '../../state/facade/import-facade.service';

@Component({
  selector: 'ryax-import-git',
  templateUrl: './import-git.component.pug',
  styleUrls: ['./import-git.component.scss']
})
export class ImportGitComponent implements OnInit {

  public gitUrl = '';
  public existingGits$ = this.importFacade.existingGits$;

  constructor(
    private importFacade: ImportFacade
  ) { }

  public ngOnInit(): void {
    this.importFacade.loadExistingGit();
  }

  public loadGit() {
    this.importFacade.loadNewGit(this.gitUrl);
  }

  public fileDropped(event: any) {
    console.log(event);
    this.importFacade.codeDropped(event);
  }

}
