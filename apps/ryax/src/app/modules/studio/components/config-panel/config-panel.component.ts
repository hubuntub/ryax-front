// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { WorkflowModuleLight } from '../../entities/workflow-module';
import { PanelStatus } from '../../entities/workflow-step';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-config-panel',
  templateUrl: './config-panel.component.pug',
  styleUrls: ['./config-panel.component.scss']
})
export class ConfigPanelComponent implements OnInit {

  public panelsDisplay: boolean[] = [true, false];

  public isSource = true;

  public sourceList$ = this.builderFacade.sourceList$;
  public processList$ = this.builderFacade.processList$;

  public searchTerm = '';

  public currentStep$ = this.builderFacade.currentStep$;
  public currentIndex$ = this.builderFacade.currentIndex$;
  public currentPanelIndex$ = this.builderFacade.currentPanelIndex$;

  public currentStepNameInput = '';
  public editStepName = false;

  constructor(
    public builderFacade: BuilderFacade
  ) { }

  public ngOnInit(): void {
    this.builderFacade.getModules();

    this.builderFacade.statusList$.subscribe((statusList: PanelStatus[]) => {
      const indexOpen = statusList.findIndex((status) => status !== 'finish');
      this.panelsDisplay = statusList.map((value, index) => index === indexOpen || (indexOpen === -1 && index === statusList.length - 1));
    });

    this.builderFacade.currentIndex$.subscribe((index: number) => {
      this.isSource = index === 0;
    });
  }

  public selectModule(module: WorkflowModuleLight, newModule: boolean) {
    if (newModule) {
      this.builderFacade.addModule(module.id);
    } else {
      this.builderFacade.updateModule(module.id);
    }
    this.panelsDisplay = [false, true];
    this.editStepName = false;
  }

  public updatePanelIndex(index: number) {
    this.builderFacade.updatePanelIndex(index);
    this.editStepName = false;
  }

  public removeStep() {
    this.builderFacade.removeCurrentStep();
    this.editStepName = false;
  }

  public async startEdit() {
    this.editStepName = true;
    this.currentStepNameInput = (await this.currentStep$.pipe(take(1)).toPromise()).name;
  }

  public validate() {
    this.editStepName = false;
    this.builderFacade.saveActionName(this.currentStepNameInput);
  }
}
