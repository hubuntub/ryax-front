// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { ImportFacade } from '../../state/facade/import-facade.service';

@Component({
  selector: 'ryax-import',
  templateUrl: './import.component.pug',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent {

  public panelIndex$ = this.importFacade.panelIndex$;
  public panels$ = this.importFacade.panels$;

  constructor(
    private importFacade: ImportFacade
  ) { }

  public onIndexChange(event: number): void {
    this.importFacade.goToPanel(event);
  }

}
