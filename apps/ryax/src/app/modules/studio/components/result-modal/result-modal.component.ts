// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-result-modal',
  templateUrl: './result-modal.component.pug',
  styleUrls: ['./result-modal.component.scss']
})
export class ResultModalComponent implements OnInit, OnChanges, OnDestroy {

  public results$ = this.builderFacade.results$;
  public everyInput$ = this.builderFacade.everyInput$;
  public everyOutput$ = this.builderFacade.everyOutput$;
  @Input() public isVisible = false;
  @Output() public isVisibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  public form = this.fb.group({});
  private subs = new Subscription();

  constructor(
    private builderFacade: BuilderFacade,
    private fb: FormBuilder
  ) { }

  public ngOnInit(): void {
    this.subs.add(this.results$.subscribe((results) => {
      results.forEach((result, index) => {
        this.form.addControl(index.toString(), this.fb.group({
          key: this.fb.control(result.key, Validators.required),
          value: this.fb.control(result.value, Validators.required)
        }))
      })
    }));
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['isVisible'].currentValue === true) {
      this.builderFacade.getResults();
    }
  }

  public addResult() {
    this.builderFacade.addResult();
  }

  public removeResult(index: number) {
    this.builderFacade.removeResult(index);
  }

  public handleCancel() {
    this.isVisibleChange.emit(false);
  }

  public handleOk() {
    this.builderFacade.saveResults(Object.values(this.form.value));
    this.isVisibleChange.emit(false);
  }

  public ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
