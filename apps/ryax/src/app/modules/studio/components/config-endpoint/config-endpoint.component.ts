import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, take, withLatestFrom } from 'rxjs/operators';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-config-endpoint',
  templateUrl: './config-endpoint.component.pug',
  styleUrls: ['./config-endpoint.component.scss']
})
export class ConfigEndpointComponent implements OnInit, OnDestroy {

  public form: FormControl = this.fb.control(undefined, Validators.required);
  private subs = new Subscription();

  constructor(
    private fb: FormBuilder,
    private builderFacade: BuilderFacade
  ) { }

  public ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    // Set value from state in control
    this.subs.add(this.builderFacade.endpointValue$.subscribe((value) => {
      this.form.setValue(value);
    }));

    // Auto-save
    this.subs.add(this.form.valueChanges.pipe(
      distinctUntilChanged(),
      withLatestFrom(this.builderFacade.endpointValue$),
      filter(([value, savedEndpoint]) => value !== savedEndpoint),
      debounceTime(500)
    ).subscribe((value) => {
      this.builderFacade.saveEndpoint(value[0]);
    }));
  }

  public ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
