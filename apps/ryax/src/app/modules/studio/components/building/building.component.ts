// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-building',
  templateUrl: './building.component.pug',
  styleUrls: ['./building.component.scss']
})
export class BuildingComponent implements OnInit {

  public loading = true;
  public loadingValue = 0;

  constructor(
    private builderFacade: BuilderFacade,
  ) { }

  public ngOnInit(): void {
    timer(200, 500).pipe(take(5)).subscribe(() => {
      this.loadingValue += 20;
      if (this.loadingValue === 100) {
        this.loading = false;
      }
    });
  }

  public done() {
    this.builderFacade.done()
  }
}
