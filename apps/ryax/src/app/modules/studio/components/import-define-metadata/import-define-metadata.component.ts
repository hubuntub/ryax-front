// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { ImportFacade } from '../../state/facade/import-facade.service';

@Component({
  selector: 'ryax-import-define-metadata',
  templateUrl: './import-define-metadata.component.pug',
  styleUrls: ['./import-define-metadata.component.scss']
})
export class ImportDefineMetadataComponent implements OnInit {

  public form = this.fb.group({
    moduleName: this.fb.control('', Validators.required),
    language: this.fb.control('', Validators.required),
    dependencies: this.fb.control('', Validators.required),
    inputs: this.fb.array([]),
    outputs: this.fb.array([])
  });

  public languages = [
    { name: 'Javascript', packageFile: 'package.json' },
    { name: 'Python', packageFile: 'requirement.txt' },
    { name: 'Java', packageFile: 'package file' },
    { name: 'Julia', packageFile: 'package file' },
    { name: 'C#', packageFile: 'package file' },
  ];

  public typeList = [
    { name: 'Text', value: 'text' },
    { name: 'Long Text', value: 'textarea' },
    { name: 'Number', value: 'number' },
    { name: 'File', value: 'file' }
  ]

  public selectedPackage = 'package file';

  get formInputs() {
    return this.form.get('inputs') as FormArray;
  }

  get formOutputs() {
    return this.form.get('outputs') as FormArray;
  }

  constructor(
    private fb: FormBuilder,
    private importFacade: ImportFacade
  ) { }

  public ngOnInit(): void {
    this.form.get('language')?.valueChanges.subscribe((value) => {
      this.selectedPackage = value.packageFile;
    });
  }

  public addInput() {
    this.formInputs.push(this.fb.group({
      name: this.fb.control('', Validators.required),
      type: this.fb.control('', Validators.required),
    }));
  }

  public removeInput(index: number) {
    this.formInputs.removeAt(index);
  }

  public addOutput() {
    this.formOutputs.push(this.fb.group({
      name: this.fb.control('', Validators.required),
      type: this.fb.control('', Validators.required),
    }));
  }

  public removeOutput(index: number) {
    this.formOutputs.removeAt(index);
  }

  public saveMetadata() {
    this.importFacade.saveMetadata(this.form.value);
  }

}
