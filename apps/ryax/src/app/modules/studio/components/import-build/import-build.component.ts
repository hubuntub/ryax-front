// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { take, withLatestFrom } from 'rxjs/operators';
import { BuilderFacade } from '../../state/facade/builder-facade.service';
import { ImportFacade } from '../../state/facade/import-facade.service';

@Component({
  selector: 'ryax-import-build',
  templateUrl: './import-build.component.pug',
  styleUrls: ['./import-build.component.scss'],
})
export class ImportBuildComponent implements OnInit {

  public loading = true;
  public loadingValue = 0;
  public metadata$ = this.importFacade.metadata$;

  constructor(
    private builderFacade: BuilderFacade,
    private importFacade: ImportFacade,
  ) { }

  public ngOnInit(): void {
    this.importFacade.build();

    timer(200, 500).pipe(take(5), withLatestFrom(this.metadata$)).subscribe(([, meta]) => {
      this.loadingValue += 20;
      if (this.loadingValue === 100) {
        this.loading = false;
        // this.builderFacade.selectModule({
        //   id: 'your-module',
        //   name: meta.moduleName,
        //   kind: 'Process',
        //   description: 'Your very own module',
        //   mainValue: meta.inputs[0] ? meta.inputs[0].name : '',
        //   configurator: meta.inputs.map((input) => {
        //     return {
        //       id: input.name,
        //       name: input.name,
        //       inputType: 'text'
        //     }
        //   })
        // })
      }
    });
  }

  public done() {
    this.importFacade.done();
  }

}
