// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-create-workflow',
  templateUrl: './create-workflow.component.pug',
  styleUrls: ['./create-workflow.component.scss']
})
export class CreateWorkflowComponent {

  constructor(
    private builderFacade: BuilderFacade
  ) { }

  public newWorkflow(): void {
    this.builderFacade.create();
  }

}
