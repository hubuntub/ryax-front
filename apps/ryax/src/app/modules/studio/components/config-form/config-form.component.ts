// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, take, withLatestFrom } from 'rxjs/operators';
import { Config } from '../../entities/workflow-module';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-config-form',
  templateUrl: './config-form.component.pug',
  styleUrls: ['./config-form.component.scss']
})
export class ConfigFormComponent implements OnChanges, OnDestroy {

  @Input() public config!: Config[];

  public form: FormGroup = this.fb.group({});
  public previousOutputs$ = this.builderFacade.previousOutputs$;
  public isCurrentStepSource$ = this.builderFacade.isCurrentStepSource$;
  public ids$ = this.builderFacade.ids$;
  private sub: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private builderFacade: BuilderFacade,
  ) {}

  public ngOnChanges() {
    this.sub.unsubscribe();
    this.sub = new Subscription();
    this.createNewForm();
  }

  private createNewForm() {
    this.form = this.fb.group({});

    this.config.forEach(async (conf) => {
      // Create Form Control
      this.form.addControl(conf.id, this.fb.group({
        linked: this.fb.control(false, Validators.required),
        value: this.fb.control(null, Validators.required)
      }));

      // Reset value when switch between reference or static
      this.sub.add(this.form.get(conf.id + '.linked')?.valueChanges.subscribe(() => {
        this.form.get(conf.id + '.value')?.setValue(null);
      }));

      // Set value from state in control
      const currentStep = await this.builderFacade.currentStep$.pipe(take(1)).toPromise();
      if (currentStep.panels[1].inputFormValues[conf.id]) {
        this.form.get(conf.id)?.patchValue(currentStep.panels[1].inputFormValues[conf.id], {emitEvent: false});
      }
    });

    // Auto-save
    this.sub.add(this.form.valueChanges.pipe(
      distinctUntilChanged((prev, curr) => JSON.stringify(prev) === JSON.stringify(curr)),
      withLatestFrom(this.builderFacade.currentStep$),
      filter(([values, currentStep]) => JSON.stringify(values) !== JSON.stringify(currentStep.panels[1].inputFormValues)),
      debounceTime(500)
    ).subscribe((value) => {
      this.builderFacade.saveForm(value[0], this.form.valid);
    }));
  }

  public handleUpload(): boolean  {
    return false;
  }

  public handleChange(info: NzUploadChangeParam, inputId: string): void {
    if (info.file.status === 'uploading') {
      console.log(info.file);
    } else if (info.file.status === 'done') {
      this.builderFacade.saveFileSuccess(inputId, info.file.name)
      this.form.get(inputId + '.value')?.setValue(info.file.name)
    } else if (info.file.status === 'error') {
      this.builderFacade.saveFileError(info.file.error);
    }
  }

  public removeFile(inputId: string) {
    this.builderFacade.removeFile(inputId);
    this.form.get(inputId + '.value')?.setValue(undefined);
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
