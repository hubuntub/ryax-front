// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PortalFormData } from '../../entities';
import { PortalStore } from '../../state';
import { Location } from '@angular/common';

@Component({
  selector: 'ryax-portal-form',
  templateUrl: './portal-form.component.pug',
  styleUrls: ['./portal-form.component.scss'],
})
export class PortalFormComponent implements OnInit {
  public portal$ = this.store.portal$;
  public loading$ = this.store.loading$;
  public displayReturn = false;

  constructor(
    private store: PortalStore,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  public ngOnInit(): void {
    if (this.route.parent) {
      const currentRoute = this.route.parent.snapshot;
      this.displayReturn = currentRoute.parent?.parent?.url[0].path === 'execution';
      this.store.loadPortalInputs(currentRoute.params['workflowId']);
    }
  }

  public onSubmit(data: PortalFormData) {
    this.store.sendFormData(data);
  }

  public back() {
    this.location.back()
  }
}
