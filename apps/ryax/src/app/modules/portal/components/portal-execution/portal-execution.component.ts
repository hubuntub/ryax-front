// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import { ModuleExecution } from '../../../shared/entity';
import { PortalStore } from '../../state';

@Component({
  selector: 'ryax-portal-execution',
  templateUrl: './portal-execution.component.pug',
  styleUrls: ['./portal-execution.component.scss']
})
export class PortalExecutionComponent implements OnInit {

  public executionId$ = this.store.executionId$;
  public execution$ = this.store.execution$;
  public logs$ = this.store.logs$;
  public results$ = this.store.results$;
  public showAllDetails: boolean[] = [];
  public showResults = false;
  public resultUrl = '';

  constructor(
    private readonly store: PortalStore,
    private route: ActivatedRoute
  ) { }

  public async ngOnInit() {
    if (this.route.snapshot.url[0].path === 'execution') {
      this.store.getExecutionStatus(this.executionId$);
      this.store.getResults(this.executionId$);
      this.resultUrl = '/api/runner/results/' + await this.executionId$.pipe(take(1)).toPromise()
    } else {
      this.store.getExecutionStatus(of(this.route.snapshot.url[0].path));
      this.store.getResults(of(this.route.snapshot.url[0].path));
      this.resultUrl = '/api/runner/results/' + this.route.snapshot.url[0].path;
    }
    this.execution$.pipe(take(1)).subscribe((execution) => {
      if (execution) {
        this.showAllDetails = execution.moduleExecutions.map(() => false);
      }
    });
  }

  public areAllDetailsOpen(): boolean {
    return this.showAllDetails.reduce((answer, current) => answer || current, false);
  }

  public toggleAllDetails() {
    if (this.areAllDetailsOpen()) {
      this.showAllDetails = this.showAllDetails.map(() => false);
    } else {
      this.showAllDetails = this.showAllDetails.map(() => true);
    }
  }

  public getLogs(index: number) {
    this.store.getLogs(index);
  }

  public trackByFn(index: number, item: ModuleExecution) {
    return item.id
  }

  public displayResults(): void {
    this.showResults = true;
  }

  public handleCancel(): void {
    this.showResults = false;
  }

}
