// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ModuleExecutionStatus, WorkflowExecutionStatus } from '../../../shared/entity';
import { mockPipe } from '../../../shared/pipes';
import { PortalStore } from '../../state';

import { PortalExecutionComponent } from './portal-execution.component';

describe('PortalExecutionComponent', () => {
  let component: PortalExecutionComponent;
  let fixture: ComponentFixture<PortalExecutionComponent>;

  const portalStoreSpy = {
    getExecutionStatus: jest.fn(),
    execution$: of({
      id: 'exec_id',
      status: WorkflowExecutionStatus.Created,
      startedAt: new Date(0),
      moduleExecutions: [
        {
          id: 'module-id',
          name: 'module-name',
          version: 'module-version',
          description: 'module-description',
          status: ModuleExecutionStatus.Waiting
        }
      ]
    }),
    executionId$: of('exec_id')
  };

  const routeSpy = {
    snapshot: {
      url: [{
        path: 'execution'
      }]
    }
  }

  beforeEach(async () => {
    portalStoreSpy.getExecutionStatus.mockClear();

    await TestBed.configureTestingModule({
      declarations: [
        PortalExecutionComponent,
        mockPipe('statusTagColor'),
        mockPipe('statusName'),
        mockPipe('statusBarColor'),
      ],
      providers: [
        { provide: ActivatedRoute, useValue: routeSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    TestBed.overrideProvider(PortalStore, { useValue: portalStoreSpy });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call execution and set variables onInit - portal case', () => {
    routeSpy.snapshot.url[0].path = 'execution';

    expect(portalStoreSpy.getExecutionStatus).toHaveBeenCalled();
    expect(component.showAllDetails).toEqual([false]);
  });

  it('should call execution and set variables onInit - execution detail case', () => {
    routeSpy.snapshot.url[0].path = 'execution_id';

    expect(portalStoreSpy.getExecutionStatus).toHaveBeenCalled();
    expect(component.showAllDetails).toEqual([false]);
  });

  it('should give a state depending on the details shown', () => {
    expect(component.areAllDetailsOpen()).toEqual(false);

    component.showAllDetails[0] = true;

    expect(component.areAllDetailsOpen()).toEqual(true);
  });

  it('should show all details when asked', () => {
    component.toggleAllDetails();

    expect(component.showAllDetails).toEqual([true]);
  });

  it('should hide all details when asked', () => {
    component.toggleAllDetails();
    component.toggleAllDetails();

    expect(component.showAllDetails).toEqual([false]);
  });
});
