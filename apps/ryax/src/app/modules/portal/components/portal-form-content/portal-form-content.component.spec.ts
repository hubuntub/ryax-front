// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FormInputType } from '../../entities';

import { PortalFormContentComponent } from './portal-form-content.component';

describe('PortalFormInputsComponent', () => {
  let component: PortalFormContentComponent;
  let fixture: ComponentFixture<PortalFormContentComponent>;
  const inputsLoaded = {
    portalInputs: {
      currentValue: [{
        id: '123',
        technicalName: 'testName',
        displayName: 'Test Name',
        type: FormInputType.STRING,
        help: 'Help string',
        enumValues: [],
        workflowModuleId: 'abc',
        workflowId: 'def'
      }]
    } as SimpleChange
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      providers: [FormBuilder],
      declarations: [PortalFormContentComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalFormContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add received inputs to the form', () => {
    component.ngOnChanges(inputsLoaded);

    expect(component.form.get('123')).toBeTruthy();
  });

  it('should emit an event on form submit', () => {
    const spy = jest.spyOn(component.submitEmitter, 'emit');

    component.ngOnChanges(inputsLoaded);
    component.form.setValue({ 123: 'test' });
    component.onSubmit();

    expect(spy).toHaveBeenCalledWith({ 123: 'test' });
  });

  it('should return if input matches type defined', () => {
    const wantedTypes = [FormInputType.STRING, FormInputType.FILE];

    expect(component.matchInput(wantedTypes, FormInputType.STRING)).toEqual(true);
    expect(component.matchInput(wantedTypes, FormInputType.FILE)).toEqual(true);
    expect(component.matchInput(wantedTypes, FormInputType.FLOAT)).toEqual(false);
  });

  it('should switch input type when asked', () => {
    const input = document.createElement('input');
    input.type = 'text';

    component.switchType(input);

    expect(input.type).toEqual('password');

    component.switchType(input);

    expect(input.type).toEqual('text');
  });
});
