// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { DateTime } from 'luxon';
import { WorkflowExecution, WorkflowExecutionStatus } from '../../../shared/entity';
import { WorkflowExecutionDto } from '../../../shared/entity/dtos';
import { ExecutionIdentifier, FormInputType, Portal } from '../../entities';
import { ModuleRunDto, WorkflowPortalDto } from '../../entities/dtos';
import { PortalApi } from './portal-api.service';

const loadPortalResponseBody: WorkflowPortalDto = {
  workflow_deployment_id: 'test',
  workflow_definition_id: 'test2',
  name: 'my-portal-form',
  outputs: [
    {
      technical_name: 'tech_name',
      id: 'id',
      type: 'FILE',
      display_name: 'Sales history',
      help: 'New output help'
    },
    {
      technical_name: 'tech_name2',
      id: 'id2',
      type: 'STRING',
      display_name: 'Email',
      help: 'New output help'
    }
  ]
};

const loadPortalResult: Portal = {
  id: 'test2',
  name: 'my-portal-form',
  workflowId: 'abc',
  outputs: [
    {
      technicalName: 'tech_name',
      id: 'id',
      type: FormInputType.FILE,
      displayName: 'Sales history',
      help: 'New output help'
    },
    {
      technicalName: 'tech_name2',
      id: 'id2',
      type: FormInputType.STRING,
      displayName: 'Email',
      help: 'New output help'
    },
  ]
};

const sendFormDataResponseBody: ModuleRunDto = {
  workflow_run_id: 'my-exec'
};

const sendFormDataResult: ExecutionIdentifier = {
  id: 'my-exec'
};

const getExecutionStatusResponseBody: WorkflowExecutionDto = {
  id: 'my-exec',
  workflow_definition_id: 'workflow_id',
  started_at: '1970-01-01T00:00:00.000Z',
  last_result_at: '1970-01-01T00:00:01.000Z',
  submitted_at: '1970-01-01T00:00:00.000Z',
  total_steps: 34,
  completed_steps: 12,
  state: 'Created',
  executions: []
};

const getExecutionStatusResult: WorkflowExecution = {
  id: 'my-exec',
  status: WorkflowExecutionStatus.Created,
  duration: '1000m',
  startedAt: DateTime.now(),
  moduleExecutions: [],
  step: 12,
  totalSteps: 34
};

describe('PortalApi', () => {
  let service: PortalApi;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PortalApi
      ],
    });

    service = TestBed.inject(PortalApi);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should load portal', () => {
    expect(service.loadPortal('abc').toPromise())
    .resolves.toEqual(loadPortalResult);

    const request = httpTestingController.expectOne('/runner/portals/abc');
    expect(request.request.method).toEqual('GET');
    request.flush(loadPortalResponseBody);
  });

  it('should send form data', () => {
    expect(service.sendFormData('workflow-id', { test: 'yes', test2: ['no'] }).toPromise())
    .resolves.toEqual(sendFormDataResult);

    const request = httpTestingController.expectOne('/runner/portals/workflow-id');
    expect(request.request.method).toEqual('POST');
    request.flush(sendFormDataResponseBody);
  });

  it('should get execution status', () => {
    expect(service.getExecutionStatus('exec-id').toPromise())
    .resolves.toEqual(getExecutionStatusResult);

    const request = httpTestingController.expectOne('/runner/workflow_runs/exec-id');
    expect(request.request.method).toEqual('GET');
    request.flush(getExecutionStatusResponseBody);
  });

});
