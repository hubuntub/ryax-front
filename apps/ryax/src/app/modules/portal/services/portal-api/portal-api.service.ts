// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkflowExecution } from '../../../shared/entity';
import { ExecutionAdapter } from '../../../shared/entity/adapters';
import { WorkflowExecutionDto } from '../../../shared/entity/dtos';
import { ExecutionIdentifier, Portal, PortalFormData } from '../../entities';
import { ModuleRunAdapter, WorkflowPortalAdapter } from '../../entities/adapters';
import { ModuleRunDto, WorkflowPortalDto } from '../../entities/dtos';

@Injectable()
export class PortalApi {
  private baseUrl = '/api/runner';

  constructor(
    private readonly http: HttpClient
  ) {}

  public loadPortal(workflowId: string): Observable<Portal> {
    return this.http.get<WorkflowPortalDto>(`${this.baseUrl}/portals/${workflowId}`, { responseType: 'json' }).pipe(
      map(dto => new WorkflowPortalAdapter().adapt(dto, workflowId))
    );
  }

  public sendFormData(workflowId: string, data: PortalFormData): Observable<ExecutionIdentifier> {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value instanceof Array) {
        for (let i = 0; i < value.length; i++) {
          formData.append(key, value[i]);
        }
      } else {
        formData.append(key, value)
      }
    });

    return this.http.post<ModuleRunDto>(`${this.baseUrl}/portals/${workflowId}`, formData, { responseType: 'json' }).pipe(
      map(dto => new ModuleRunAdapter().adapt(dto))
    );
  }

  public getExecutionStatus(executionId: string): Observable<WorkflowExecution> {
    return this.http.get<WorkflowExecutionDto>(`${this.baseUrl}/workflow_runs/${executionId}`).pipe(
      map(dto => new ExecutionAdapter().adapt(dto))
    )
  }

  public getLogs(index: number, execution: WorkflowExecution | null): Observable<{ [key: string]: string }> {
    if (execution) {
      return this.http.get<{id: string, log: string}>(
        `${this.baseUrl}/workflow_runs/${execution.moduleExecutions[index].id}/logs`
      ).pipe(
        map(dto => {
          return { [index]: dto.log ? dto.log : 'Nothing was logged.' }
        })
      )
    } else {
      return of({ [index]: 'Something went wrong when loading the logs.' });
    }
  }

  public getResults(executionId: string): Observable<string> {
    return this.http.get<{ [key: string]: string }>(`${this.baseUrl}/results/${executionId}`).pipe(
      map((value) => JSON.stringify(value, undefined, 2))
    );
  }
}
