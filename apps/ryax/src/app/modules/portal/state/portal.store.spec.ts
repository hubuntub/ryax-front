// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { cold, hot } from 'jest-marbles';
import { WorkflowExecutionStatus } from '../../shared/entity';
import { FormInputType, Portal } from '../entities';
import { PortalApi } from '../services/portal-api/portal-api.service';
import { PortalState, PortalStore } from './portal.store';

const fakePortal: Portal = {
  id: 'portal_id',
  name: 'portal_name',
  workflowId: 'portal_workflowId',
  outputs: [
    {
      technicalName: 'tech_name',
      id: 'id',
      type: FormInputType.FILE,
      enumValues: [],
      displayName: 'Sales history',
      help: 'New output help'
    },
    {
      technicalName: 'tech_name2',
      id: 'id2',
      type: FormInputType.STRING,
      enumValues: [],
      displayName: 'Email',
      help: 'New output help'
    },
  ]
};

const fakeModuleExecution = [{
  id: 'fakeModuleExecution_id',
  name: 'fakeModuleExecution_name',
  version: 'fakeModuleExecution_version',
  description: 'fakeModuleExecution_description',
  status: WorkflowExecutionStatus.Created,
  inputs: [],
  outputs: [],
}];

const fakeWorkflowExecution = {
  id: 'fakeWorkflowExecution',
  status: WorkflowExecutionStatus.Created,
  startedAt: new Date(),
  modules: fakeModuleExecution,
};

// const thirdPortalInput = [{
//   technicalName: 'tech_name3',
//   id: 'id2',
//   workflowId: 'abc',
//   workflowModuleId: 'work_module_id2',
//   type: FormInputType.STRING,
//   enumValues: [],
//   displayName: 'Email',
//   help: 'New output help'
// }];

const formData = {
  'f946b952-488c-4358-bbb1-60b04e46de34': 'aze'
};

const initialState: PortalState = {
  loading: false,
  portal: fakePortal,
  fetchExecutionStatus: false,
  executionId: '',
  execution: null,
  logs: {},
  results: ''
};

describe('PortalStore', () => {
  let portalStore: PortalStore;

  const portalApiSpy = {
    loadPortal: jest.fn(),
    sendFormData: jest.fn(),
    getExecutionStatus: jest.fn()
  };

  const portalRouterSpy = {
    navigate: jest.fn()
  };

  const routeSpy = {
    parent: {
      snapshot: {
        params: {
          workflowId: 'workflowId',
          workflowPortalId: 'workflowModuleId'
        }
      }
    }
  };

  beforeEach(() => {
    portalApiSpy.loadPortal.mockClear();
    portalApiSpy.sendFormData.mockClear();
    portalApiSpy.getExecutionStatus.mockClear();
    TestBed.configureTestingModule({
      providers: [
        PortalStore,
        { provide: Router, useValue: portalRouterSpy },
        { provide: PortalApi, useValue: portalApiSpy },
        { provide: ActivatedRoute, useValue: routeSpy },
      ],
    });
    portalStore = TestBed.inject(PortalStore);
    portalStore.setState(initialState);
  });

  //selectors
  describe('Selectors', () => {
    it('should select the portal inputs', () => {
      expect(portalStore.portal$).toBeObservable(hot('a', { a: fakePortal }));
    });
  });

  //reducers
  // describe('Reducers', () => {
  //   it('should override portal inputs in state when addInputs is called', () => {
  //     cold('---a|').pipe(tap(() => portalStore.addInputs(thirdPortalInput))).subscribe();
  //     expect(portalStore.state$).toBeObservable(hot('a--b', {
  //       a: initialState,
  //       b: { ...initialState, portalInputs: thirdPortalInput }
  //     }));
  //   });
  // });

  //effects
  describe('loadPortalInputs', () => {
    it('call api success', () => {
      portalApiSpy.loadPortal.mockImplementation(() => {
        return (cold('-(a|)', { a: { ...fakePortal, id: 'portal_id2' } }));
      });

      portalStore.loadPortalInputs('workflowId');

      expect(portalApiSpy.loadPortal).toHaveBeenCalledWith('workflowId');

      expect(portalStore.state$).toBeObservable(hot('ab', {
        a: { ...initialState, loading: true },
        b: {
          ...initialState,
          loading: false,
          portal: { ...fakePortal, id: 'portal_id2' },
        }
      }));
    });


    it('call api error', () => {
      portalApiSpy.loadPortal.mockImplementation(() => {
        return (cold('-#', {}, { error: '400' }));
      });

      portalStore.loadPortalInputs('workflowId');

      expect(portalApiSpy.loadPortal).toHaveBeenCalledWith('workflowId');

      expect(portalStore.state$).toBeObservable(hot('ab', { a: { ...initialState, loading: true }, b: initialState }));
    });
  });

  describe('sendFormData', () => {
    it('call portalApi.sendFormData success', () => {
      portalApiSpy.sendFormData.mockImplementation(() => {
        return (cold('--(a|)', { a: { ...fakePortal, id: 'newExecutionId' } }));
      });

      portalStore.sendFormData(formData);

      expect(portalApiSpy.sendFormData).toHaveBeenCalledWith('portal_workflowId', formData);
      cold('--a').subscribe(() => {
        expect(portalRouterSpy.navigate).toHaveBeenCalledWith(['execution'], { relativeTo: routeSpy });
      });
      expect(portalStore.state$).toBeObservable(hot('a-b', {
        a: { ...initialState, loading: true }, b: {
          ...initialState,
          executionId: 'newExecutionId'
        }
      }));
    });

    it('call portalApi.sendFormData error', () => {
      portalApiSpy.sendFormData.mockImplementation(() => {
        return (cold('--#', {}, { error: '400' }));
      });

      portalStore.sendFormData(formData);

      expect(portalApiSpy.sendFormData).toHaveBeenCalledWith('portal_workflowId', formData);
      cold('--a').subscribe(() => {
        expect(portalRouterSpy.navigate).not.toHaveBeenCalled();
      });
      expect(portalStore.state$).toBeObservable(hot('a-b', {
        a: { ...initialState, loading: true }, b: {
          ...initialState
        }
      }));
    });
  });

  describe('getExecutionStatus', () => {
    it('call portalApi.getExecutionStatus success', () => {
      portalApiSpy.getExecutionStatus.mockImplementation(() => {
        return (cold('--(a|)', { a: { ...fakeWorkflowExecution, id: 'newExecutionId' } }));
      });

      portalStore.getExecutionStatus('execId');

      expect(portalApiSpy.getExecutionStatus).toHaveBeenCalledWith('execId');
      expect(portalStore.state$).toBeObservable(hot('a-b', {
        a: { ...initialState }, b: {
          ...initialState,
          execution: { ...fakeWorkflowExecution, id: 'newExecutionId' }
        }
      }));
    });

    it('call portalApi.sendFormData error', () => {
      portalApiSpy.getExecutionStatus.mockImplementation(() => {
        return (cold('--#', {}, { error: '400' }));
      });

      portalStore.getExecutionStatus('execId');

      expect(portalApiSpy.getExecutionStatus).toHaveBeenCalledWith('execId');
      expect(portalStore.state$).toBeObservable(hot('a', { a: { ...initialState } }));
    });
  });
});
