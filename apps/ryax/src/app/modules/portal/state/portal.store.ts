// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { EMPTY, Observable, timer } from 'rxjs';
import { catchError, expand, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { WorkflowExecutionStatus, WorkflowExecution } from '../../shared/entity';
import { Portal, PortalFormData } from '../entities';
import { PortalApi } from '../services/portal-api/portal-api.service';

export interface PortalState {
  loading: boolean;
  portal: Portal;
  fetchExecutionStatus: boolean;
  executionId: string;
  execution: WorkflowExecution | null;
  logs: { [key: string]: string };
  results: string;
}

@Injectable()
export class PortalStore extends ComponentStore<PortalState> {

  //selector
  readonly portal$: Observable<Portal> = this.select(state => state.portal);
  readonly loading$: Observable<boolean> = this.select(state => state.loading);
  readonly executionId$: Observable<string> = this.select(state => state.executionId);
  readonly execution$: Observable<WorkflowExecution | null> = this.select(state => state.execution);
  readonly logs$: Observable<{ [key: string]: string }> = this.select(state => state.logs);
  readonly results$: Observable<string> = this.select(state => state.results);

  //reducers
  readonly startLoading = this.updater((state: PortalState) => ({
    ...state,
    loading: true
  }));
  readonly savePortal = this.updater((state: PortalState, portal: Portal) => ({
    ...state,
    portal,
    loading: false
  }));
  readonly saveExecutionId = this.updater((state: PortalState, executionId: string) => ({
    ...state,
    executionId,
    loading: false
  }));
  readonly saveExecutionStatus = this.updater((state: PortalState, execution: WorkflowExecution) => ({
    ...state,
    execution
  }));
  readonly saveLogs = this.updater((state: PortalState, logs: { [key: string]: string }) => ({
    ...state,
    logs: {...state.logs, ...logs}
  }));
  readonly loadFailed = this.updater((state: PortalState) => ({
    ...state,
    loading: false
  }));
  readonly saveResults = this.updater((state: PortalState, results: string) => ({
    ...state,
    results
  }));

  //effects
  readonly loadPortalInputs = this.effect((workflowId$: Observable<string>) => {
    return workflowId$.pipe(
      tap(() => this.startLoading()),
      switchMap(
        (id) => this.portalApi.loadPortal(id).pipe(
          tap({
          next: (portalData) => {
            this.savePortal(portalData);
          },
          error: () => this.loadFailed(),
        }),
        catchError(() => EMPTY),
      )),
    );
  });

  readonly sendFormData = this.effect((formData$: Observable<PortalFormData>) => {
    return formData$.pipe(
      tap(() => this.startLoading()),
      withLatestFrom(this.portal$),
      switchMap(([data, portal]) => this.portalApi.sendFormData(portal.workflowId, data).pipe(
        tap({
          next: (execId) => {
            this.saveExecutionId(execId.id);
            this.router.navigate(['execution'], { relativeTo: this.route });
          },
          error: () => this.loadFailed(),
        }),
        catchError(() => EMPTY),
      )),
    );
  });

  readonly getExecutionStatus = this.effect((executionId$: Observable<string>) => {
    return executionId$.pipe(
      switchMap((executionId) => this.portalApi.getExecutionStatus(executionId)),
      expand(execution => {
        if (
          execution.status != WorkflowExecutionStatus.Completed &&
          execution.status != WorkflowExecutionStatus.Error
        ) {
          return timer(2000).pipe(switchMap(() => this.portalApi.getExecutionStatus(execution.id)));
        } else {
          return EMPTY;
        }
      }),
      tap({
        next: (workflowExecution: WorkflowExecution) => {
          this.saveExecutionStatus(workflowExecution);
        }
      }),
      catchError(() => EMPTY)
    );
  });

  readonly getLogs = this.effect((index$: Observable<number>) => {
    return index$.pipe(
      withLatestFrom(this.execution$),
      switchMap(([index, execution]) => this.portalApi.getLogs(index, execution)),
      tap({
        next: (logs: { [key: string]: string }) => {
          this.saveLogs(logs);
        }
      }),
      catchError(() => EMPTY)
    );
  });

  readonly getResults = this.effect((executionId$: Observable<string>) => {
    return executionId$.pipe(
      switchMap((executionId) => this.portalApi.getResults(executionId)),
      tap({
        next: (results: string) => {
          this.saveResults(results);
        }
      }),
      catchError(() => EMPTY)
    );
  });

  constructor(
    private readonly portalApi: PortalApi,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super({
      loading: false,
      portal: {} as Portal,
      fetchExecutionStatus: false,
      executionId: '',
      execution: null,
      logs: {},
      results: ''
    });
  }
}

