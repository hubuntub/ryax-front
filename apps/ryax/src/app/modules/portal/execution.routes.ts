// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { LayoutComponent } from '../shared/components/index';
import { PortalExecutionComponent } from './components/portal-execution/portal-execution.component';
import { PortalFormComponent } from './components/portal-form/portal-form.component';
import { PortalComponent } from './components/portal/portal.component';

export const executionRoutes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'historic',
        component: PortalComponent,
        children: [
          {
            path: ':executionId',
            component: PortalExecutionComponent
          },
          {
            path: '**',
            redirectTo: '/dashboard'
          }
        ]
      },
      {
        path: ':workflowId',
        component: PortalComponent,
        children: [
          {
            path: 'form',
            component: PortalFormComponent,
          },
          {
            path: 'execution',
            component: PortalExecutionComponent
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'form',
          }
        ]
      },
      {
        path: '**',
        redirectTo: '/dashboard'
      }
    ]
  }
];
