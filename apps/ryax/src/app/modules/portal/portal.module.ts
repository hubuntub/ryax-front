// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../shared/shared.module';
import { PortalExecutionComponent } from './components/portal-execution/portal-execution.component';
import { PortalFormContentComponent } from './components/portal-form-content/portal-form-content.component';
import { PortalFormComponent } from './components/portal-form/portal-form.component';
import { PortalComponent } from './components/portal/portal.component';
import { PortalApi } from './services/portal-api/portal-api.service';


@NgModule({
  declarations: [
    PortalFormComponent,
    PortalFormContentComponent,
    PortalComponent,
    PortalExecutionComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    AuthModule
  ],
  providers: [
    PortalApi
  ]
})
export class PortalModule { }
