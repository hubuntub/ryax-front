// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleRunDto } from '../../dtos';
import { ExecutionIdentifier } from '../../index';

export class ModuleRunAdapter {
  public adapt(dto: ModuleRunDto): ExecutionIdentifier {
    return {
      id: dto.workflow_run_id,
    };
  }
}
