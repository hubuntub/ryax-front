// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleRunDto } from '../../dtos';
import { ModuleRunAdapter } from './module-run.adapter';


describe('ModuleRunAdapter', () => {
  const moduleRunAdapter: ModuleRunAdapter = new ModuleRunAdapter();

  it('should adapt ModuleRunDto into ExecutionIdentifier', () => {
    const moduleRunDto: ModuleRunDto = {
      workflow_run_id: 'execution-id',
    };
    const result = moduleRunAdapter.adapt(moduleRunDto);
    expect(result).toEqual({
      id: 'execution-id'
    })
  });
});
