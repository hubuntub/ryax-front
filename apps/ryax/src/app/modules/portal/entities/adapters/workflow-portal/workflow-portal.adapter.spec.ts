// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowPortalDto } from '../../dtos';
import { WorkflowPortalAdapter } from './workflow-portal.adapter';

describe('WorkflowPortalAdapter', () => {
  const workflowPortalAdapter: WorkflowPortalAdapter = new WorkflowPortalAdapter();

  it('should adapt WorkflowPortalDto and workflowId into Portal', () => {
    const workflowPortalDto: WorkflowPortalDto = {
      name: 'workflow-name',
      workflow_deployment_id: 'workflow-id2',
      workflow_definition_id: 'workflow-id'
    };
    const workflowId = 'bbb-123';
    const result = workflowPortalAdapter.adapt(workflowPortalDto, workflowId);
    expect(result).toEqual({
      id: 'workflow-id',
      name: 'workflow-name',
      workflowId: 'bbb-123'
    })
  });
});
