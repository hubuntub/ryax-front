// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export type WorkflowModuleIOTypeDto =
  'STRING'
  | 'LONGSTRING'
  | 'INTEGER'
  | 'FLOAT'
  | 'PASSWORD'
  | 'ENUM'
  | 'FILE'
  | 'DIRECTORY'
  | 'TABLE'

export interface WorkflowModuleIODto {
  id: string;
  technical_name: string;
  display_name: string;
  type: WorkflowModuleIOTypeDto;
  help: string;
  enum_values?: string[];
}

export interface WorkflowModuleOutputDto extends WorkflowModuleIODto {
  workflow_module_id: string;
}


