// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectAdapter } from '../../entities/adapters';
import { ProjectDto } from '../../entities/dtos';
import { Project } from '../../entities';

@Injectable()
export class ProjectApiService {
  private baseUrl = '/api/authorization';

  constructor(private http: HttpClient) { }

  public getProjects(): Observable<Project[]> {
    return this.http.get<ProjectDto[]>(this.baseUrl + '/v2/projects').pipe(
      map(dtos => dtos.map(dto => new ProjectAdapter().adapt(dto)))
    );
  }

  public setProject(projectId: string): Observable<void> {
    return this.http.post<void>(this.baseUrl + '/v2/projects/current', { project_id: projectId });
  }
}
