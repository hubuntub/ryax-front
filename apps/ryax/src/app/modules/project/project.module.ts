// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';
import { HasProjectsComponent } from './components/has-projects/has-projects.component';
import { ProjectComponent } from './components/project/project.component';
import { ProjectApiService } from './services/project-api/project-api.service';
import { ProjectEffects } from './state/effects/index';
import { ProjectFeatureKey, ProjectReducerProvider, ProjectReducerToken } from './state/reducers/index';


@NgModule({
  declarations: [
    ProjectComponent,
    HasProjectsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    StoreModule.forFeature(ProjectFeatureKey, ProjectReducerToken),
    EffectsModule.forFeature([ProjectEffects]),
    SharedModule
  ],
  providers: [
    ProjectReducerProvider,
    ProjectApiService
  ],
  exports: [
    HasProjectsComponent
  ]
})
export class ProjectModule { }
