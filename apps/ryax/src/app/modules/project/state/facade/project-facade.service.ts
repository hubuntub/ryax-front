// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProjectState } from '../reducers';
import { ProjectActions } from '../actions';
import { selectProjectList, selectProjectListError, selectProjectListLoading } from '../selectors/index';

@Injectable({
  providedIn: 'root'
})
export class ProjectFacade {

  public error$ = this.store.select(selectProjectListError);
  public loading$ = this.store.select(selectProjectListLoading);
  public projects$ = this.store.select(selectProjectList);


  constructor(
    private readonly store: Store<ProjectState>
  ) { }

  public getList() {
    this.store.dispatch(ProjectActions.getList());
  }

  public activateProject(projectId: string) {
    this.store.dispatch(ProjectActions.activate(projectId));
  }
}
