// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { Project } from '../../entities/index';


export const getList = createAction(
  '[Project] Get Project list'
);

export const loadSuccess = createAction(
  '[Project] Load Project list success',
  (projects: Project[]) => ({ projects })
);

export const loadError = createAction(
  '[Project] Load Project list error',
  (error: HttpErrorResponse) => ({ error })
);

export const activate = createAction(
  '[Project] Activate Project',
  (projectId: string) => ({ projectId })
);

export const setSuccess = createAction(
  '[Project] Set Project success',
  (projectId: string) => ({ projectId })
);

export const setError = createAction(
  '[Project] Set Project error',
  (error: HttpErrorResponse) => ({ error })
);


