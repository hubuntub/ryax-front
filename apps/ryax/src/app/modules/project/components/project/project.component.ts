// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { ProjectFacade } from '../../state/facade/index';

@Component({
  selector: 'ryax-project',
  templateUrl: './project.component.pug',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent {

  public projects$ = this.projectFacade.projects$;
  public loading$ = this.projectFacade.loading$;

  constructor(
    private projectFacade: ProjectFacade
  ) { }

  public activateProject(projectId: string) {
    this.projectFacade.activateProject(projectId);
  }


}
