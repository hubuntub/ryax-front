// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Route } from '@angular/router';
import { AlreadyLoggedGuard } from './modules/auth/guards/already-logged/already-logged.guard';
import { IsLoggedGuard } from './modules/auth/guards/is-logged/is-logged.guard';
import { HeaderComponent } from './modules/header/header/header.component';
import { LayoutComponent } from './modules/shared/components';

const portalRoute: Route = {
  path: 'portal',
  loadChildren: () => import('./modules/portal/portal-access.module').then(m => m.PortalAccessModule)
};

const executionRoute: Route = {
  path: 'execution',
  loadChildren: () => import('./modules/portal/execution-access.module').then(m => m.ExecutionAccessModule)
};

const authRoute: Route = {
  path: 'login',
  canActivate: [AlreadyLoggedGuard],
  loadChildren: () => import('./modules/auth/auth-access.module').then(m => m.AuthAccessModule)
};

const projectRoute: Route = {
  path: 'projects',children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () => import('./modules/project/project-access.module').then(m => m.ProjectAccessModule)
    }
  ]
};

const dashboardRoute: Route = {
  path: 'dashboard',
  loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
};

const settingsRoute: Route = {
  path: 'settings',
  loadChildren: () => import('./modules/settings/settings.module').then(m => m.SettingsModule)
};

const workflowRoute: Route = {
  path: 'workflow',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () => import('./modules/workflow/workflow-access.module').then(m => m.WorkflowAccessModule)
    }
  ]
};

const studioRoute: Route = {
  path: 'studio',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () => import('./modules/studio/studio-access.module').then(m => m.StudioAccessModule)
    }
  ]
};

const redirect: Route = {
  path: '',
  pathMatch: 'full',
  redirectTo: '/login'
}

const loggedRoutes: Route = {
  path: '',
  canActivate: [IsLoggedGuard],
  component: HeaderComponent,
  children: [
    dashboardRoute,
    projectRoute,
    workflowRoute,
    studioRoute,
    executionRoute,
    settingsRoute,
    redirect,
  ]
}

const wildcardRoute: Route = {
  path: '**',
  redirectTo: 'dashboard',
};

export const appRoutes = [
  portalRoute,
  loggedRoutes,
  authRoute,
  redirect,
  wildcardRoute,
];
