{ ryaxpkgs ? import <ryaxpkgs> {},
  appDist ? ./dist/apps/ryax,
  nginxConfig ? ./packaging/nginx.conf
}:
let
  baseImage = ryaxpkgs.pkgs.callPackage ./packaging/ryax-front-base.nix { };
in
{
  inherit baseImage;
  image = ryaxpkgs.pkgs.callPackage ./packaging/ryax-front.nix {
    inherit appDist;
    inherit nginxConfig;
    inherit baseImage;
  };
}
