// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { SpinComponent } from './spin.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzIconModule } from 'ng-zorro-antd/icon';

export default {
  title: 'SpinComponent',
  component: SpinComponent,
  decorators: [
    moduleMetadata({
      imports: [
        NzSpinModule,
        NzIconModule
      ],
    })
  ],
} as Meta<SpinComponent>;

const Template: Story<SpinComponent> = (args: SpinComponent) => ({
  props: args,
});


export const Base = Template.bind({});
Base.args = {
  indicator: false
}
Base.argTypes = {
  indicator: {
    control: { type: 'boolean' }
  },
}

export const Custom = Template.bind({});
Custom.args = {
  indicator: true
}
Custom.argTypes = {
  indicator: {
    control: { type: 'boolean' }
  },
}
