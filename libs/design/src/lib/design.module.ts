// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import { InputNumberComponent } from './input-number/input-number.component';
import { SelectComponent } from './select/select.component';
import { SpinComponent } from './spin/spin.component';
import { UploadComponent } from './upload/upload.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { IconDefinition } from '@ant-design/icons-angular';
import {
  DeleteOutline,
  DownloadOutline,
  EyeInvisibleOutline,
  FileTwoTone,
  InboxOutline
} from '@ant-design/icons-angular/icons';
import { NzIconModule } from 'ng-zorro-antd/icon';

const icons: IconDefinition[] = [
  InboxOutline,
  DownloadOutline,
  DeleteOutline,
  EyeInvisibleOutline,
  FileTwoTone
];

@NgModule({
  imports: [
    CommonModule,
    NzIconModule.forChild(icons),
    ...DesignModule.NgZorroModules,
  ],
  declarations: [
    ButtonComponent,
    InputComponent,
    InputNumberComponent,
    SelectComponent,
    SpinComponent,
    UploadComponent
  ],
  exports: [
    UploadComponent
  ]
})
export class DesignModule {

  // eslint-disable-next-line
  static NgZorroModules: any[] = [
    NzButtonModule,
    NzInputModule,
    NzSelectModule,
    NzUploadModule,
    NzCardModule,
    NzSpinModule,
    NzInputNumberModule,
    NzFormModule,
    NzToolTipModule,
    NzTimelineModule,
    NzTagModule,
    NzGridModule,
    NzCollapseModule
  ];
}
