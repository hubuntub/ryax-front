// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NzButtonShape, NzButtonSize, NzButtonType } from 'ng-zorro-antd/button';

@Component({
  selector: 'ryax-button',
  templateUrl: './button.component.pug'
})
export class ButtonComponent {

  @Input() public text = '';
  @Input() public type: NzButtonType = "primary";
  @Input() public size: NzButtonSize = "default";
  @Input() public shape: NzButtonShape = null;
  @Input() public loading = false;
  @Input() public disabled = false;
  @Input() public danger = false;
  @Input() public ghost = false;

  @Output() public buttonClick = new EventEmitter<MouseEvent>();

  public clicked(event: MouseEvent) {
    this.buttonClick.emit(event);
  }

}
