// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { UploadComponent } from './upload.component';

import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzIconModule } from 'ng-zorro-antd/icon';

import { IconDefinition } from '@ant-design/icons-angular';
import { InboxOutline, DownloadOutline, DeleteOutline, FileTwoTone } from '@ant-design/icons-angular/icons';

const icons: IconDefinition[] = [ InboxOutline, DownloadOutline, DeleteOutline, FileTwoTone ];

export default {
  title: 'UploadComponent',
  component: UploadComponent,
  decorators: [
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        NzIconModule.forRoot(icons),
        NzUploadModule,
      ],
    })
  ],
} as Meta<UploadComponent>;

const Template: Story<UploadComponent> = (args: UploadComponent) => ({
  props: args,
});


export const DragBox = Template.bind({});
DragBox.args = {
  listType: 'picture-card',
  nzDirectory: false
}
DragBox.argTypes = {
  listType: {
    options: ['text', 'picture', 'picture-card'],
    control: { type: 'select' }
  },
  nzDirectory: {
    control: { type: 'boolean' }
  },
  uploadDone: {
    action: 'upload'
  }
}
