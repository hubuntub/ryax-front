// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { NzUploadFile, NzUploadListType } from 'ng-zorro-antd/upload';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ryax-upload',
  templateUrl: './upload.component.pug',
  styleUrls: ['./upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef( /* istanbul ignore next */ () => UploadComponent),
      multi: true
    }
  ]
})

export class UploadComponent implements ControlValueAccessor {
  @Input() public listType: NzUploadListType = "text";
  @Input() public nzDirectory = false;
  @Output() public delete: EventEmitter<void> = new EventEmitter<void>();
  @Output() public uploadDone: EventEmitter<string> = new EventEmitter<string>();

  public fileName = "";
  public fileList: NzUploadFile[] = [];
  private disabled = false;
  /* istanbul ignore next */
  private onChange = (arg?: unknown) => arg;
  /* istanbul ignore next */
  private onTouched = (arg?: unknown) => arg;

  get isDisabled() {
    return this.disabled;
  }

  get hasFile() {
    return !!this.fileName;
  }

  get isUploadZone() {
    return this.listType === 'picture-card';
  }

  public onInputChange = (file: NzUploadFile): boolean => {
    if (this.nzDirectory) {
      this.fileList = [...this.fileList, file];
    } else {
      this.fileName = file.name;
    }
    this.onChange(file);
    this.onTouched();
    return false;
  };

  public registerOnChange(onChange: () => unknown): void {
    this.onChange = onChange;
  }

  public registerOnTouched(onTouched: () => unknown): void {
    this.onTouched = onTouched;
  }

  public writeValue(value: string): void {
    this.fileName = value;
  }

  public clearFile() {
    if (this.nzDirectory) {
      this.fileList = [];
    } else {
      this.fileName = "";
    }
    this.onChange();
    this.onTouched();
    this.delete.emit();
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
