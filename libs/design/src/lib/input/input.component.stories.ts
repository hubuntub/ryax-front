// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { InputComponent } from './input.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';

export default {
  title: 'InputComponent',
  component: InputComponent,
  decorators: [
    moduleMetadata({
      imports: [
        NzInputModule,
        NzIconModule
      ],
    })
  ],
} as Meta<InputComponent>;

const Template: Story<InputComponent> = (args: InputComponent) => ({
  props: args,
});


export const Text = Template.bind({});
Text.args = {
  type: 'text'
}
Text.argTypes = {
  type: {
    options: ['text', 'textarea', 'password'],
    control: { type: 'radio' }
  }
}

export const Textarea = Template.bind({});
Textarea.args = {
  type: 'textarea'
}
Textarea.argTypes = {
  type: {
    options: ['text', 'textarea', 'password'],
    control: { type: 'radio' }
  }
}

export const Password = Template.bind({});
Password.args = {
  type: 'password'
}
Password.argTypes = {
  type: {
    options: ['text', 'textarea', 'password'],
    control: { type: 'radio' }
  }
}
