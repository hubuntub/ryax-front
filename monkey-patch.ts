// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
/**
 * Monkey patch console warn and error to fail if a test makes calls to console.warn or console.error.
 */
function patchConsole(): void {
  console.warn = function(message?: any): void {
    const warn = new Error(message);
    Error.captureStackTrace(warn, console.warn);
    throw warn;
  };

  console.error = function(message?: any): void {
    const err = new Error(message);
    Error.captureStackTrace(err, console.error);
    throw err;
  };
}

patchConsole();
