module.exports = {
  // Redirect to fake server
  '/api': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true,
    logLevel: "debug"
  },
  // '/runner': {
  //   target: `https://dev-1.ryax.io`,
  //   secure: false,
  //   changeOrigin: true,
  // },
  // '/studio': {
  //   target: `https://dev-1.ryax.io`,
  //   secure: false,
  //   changeOrigin: true
  // },
  // '/repository': {
  //   target: `https://dev-1.ryax.io`,
  //   secure: false,
  //   changeOrigin: true
  // },
  // '/authorization': {
  //   target: `https://dev-1.ryax.io`,
  //   secure: false,
  //   changeOrigin: true
  // },
};
