module.exports = (config) => {
  const pugTemplateConfig = {
    test: /\.pug$/,
    use: [
      { loader: "apply-loader" },
      { loader: "pug-loader" }
    ]
  };

  config.module.rules.push(pugTemplateConfig);
  return config;
};
