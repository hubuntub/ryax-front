module.exports = {
  stories: [],
  addons: ['@storybook/addon-essentials'],
  // uncomment the property below if you want to apply some webpack config globally
  webpackFinal: async (config) => {
    // Make whatever fine-grained changes you need that should apply to all storybook configs
    const pugTemplateConfig = {
      test: /\.pug$/,
      use: [
        { loader: "apply-loader" },
        { loader: "pug-loader" }
      ]
    };

    config.module.rules.push(pugTemplateConfig);
    // Return the altered config
    return config;
  },
};
