{
  dockerTools,
  baseImage,
  appDist,
  nginxConfig ? ./nginx.conf,
  baseConfig ? (import ./config.nix),
  tag ? "latest",
}:
dockerTools.buildImage {
  name = "ryax-front";
  inherit tag;
  fromImage = baseImage;

  extraCommands = ''
    mkdir data
    echo "Copying web app from path: ${appDist}"
    cp -r ${appDist}/* data
    mkdir -p etc/nginx
    cp ${nginxConfig} etc/nginx/nginx.conf
  '';

  config = baseConfig;
}
