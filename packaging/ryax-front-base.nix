{
  writeText,
  dockerTools,
  nginx,
  nginxConfig ? ./nginx.conf,
  baseConfig ? (import ./config.nix),
  tag ? "latest"
}:

dockerTools.buildImage {
  name = "ryax-base-front";
  inherit tag;
  contents = [ nginx ];

  extraCommands = ''
    # Create Nginx user
    mkdir etc
    chmod u+w etc
    echo "nginx:x:1000:1000::/:" > etc/passwd
    echo "nginx:x:1000:nginx" > etc/group

    # Add Nginx config
    mkdir etc/nginx
    ln -s ${nginxConfig} etc/nginx/nginx.conf
    ln -s ${nginx}/conf/mime.types etc/nginx/mime.types

    # Create /var folders
    mkdir -p var/cache/nginx
    mkdir -p var/log/nginx

    # Create web site root location directory
    mkdir data
  '';

  config = baseConfig;
}
