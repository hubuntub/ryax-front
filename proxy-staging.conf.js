module.exports = {
  // Redirect to fake server
  '/runner': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true,
  },
  '/studio': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/repository': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/authorization': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
};
