const { getJestProjects } = require('@nrwl/jest');

module.exports = {
  projects: getJestProjects(),
  collectCoverage: true,
  collectCoverageFrom: ["**/*.{component,service,store,adapter,pipe,directive,facade,effects,reducers,guard}.ts", "!**/node_modules/**"],
  coverageDirectory: "./coverage",
  coverageReporters: ["html", "text", "text-summary", "cobertura"],
  reporters: ["default", "jest-junit"],
  testMatch: ["**/*.spec.ts"],
  setupFiles: ['./monkey-patch.ts'],
};
